#! /usr/bin/env python3

import json
import os

from dotenv import load_dotenv
from transformers import pipeline

load_dotenv()

classifications_file_path = os.path.join(
    os.environ["ECHEANCIER_DATA_DIR"], os.environ["CLASSIFICATIONS_FILENAME"]
)
with open(classifications_file_path) as classifications_file:
    classifications = json.load(classifications_file)

classifier = pipeline(
    "zero-shot-classification",
    model="joeddav/xlm-roberta-large-xnli",
    use_auth_token=os.environ["HUGGING_FACE_USER_ACCESS_TOKEN"],
)
etat_by_categorie = {
    "Mesure appliquée": "applique",
    "Mesure en attente d'application": "en_attente_application",
    "Mesure ne nécessitant pas de texte d'application": "caduc",
}
categories = list(etat_by_categorie.keys())

for classification in classifications:
    classifier_etat = classifier(classification["decret"], categories)
    classification["classifier_etat"] = etat_by_categorie[classifier_etat["labels"][0]]

    classement = classification["classifier_etat"]
    decret = classification["decret"]
    print(
        f"{classement}|{decret}",
    )

with open(classifications_file_path, "w") as classifications_file:
    json.dump(classifications, classifications_file, ensure_ascii=False, indent=2)
