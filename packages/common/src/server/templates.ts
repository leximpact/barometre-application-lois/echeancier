import type { DossierParlementaire } from "@tricoteuses/assemblee"
import dedent from "dedent-js"
import fs from "fs-extra"
import { parse, type HTMLElement } from "node-html-parser"
import path from "path"

const commonIFrameStyle = dedent`
  iframe#barometre-leximpact {
    border-width: 0;
    min-width: 100%;
    width: 1px;
  }
  
  #leximpact-btn-scroll-top {
    position: fixed;
    right: 24px;
    bottom: 24px;
    width: 50px;
    height: 50px;
    border: none;
    box-shadow: 2px 2px 8px 0 rgba(0,0,0,.25);
  }
  #leximpact-btn-scroll-top > svg {
    transform: rotate(-90deg);
  }
  .leximpact-hidden {
    display: none;
  }
`

export const iFrameResizerScript = fs.readFileSync(
  path.resolve("../../node_modules/iframe-resizer/js/iframeResizer.js"),
  { encoding: "utf-8" },
)

export const dossierAssembleeIFrameStyle = dedent`
  ${commonIFrameStyle}

  /* Suppression lien échéancier Légifrance */

  div.jumbotron.jumbotron-fluid.p-4.m-0 {
    display: none;
  }

  /* Version mobile */

  @media (max-width: 977px) {
    /* Modifie layout */
    .slick-track {
      width: auto !important;
      overflow-y: auto !important; /*Rend la frise scrolable*/
    }
    .old-style #haut-contenu-page {
      width: auto;
    }
    .old-style #page #contenu-page {
      width: auto;
    }

    /* Diminue typo titre */
    .old-style h1 {
      font-size: 2rem !important;
    }
    /* Diminue typo breadcrumb */
    .old-style ol.breadcrumb li {
      font-size: 0.875rem !important;
    }

    /* Diminue typo et taille des boutons */
    .old-style a.version {
      font-size: 0.875rem !important;
      padding: 0.3rem !important;
      margin: 1px !important;
    }
    /* Diminue typo frise étapes */
    .old-style
      .frise-etape
      .frise-etape-top
      .frise-etape-container
      .frise-etape-text {
      font-size: 12px;
    }
  }
`

export const iFrameScript = dedent`
  window.leximpact ??= {};
  const leximpact = window.leximpact;

  leximpact.iFrames = iFrameResize({
    heightCalculationMethod: "documentElementOffset",
    inPageLinks: true,
    log: false,
    onInit: (iframe) => {
      if (window.location.hash) {
        const hash = window.location.hash.replace(/:/g, "=").replace(/~/g, "&")
        leximpact.barometreIFrame.iFrameResizer.sendMessage({
          action: "hash",
          value: hash,
        }, "*");
      }
    },
    onMessage: ({ iframe, message }) => {
      if (message.action === "filtersOpen") {
        if (message.value) {
          document.body.style.overflow = "hidden";
          leximpact.barometreIFrame.iFrameResizer.sendMessage({
            action: "frameHeight",
            value: window.innerHeight
                   - document.getElementById("header-navigation-mobile").clientHeight
                   - document.getElementById("site-footer--navigation").clientHeight
          }, "*");
        } else {
          document.body.style.overflow = "auto";
        }
      } else if (message.action === "hash") {
        const hash = message.value.replace(/=/g, ":").replace(/&/g, "~");
        window.history.pushState(null, "", window.location.href.replace(window.location.hash, "") + hash);
      }
    }
  }, "#barometre-leximpact");
  leximpact.barometreIFrame = leximpact.iFrames[0];

  if (window.location.hash) {
    leximpact.barometreIFrame.addEventListener("load", (event) => {
      const hash = window.location.hash.replace(/:/g, "=").replace(/~/g, "&")
      leximpact.barometreIFrame.src = leximpact.barometreIFrame.src.split("#")[0] + hash;
    });
  }

  leximpact.timeout = undefined;

  document.addEventListener("scroll", () => {
    const offsetY = Math.round(
      leximpact.barometreIFrame.getBoundingClientRect().top
      - document.getElementById("header-navigation").getBoundingClientRect().bottom
      - document.getElementById("header-navigation-mobile").getBoundingClientRect().bottom
    );
    
    if (leximpact.timeout == null) {
      leximpact.barometreIFrame.iFrameResizer.sendMessage({ action: "scrollStarted" }, "*");
    } else {
      clearTimeout(leximpact.timeout);
    }
    leximpact.timeout = setTimeout(() => {
      leximpact.barometreIFrame.iFrameResizer.sendMessage({
        action: "offsetY",
        value: offsetY
      }, "*");
      leximpact.timeout = undefined;
    }, 100);
    
    if (scrollY > window.innerHeight) {
      document.getElementById("leximpact-btn-scroll-top").classList.remove("leximpact-hidden");
    } else {
      document.getElementById("leximpact-btn-scroll-top").classList.add("leximpact-hidden");
    }
  });
  
  document.getElementById("leximpact-btn-scroll-top").addEventListener("click", function() {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  });
`

export const standardIFrameStyle = dedent`
  ${commonIFrameStyle}

  /* EN-TÊTE */

  /* Retire fil d'Ariane */
  .page-breadcrumb {
    display: none !important;
  }

  /* Version Mobile */

  @media (max-width: 768px) {
    #leximpact-btn-scroll-top {
      bottom: 74px;
    }
    
    .h1 {
      font-size: 1.2rem;
    }
    .page-description {
      font-size: 0.875rem;
    }
    .page-title {
      padding-bottom: 0.8rem !important;
    }
    .page-header {
      padding-top: 1rem !important;
    }
    .page-title .hr-flag {
      margin-top: 0.5rem !important;
    }
    .page-description {
      padding-bottom: 0rem !important;
    }
  }

  @media (min-width: 768px) and (max-width: 977px) {
    .h1 {
      font-size: 1.6rem;
    }
    .page-description {
      font-size: 0.9rem;
    }
    .page-title {
      padding-bottom: 1rem !important;
    }
    .page-header {
      padding-top: 1rem !important;
    }
    .page-title .hr-flag {
      margin-top: 0.5rem !important;
    }
    .page-description {
      padding-bottom: 0rem !important;
    }
  }

  @media (min-width: 977px) {
    .page-title {
      padding-bottom: 1.5rem !important;
    }
    .page-header {
      padding-top: 1.2rem !important;
    }
    .page-title .hr-flag {
      margin-top: 0.8rem !important;
    }
    .page-description {
      padding-bottom: 0.5rem !important;
    }
  }
`

const scrollTopButton = dedent`
<button id="leximpact-btn-scroll-top" class="leximpact-hidden">
  <svg width="20" height="20" viewBox="0 0 8 13" fill="#000" xmlns="http://www.w3.org/2000/svg"><path d="M0 0V4.33333L2.82353 6.25926L0 8.66667V13L8 6.25926L0 0Z"></path></svg>
</button>
`

function bareHtmlFromElement(
  element: HTMLElement | undefined | null,
  outer = false,
): string {
  return (
    (outer ? element?.outerHTML : element?.innerHTML)
      ?.replace(/\s+aria-[-a-zA-Z]+=".*?"/gs, "")
      .replace(/\s+class=".*?"/gs, "")
      .replace(/\s+data-[-a-zA-Z]+=".*?"/gs, "")
      .replace(/\s+href=".*?"/gs, "")
      .replace(/\s+style=".*?"/gs, "")
      .replace(/\s+target=".*?"/gs, "")
      .replace(/<a(>|\s.*?>)/gs, "")
      .replace(/<\/a(>|\s.*?>)/gs, "")
      .replace(/<iconify-icon .*?<\/iconify-icon>/gs, "")
      .replace(/<script.*?<\/script>/gs, "")
      .replace(/<svg.*?<\/svg>/gs, "") ?? ""
  )
}

export function dossierAssembleeHtmlFragmentFromTemplate({
  dossierAssemblee,
  embeddedPage,
  frontOfficeUrl,
}: {
  dossierAssemblee: DossierParlementaire
  embeddedPage?: string
  frontOfficeUrl: string
}): string {
  const embeddedElement =
    embeddedPage === undefined ? undefined : parse(embeddedPage)
  const tableElement = embeddedElement?.querySelector("table")
  const tableHtml = bareHtmlFromElement(tableElement, true)

  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      `/embedded/dossiers/${dossierAssemblee.uid}`,
      frontOfficeUrl,
    ).toString()}">${tableHtml}</iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}

export function echeancierHtmlFragmentFromTemplate({
  dossierAssembleeUid,
  embeddedPage,
  frontOfficeUrl,
  loiJorfId,
}: {
  dossierAssembleeUid?: string | null
  embeddedPage?: string
  frontOfficeUrl: string
  loiJorfId: string
}): string {
  const embeddedElement =
    embeddedPage === undefined ? undefined : parse(embeddedPage)
  const tableElement = embeddedElement?.querySelector("table")
  const tableHtml = bareHtmlFromElement(tableElement, true)

  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      `/embedded/${dossierAssembleeUid ?? loiJorfId}`,
      frontOfficeUrl,
    ).toString()}">${tableHtml}</iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}

export function fonctionnementHtmlFragmentFromTemplate({
  embeddedPage,
  frontOfficeUrl,
}: {
  embeddedPage?: string
  frontOfficeUrl: string
}): string {
  const embeddedElement =
    embeddedPage === undefined ? undefined : parse(embeddedPage)
  const fonctionnementElement = embeddedElement?.querySelector("body>div")
  const fonctionnementHtml = bareHtmlFromElement(fonctionnementElement)

  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      "/embedded/fonctionnement",
      frontOfficeUrl,
    ).toString()}">${fonctionnementHtml}</iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}

export function indexHtmlFragmentFromTemplate({
  embeddedPage,
  frontOfficeUrl,
}: {
  embeddedPage?: string
  frontOfficeUrl: string
}): string {
  const embeddedElement =
    embeddedPage === undefined ? undefined : parse(embeddedPage)
  const loisElement = embeddedElement?.querySelector("section#liste_lois")
  const loisHtml = bareHtmlFromElement(loisElement)

  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      "/embedded",
      frontOfficeUrl,
    ).toString()}">${loisHtml}</iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}

export function standardHtmlFragmentFromTemplate({
  fragmentUrl,
  frontOfficeUrl,
}: {
  fragmentUrl: string
  frontOfficeUrl: string
}): string {
  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      fragmentUrl,
      frontOfficeUrl,
    ).toString()}"></iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}

export function statistiquesHtmlFragmentFromTemplate({
  embeddedPage,
  frontOfficeUrl,
}: {
  embeddedPage?: string
  frontOfficeUrl: string
}): string {
  const embeddedElement =
    embeddedPage === undefined ? undefined : parse(embeddedPage)
  const statistiquesElement = embeddedElement?.querySelector("body>div")
  const statistiquesHtml = bareHtmlFromElement(statistiquesElement)

  return dedent`
    ${scrollTopButton}
    <iframe id="barometre-leximpact" src="${new URL(
      "/embedded/statistiques",
      frontOfficeUrl,
    ).toString()}">${statistiquesHtml}</iframe>
    <script>
      ${iFrameScript}
    </script>
  `
}
