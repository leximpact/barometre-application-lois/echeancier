import type {
  Document,
  DossierParlementaire,
  Organe,
} from "@tricoteuses/assemblee"
import type {
  DossierLegislatif,
  Echeancier,
  EcheancierLigne,
  JorfArticle,
  JorfSectionTa,
  JorfTexteVersion,
  LegiArticle,
  LegiTexteVersion,
} from "@tricoteuses/legal-explorer"
import objectHash from "object-hash"

export interface Classification {
  decret: string // dossier.CONTENU.ECHEANCIER.LIGNE.DECRET
  algorithmic_etat?: EtatLigne
  classifier_etat?: EtatLigne
  human_etat?: EtatLigne
  note?: string
}

export type EcheancierLigneEtendue = EcheancierLigne & {
  DECRET_HTML?: string
}
export type EtatLigne = (typeof etatsLigne)[number]

export interface Loi extends LoiStats {
  /// Identifiant unique de la commission de l'Assemblée qui a été saisie au fond
  /// pour cette loi.
  /// Cette information est présente même pour les lois qui n'ont pas de dossier législatif
  /// Assemblée, suite à un travail de collecte de l'équipe LexImpact.
  commissionFondAssembleeUid?: string
  /// Date de publication de la loi au Journal officiel
  /// Issu de TEXTE_VERSION.META.META_SPEC.META_TEXTE_CHRONICLE.DATE_TEXTE
  datePublication: string
  /// Informations extraites du dossier parlementaire de l'Assemblée concernant cette loi
  /// Présent à partir de la XVème législature (et une partie de la XIVème)
  dossierAssembleeExtrait?: {
    /// Identifiant unique de la commission de l'Assemblée qui a été saisie au fond
    /// pour cette loi.
    /// Issu du champ organeRef de l'acte législatif du DossierParlementaire de l'Assemblée
    /// dont le codeActe correspond à l'expression régulière /^AN.*-COM-FOND-SAISIE$/
    /// Utiliser plutôt le champ commissionFondAssembleeUid.
    commissionFondUid?: string
    /// Identifiant du dossier législatif du Sénat
    dossierSenatId?: string
    /// Symbole utilisé pour construire l'URL publique d'accès au dossier parlementaire
    /// de l'Assemblée
    titreChemin?: string
    /// Identifiant unique du dossier parlementaire de l'Assemblée
    uid: string
  }
  /// Identifiant unique du dossier législatif de Légifrance concernant cette loi
  /// Présent pour certaines lois depuis 2002
  dossierId?: string
  /// Numéro de la législature sous laquelle la loi a été votée
  /// Issu de DOSSIER_LEGISLATIF.META.META_DOSSIER_LEGISLATIF.LEGISLATURE?.NUMERO
  /// ou sinon, en cas d'absence de dossier législatif, en recherchant la législature
  /// dont l'intervalle formé avec les dates de début et de fin comprennent la date
  ///  de publication de la loi
  legislature: number
  /// Identifiant unique de la loi publiée au Journal officiel
  loiJorfId: string
  /// Informations sur les mesures demandées par la loi
  /// Issu de DOSSIER_LEGISLATIF.CONTENU.ECHEANCIER
  /// (base JORF de la Dila)
  mesures?: Array<{
    delaiApplication?: number
    etat: EtatLigne
  }>
  /// Mots-clés décrivant la loi
  /// Issu de TEXTE_VERSION.META.META_SPEC.META_TEXTE_VERSION.MCS_TXT.MC
  /// (base JORF de la Dila)
  motsCles?: string[]
  /// Titre complet de la loi
  /// Issu de TEXTE_VERSION.META.META_SPEC.META_TEXTE_VERSION.TITREFULL
  /// (base JORF de la Dila)
  titreComplet?: string
}

export interface LoiContext {
  loi: Loi
  gouvernement?: Organe
  presidence?: Organe
  year: number
}

export interface LoiEtMesures extends LoiStats {
  /// Dossier législatif de Légifrance sur la loi
  dossier: DossierLegislatif
  /// Dossier législatif de l'Assemblée
  dossierAssemblee?: DossierParlementaire
  /// URL de la page web de l'échéancier sur le site Légifrance
  echeancierLegifranceUrl?: string
  /// Liste ante-chronologique des différentes versions de l'échéancier
  /// des mesures de la loi
  echeanciers: Echeancier[]
  /// Identifiant unique de la loi publiée au Journal officiel
  loiJorfId: string
  /// Rapport d'application de l'Assemblée sur la loi
  rapportApplication?: Document
  /// Articles de Légifrance liés à la loi
  /// TODO ajouter KaliArticle à referenceArticleParId.
  referenceArticleParId?: { [id: string]: JorfArticle | LegiArticle }
  /// Sections de textes de Légifrance liés à la loi
  referenceSectionTaParId?: { [id: string]: JorfSectionTa }
  /// Textes versions de Légifrance liés à la loi
  referenceTexteVersionParId?: {
    [id: string]: JorfTexteVersion | LegiTexteVersion
  }
  /// Informations de Légifrance sur la loi
  texteVersion: JorfTexteVersion
}

export interface LoiStats {
  /// Booléen indiquant si la loi est d'application directe ou non
  /// Pour qu'une loi soit considérée d'application directe, il faut qu'elle n'ait pas
  /// de dossier législatif sur Légifrance ou que celui-ci n'ait pas d'échéancier et que,
  /// soit elle autorise un accord international (cf attribut `autorisationAccordInternational`),
  /// soit DOSSIER_LEGISLATIF.CONTENU.LIBELLE_TEXTE_1 inclut la phrase
  /// "n'appelant pas de décret d'application". Ou alors il faut que la loi ait des mesures
  /// et qu'elles soient toutes dans l'état "caduc".
  applicationDirecte?: boolean
  /// Booléen indiquant si la loi est une loi autorisant des accords internationaux
  /// Pour cela, il faut que le titre de la loi (dossier.META.META_DOSSIER_LEGISLATIF.TITRE)
  /// inclut "autorisant l'accession", "autorisant l'adhésion", "autorisant l'approbation" ou
  /// "autorisant la ratification".
  autorisationAccordInternational: boolean
  /// Booléen indiquant si la loi est une loi habilitant le gouvernement à prendre des
  /// ordonnances
  /// Pour cela, il faut que ce ne soit pas une loi de ratification d'ordonnances et que
  /// le titre de la loi (dossier.META.META_DOSSIER_LEGISLATIF.TITRE)
  /// inclut "habilitant le Gouvernement", "d'habilitation à prendre par ordonnances" ou
  /// "portant habilitation du Gouvernement".
  habilitationOrdonnances: boolean
  /// Nombre de mesures dont l'état n'est pas "caduc"
  nombreMesuresAAppliquer: number
  /// Nombre de mesures appliquées
  nombreMesuresAppliquees: number
  /// Booléen indiquant si la loi est une loi de ratification d'ordonnances
  /// Pour cela, il faut que le titre de la loi (dossier.META.META_DOSSIER_LEGISLATIF.TITRE)
  /// inclut "ratifiant l'ordonnance", "ratifiant diverses ordonnances" ou
  /// "ratifiant les ordonnances".
  ratificationOrdonnances: boolean
  /// Booléen indiquant si la loi est sans échéancier et n'est pas d'application directe
  sansEcheancier: boolean
  /// Ratio entre le nombre de mesures appliquées et le nombre de mesures
  /// dont l'état n'est pas "caduc". Quand le nombre de mesures dont l'état n'est
  /// pas "caduc" est 0, ce ratio vaut 1 quand la loi doit être considérée d'application
  /// directe et 0 sinon.
  tauxApplicationMesures: number
}

export interface MesuresData {
  assembleeByLegislature: { [legislature: string]: Organe }
  commissionByUid: { [uid: string]: Organe }
  gouvernements: Organe[]
  lois: Loi[]
  presidences: Organe[]
}

export const etatsLigne = [
  "applique",
  /// Ne nécessite pas de mesure d'application (application directe) ou abrogé, réécrit…
  "caduc",
  "en_attente_application",
] as const
export const etatsLigneMutable = [...etatsLigne]

export const labelByEtatLigne = new Map<EtatLigne | undefined, string>([
  ["en_attente_application", "En attente d'application"],
  ["applique", "Appliqué"],
  ["caduc", "Sans objet, non pris en compte dans le taux d'application"],
])

export function etatFromLigne(
  classificationByDecret: { [decret: string]: Classification },
  ligne: EcheancierLigneEtendue,
): EtatLigne {
  if (ligne.CID_LOI_CIBLE !== undefined) {
    return "applique"
  }
  if (ligne.DECRET === undefined) {
    return "en_attente_application"
  }
  const classification = classificationByDecret[ligne.DECRET]
  if (classification === undefined) {
    return "en_attente_application"
  }
  return (
    classification.human_etat ??
    classification.algorithmic_etat ??
    // classification.classifier_etat ??
    "en_attente_application"
  )
}

export function getOrganeAtDate(
  organes: Organe[],
  date: string,
): Organe | undefined {
  // Sometimes, for example for the legislatures during the dissolution of 2024-06-09,
  // there is a lapse of days between the end of an organe and the creation of a new one.
  // When it occurs, assume that the previous organe continues until the next one begins.
  // => Don't use organe.viMoDe.dateFin.
  //
  // Note: This function assumes that organes are sorted in chronological order of
  // organe.viMoDe.dateDebut.
  for (const organe of [...organes].reverse()) {
    if ((organe.viMoDe.dateDebut as unknown as string) <= date) {
      return organe
    }
  }
  return undefined
}

export function hashEcheancier(echeancier: Echeancier): string {
  return objectHash(echeancier, {
    excludeKeys: (key) => key === "@derniere_maj",
  })
}

export function hashLigne(ligne: EcheancierLigneEtendue): string {
  return objectHash(ligne, {
    excludeKeys: (key) => ["DECRET_HTML", "NUMERO_ORDRE"].includes(key),
  })
}

export function* iterLignes(lignes: EcheancierLigneEtendue[]): Generator<
  {
    ligne: EcheancierLigneEtendue
    merge?: boolean
    sameObjetCount?: number
  },
  void,
  unknown
> {
  let previousLigne: EcheancierLigneEtendue | undefined = undefined
  let sameObjetLignes: EcheancierLigneEtendue[] = []
  for (const ligne of lignesSorted(lignes)) {
    if (
      previousLigne !== undefined &&
      ligne.ARTICLE === previousLigne.ARTICLE &&
      ligne.BASE_LEGALE === previousLigne.BASE_LEGALE &&
      ligne.OBJET === previousLigne.OBJET
    ) {
      sameObjetLignes.push(ligne)
    } else {
      if (sameObjetLignes.length === 1) {
        yield { ligne: sameObjetLignes[0] }
      } else if (sameObjetLignes.length > 1) {
        yield {
          ligne: sameObjetLignes[0],
          sameObjetCount: sameObjetLignes.length,
        }
        for (const sameObjetLigne of sameObjetLignes.slice(1)) {
          yield { ligne: sameObjetLigne, merge: true }
        }
      }
      sameObjetLignes = [ligne]
    }
    previousLigne = ligne
  }
  if (sameObjetLignes.length === 1) {
    yield { ligne: sameObjetLignes[0] }
  } else if (sameObjetLignes.length > 1) {
    yield {
      ligne: sameObjetLignes[0],
      sameObjetCount: sameObjetLignes.length,
    }
    for (const sameObjetLigne of sameObjetLignes.slice(1)) {
      yield { ligne: sameObjetLigne, merge: true }
    }
  }
}

export function* iterLois({
  gouvernements,
  lois,
  presidences,
}: {
  gouvernements: Organe[]
  lois: Loi[]
  presidences: Organe[]
}): Generator<LoiContext, void, unknown> {
  for (const loi of lois.sort((loi1, loi2) =>
    loi2.datePublication.localeCompare(loi1.datePublication),
  )) {
    const gouvernement = getOrganeAtDate(gouvernements, loi.datePublication)
    const presidence = getOrganeAtDate(presidences, loi.datePublication)

    yield {
      loi,
      gouvernement,
      presidence,
      year: parseInt(loi.datePublication.split("-", 2)[0]),
    }
  }
}

export function lignesSorted(
  lignes: EcheancierLigneEtendue[],
): EcheancierLigneEtendue[] {
  return [...lignes].sort((ligne1, ligne2) => {
    const ordre1 =
      ligne1.NUMERO_ORDRE?.replace(/\d+/, (n) => n.padStart(3, "0")) ?? "999"
    const ordre2 =
      ligne2.NUMERO_ORDRE?.replace(/\d+/, (n) => n.padStart(3, "0")) ?? "999"
    return ordre1.localeCompare(ordre2)
  })
}
