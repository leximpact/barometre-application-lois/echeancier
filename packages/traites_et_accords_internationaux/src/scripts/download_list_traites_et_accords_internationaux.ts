import assert from "assert"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"
import { chromium } from "playwright"
import { Locator, expect } from "playwright/test"
import sade from "sade"

type Label = (typeof labels)[number]
type TreatyListItem = Record<Label, string>

const labels = [
  "Numéro",
  "Titre",
  "Date de signature",
  "Date d'adoption",
  "Type d'accord",
] as const

async function downloadListOfTreaties({
  adoption,
  resume,
}: { adoption?: boolean; resume?: string } = {}): Promise<number> {
  const dataDir = path.join(
    "..",
    "..",
    "..",
    "traites_et_accords_internationaux",
  )
  await fs.ensureDir(dataDir)
  const treatiesListFilePath = path.join(
    dataDir,
    `traites_et_accords_internationaux.csv`,
  )

  let currentDay = resume ?? "01/01/0999"
  const today = new Intl.DateTimeFormat("fr-FR").format(new Date())
  const treatyById = new Map<string, TreatyListItem>()

  if (await fs.pathExists(treatiesListFilePath)) {
    const treatiesStream = fs.createReadStream(treatiesListFilePath, {
      encoding: "utf-8",
    })
    const treatiesList = (await new Promise((resolve, reject) => {
      Papa.parse(treatiesStream, {
        header: true,
        complete(results, file) {
          resolve(results.data as TreatyListItem[])
        },
        error(err, file) {
          reject(err)
        },
      })
    })) as TreatyListItem[]
    for (const treaty of treatiesList) {
      treatyById.set(treaty["Numéro"], treaty)
    }
  }

  const browser = await chromium.launch()
  const page = await browser.newPage({ ignoreHTTPSErrors: true })

  iterPeriods: for (;;) {
    let sortNeeded = false
    await page.goto(
      "https://basedoc.diplomatie.gouv.fr/exl-php/recherche/mae_internet___traites",
    )
    {
      const frameLocator = page.frameLocator('css=[name="query"]')
      await expect(
        frameLocator.locator('css=input[type="submit"][value="Chercher"]'),
      ).toBeVisible()
      if (adoption) {
        await frameLocator
          .locator(
            `css=a[href="javascript:afficheOption('DateAdoptionDetail');"]`,
          )
          .click()
        await frameLocator
          .locator('css=input[name="WHERE_SUPEGAL_MAE_DATE_ADOPTION"]')
          .fill(currentDay)
        await frameLocator
          .locator('css=input[name="WHERE_INFEGAL_MAE_DATE_ADOPTION"]')
          .fill(today)
        sortNeeded = true
      } else {
        await frameLocator
          .locator(
            `css=a[href="javascript:afficheOption('DateSignFRDetail');"]`,
          )
          .click()
        await frameLocator
          .locator('css=input[name="WHERE_SUPEGAL_DOC_DAT_EDIT"]')
          .fill(currentDay)
        await frameLocator
          .locator('css=input[name="WHERE_INFEGAL_DOC_DAT_EDIT"]')
          .fill(today)
      }
      await frameLocator
        .locator('css=input[type="submit"][value="Chercher"]')
        .click()
    }

    let previousTraiteNumberAndCount = ""
    for (;;) {
      const frameLocator = page.frameLocator('css=[name="query"]')

      if (adoption && sortNeeded) {
        // Sort by date of adoption.
        await frameLocator
          .locator(`css=select[name="ORDERBY"]`)
          .selectOption({ value: "MAE_DATE_ADOPTION" })
        sortNeeded = false
        continue
      }

      let firstNavigationTd: Locator
      let traiteNumberAndCount: string
      while (true) {
        firstNavigationTd = frameLocator.locator(
          "css=div#site>table.navigation:nth-child(3)>tbody>tr:nth-child(1)>td",
        )
        try {
          await expect(firstNavigationTd).toBeVisible()
        } catch (e) {
          if (
            e instanceof Error &&
            (e as unknown as { matcherResult?: { timeout?: number } })
              .matcherResult?.timeout !== undefined
          ) {
            // Timeout
            if (
              (
                await frameLocator.locator("td.titrechamp").allTextContents()
              ).some((text) =>
                text.includes("Il n'y a pas de résultat à votre recherche"),
              )
            ) {
              break iterPeriods
            }
            console.warn("Timeout.")
            await sleep(1000)
            console.warn("Retrying…")
            continue
          }
          throw e
        }
        // // Reinitialize firstNavigationTd, to avoid error:
        // // locator.allTextContents: Execution context was destroyed, most likely because of a navigation
        // firstNavigationTd = frameLocator.locator(
        //   "css=div#site>table.navigation:nth-child(3)>tbody>tr:nth-child(1)>td",
        // )
        try {
          traiteNumberAndCount = (
            await frameLocator
              .locator(
                "css=div#site>table.navigation:nth-child(3)>tbody>tr:nth-child(1)>td",
              )
              .allTextContents()
          )[0]?.trim()
        } catch (e) {
          console.warn("Retrying after error:")
          console.warn(
            "locator.allTextContents: Execution context was destroyed, most likely because of a navigation",
          )
          // console.error(JSON.stringify(e, null, 2))
          continue
        }
        if (
          traiteNumberAndCount !== undefined &&
          traiteNumberAndCount !== previousTraiteNumberAndCount
        ) {
          break
        }
        await sleep(100)
      }
      const [traiteNumber, traiteCount] = traiteNumberAndCount
        .split("/")
        .map((i) => parseInt(i))
      console.log(currentDay, traiteNumberAndCount, traiteNumber, traiteCount)

      const table = frameLocator.locator("css=table.ivoirList")
      await expect(table).toBeVisible()
      const pageLabels = await table
        .locator("css=thead>tr>td")
        .evaluateAll((tds) => tds.map((td) => (td as HTMLElement).innerText))
      assert.strictEqual(pageLabels.length, labels.length)
      assert(pageLabels.every((label, index) => label === labels[index]))
      const pageTreatiesRows = await table
        .locator("css=tbody>tr")
        .evaluateAll((trs) =>
          trs.map((tr) =>
            [...tr.querySelectorAll("td")].map((td) => td.innerText),
          ),
        )
      const pageTreaties: TreatyListItem[] = []
      for (const treatyRow of pageTreatiesRows) {
        const treaty = Object.fromEntries(
          labels.map((label, index) => [label, treatyRow[index]]),
        ) as TreatyListItem
        pageTreaties.push(treaty)
        treatyById.set(treaty["Numéro"], treaty)
      }
      await fs.writeFile(
        treatiesListFilePath,
        Papa.unparse(
          [...treatyById.entries()]
            .sort(([id1], [id2]) => id1.localeCompare(id2))
            .map(([, treaty]) => treaty),
          { columns: labels as unknown as string[] },
        ),
        { encoding: "utf-8" },
      )

      previousTraiteNumberAndCount = traiteNumberAndCount
      const latestDay =
        pageTreaties.at(-1)![adoption ? "Date d'adoption" : "Date de signature"]
      if (traiteNumber + pageTreatiesRows.length >= traiteCount) {
        if (latestDay === currentDay) {
          break iterPeriods
        }
        currentDay = latestDay
        break
      }

      await firstNavigationTd
        .locator('css=a.navibtn[href="Javascript:Page_Suivante();"]')
        .click()
    }
  }

  // await page.waitForTimeout(10000)
  const pdfBuffer = await page.pdf({
    path: "/tmp/pdf.pdf",
  })
  await browser.close()

  return 0
}

function sleep(s: number): Promise<void> {
  return new Promise((resolve) => setTimeout(resolve, s))
}

sade("download_list_traites_et_accords_internationaux", true)
  .describe(
    "Download list of « Traités et accords de la France » from the website of Ministère des affaires étrangères",
  )
  .option(
    "-a, --adoption",
    "Search by date of adoption instead of date of signing",
  )
  .option("-r, --resume", "Resume downloads at given date of signing")
  .example("--resume 15/07/1304")
  .action(async (options) => {
    await downloadListOfTreaties(options)
    process.exit(0)
  })
  .parse(process.argv)
