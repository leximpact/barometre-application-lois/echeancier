import {
  Audit,
  auditChain,
  auditEmptyToNull,
  auditFunction,
  auditKeyValueDictionary,
  auditMatch,
  auditObject,
  auditOptions,
  auditPartial,
  auditRequire,
  auditSetNullish,
  auditTest,
  auditTrimString,
  strictAudit,
} from "@auditors/core"
import assert from "assert"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"
import { chromium } from "playwright"
import { expect } from "playwright/test"
import sade from "sade"

type BilateralTreaty = TreatyBase & {
  "Type de traité":
    | "BILATERAL"
    | "BILATERAL - Accord entre Etats"
    | "BILATERAL - ACCORD INTERGOUVERNEMENTAL"
    | "BILATERAL - Accord intergouvernemental"
    | "BILATERAL - Arrangement Administratif"
    | "BILATERAL - Arrangement administratif"
    | "BILATERAL - Autre"
    | "BILATERAL - AUTRE ACCORD"
    | "BILATERAL - Autre accord"
    | "BILATERAL - Procès-verbal"
}

type EuropeanAgreement = TreatyBase & {
  "Type de traité":
    | "EUROPEEN"
    | "EUROPEEN - Accord entre Etats"
    | "EUROPEEN - Accord intergouvernemental"
    | "EUROPEEN - Autre"
    | "EUROPEEN - AUTRE ACCORD"
}

type Label = (typeof labels)[number]

type MultilateralTreaty = TreatyBase & {
  "Type de traité":
    | "MULTILATERAL"
    | "MULTILATERAL - Accord entre Etats"
    | "MULTILATERAL - Accord intergouvernemental"
    | "MULTILATERAL - Arrangement administratif"
    | "MULTILATERAL - AUTRE ACCORD"
    | "MULTILATERAL - Autre accord"
  "Etat des ratifications"?: string
}

type Treaty = BilateralTreaty | EuropeanAgreement | MultilateralTreaty

type TreatyBase = {
  Numéro: string
  Titre: string
  "Titre (DE)"?: string
  "Titre (EN)"?: string
  "Titre (SP)"?: string
  "Type de traité": TreatyType
  "Date d'adoption"?: string
  "Entré(e) en vigueur le"?: string
  "Date de fin de vigueur"?: string
  "Ce traité n'est plus en vigueur"?: boolean
  "Signé(e) par la France, le"?: string
  "Signataire(s) français"?: string
  "Signé par l', le, la, les"?: string
  "Lieu d'adoption ou de signature"?: string
  "Ratification par la France"?: string
  "Entré(e) en vigueur en France"?: string
  Dépositaire?: string
  "Accéder au site du dépositaire"?: string
  Parties?: string
  "Modifie, remplace, abroge"?: string
  "Modifié, remplacé, abrogé par"?: string
  "Note juridique"?: string
  "Texte de l'accord"?: string
  "Texte et annexe(s)"?: string
  "Conditions d'entrée en vigueur"?: string
  "Conditions de durée"?: string
  "Dénonciation par la France"?: string
  "Publication au JORF"?: string
  "Publication autre que JORF"?: string
  Rectificatif?: string
  "Certificat ONU"?: string
  // Enregistrement du traité à l'ONU
  Enregistrement?: string
  Pièces: { [id: string]: string }
  Bibliographie?: string
  "Contenu du dossier"?: string
  Observations?: string
  Thématique?: string
}

type TreatyListItem = Record<Label, string>

type TreatyType = (typeof treatyTypes)[number]

const denonciationParLaFranceRegExp =
  /^Dénonciation par la France au ([0123]\d\/[01]?\d\/\d{1,4})$/
const entreeEnVigueurEnFranceRegExp =
  /^Entré\(e\) en vigueur en France, le ([0123]\d\/[01]?\d\/\d{1,4})$/
const frenchDateRegExp = /^[0123]\d\/[01]?\d\/\d{1,4}$/
const labels = [
  "Numéro",
  "Titre",
  "Date de signature",
  "Date d'adoption",
  "Type d'accord",
] as const
const pieceNumberRegExp = /^[-\/\dA-Za-z]+( [A-Z])?$/
const pieceRegExp = /^Pièce n° ([-\/\dA-Za-z]+( [A-Z])?)$/
const treatyBaseKeys = [
  "Numéro",
  "Titre",
  "Titre (DE)",
  "Titre (EN)",
  "Titre (SP)",
  "Type de traité",
  "Date d'adoption",
  "Entré(e) en vigueur le",
  "Date de fin de vigueur",
  "Ce traité n'est plus en vigueur",
  "Signé(e) par la France, le",
  "Signataire(s) français",
  "Signé par l', le, la, les",
  "Lieu d'adoption ou de signature",
  "Ratification par la France",
  "Entré(e) en vigueur en France",
  "Dépositaire",
  "Accéder au site du dépositaire",
  "Parties",
  "Modifie, remplace, abroge",
  "Modifié, remplacé, abrogé par",
  "Note juridique",
  "Texte de l'accord",
  "Texte et annexe(s)",
  "Conditions d'entrée en vigueur",
  "Conditions de durée",
  "Dénonciation par la France",
  "Publication au JORF",
  "Publication autre que JORF",
  "Rectificatif",
  "Certificat ONU",
  "Enregistrement",
  "Pièces",
  "Bibliographie",
  "Contenu du dossier",
  "Observations",
  "Thématique",
]
const treatyTypes = [
  "BILATERAL",
  "BILATERAL - Accord entre Etats",
  "BILATERAL - ACCORD INTERGOUVERNEMENTAL",
  "BILATERAL - Accord intergouvernemental",
  "BILATERAL - Arrangement Administratif",
  "BILATERAL - Arrangement administratif",
  "BILATERAL - Autre",
  "BILATERAL - AUTRE ACCORD",
  "BILATERAL - Autre accord",
  "BILATERAL - Procès-verbal",
  "EUROPEEN",
  "EUROPEEN - Accord entre Etats",
  "EUROPEEN - Accord intergouvernemental",
  "EUROPEEN - Autre",
  "EUROPEEN - AUTRE ACCORD",
  "MULTILATERAL",
  "MULTILATERAL - Accord entre Etats",
  "MULTILATERAL - Accord intergouvernemental",
  "MULTILATERAL - Arrangement administratif",
  "MULTILATERAL - AUTRE ACCORD",
  "MULTILATERAL - Autre accord",
] as const

export const auditFrenchDateString = auditChain(
  auditTrimString,
  auditTest(
    (date: string) => frenchDateRegExp.test(date) !== null,
    "Invalid date",
  ),
)

async function downloadTreaties({
  resume,
}: { resume?: string } = {}): Promise<number> {
  let skip = resume !== undefined

  const dataDir = path.join(
    "..",
    "..",
    "..",
    "traites_et_accords_internationaux",
  )
  await fs.ensureDir(dataDir)
  const treatiesListFilePath = path.join(
    dataDir,
    `traites_et_accords_internationaux.csv`,
  )
  const treatiesListStream = fs.createReadStream(treatiesListFilePath, {
    encoding: "utf-8",
  })
  const treatiesList = (await new Promise((resolve, reject) => {
    Papa.parse(treatiesListStream, {
      header: true,
      complete(results, file) {
        resolve(results.data as TreatyListItem[])
      },
      error(err, file) {
        reject(err)
      },
    })
  })) as TreatyListItem[]
  const treatiesId = treatiesList.map(({ Numéro: id }) => id)

  const browser = await chromium.launch()
  const page = await browser.newPage({ ignoreHTTPSErrors: true })
  for (const treatyId of treatiesId) {
    if (skip) {
      if (treatyId === resume) {
        skip = false
        console.log(`Resuming at treaty ${resume}…`)
      } else {
        continue
      }
    }

    const url = `https://basedoc.diplomatie.gouv.fr/exl-php/vue-consult/mae_internet___traites/${treatyId}`
    await page.goto(url)
    const frameLocator = page.frameLocator('css=[name="query"]')
    await expect(
      frameLocator.locator(`css=div[title="N° de l'accord"]`),
    ).toBeVisible()
    assert.strictEqual(
      await frameLocator
        .locator(`css=div[title="N° de l'accord"]`)
        .textContent(),
      treatyId,
    )

    const title = (
      await frameLocator
        .locator(`css=div[title="Titre de l'accord"]`)
        .textContent()
    )?.trim()
    console.log(treatyId, title)

    const rows = (
      await frameLocator
        .locator(`css=div.tabs16_body>table>tbody>tr`)
        .evaluateAll((trs) =>
          (trs as HTMLElement[]).map((tr) =>
            [...tr.getElementsByTagName("td")].map((td) => td.innerText),
          ),
        )
    ).filter((row) => {
      if (
        row.length === 2 &&
        row[0] === "" &&
        ["", "\n", "Enregistrement du Traité à l'ONU"].includes(row[1])
      ) {
        return false
      }
      if (
        row.length === 2 &&
        row[0] === " " &&
        row[1] === "Aucune pièce associée"
      ) {
        return false
      }
      if (
        row.length === 2 &&
        row[0] === "Imprimer les pièces" &&
        row[1] === ""
      ) {
        return false
      }
      if (row.length === 2 && row[0] === "Note juridiique") {
        row[0] = "Note juridique"
        return true
      }
      if (
        row.length === 3 &&
        [
          "Etat des ratifications",
          "Signé par l', le, la, les",
          "Texte de l'accord",
          "Texte et annexe(s)",
        ].includes(row[0]) &&
        row[1] === row[2]
      ) {
        row.splice(2, 1)
        return true
      }
      if (
        row.length === 4 &&
        row[0] === "Envoi par courriel" &&
        row[1] === "" &&
        row[2] === "Cliquer ici pour envoyer ce lien par messagerie" &&
        row[1] === ""
      ) {
        return false
      }
      return true
    })
    const [treaty, error] = auditChain(
      auditTest(
        (rows: string[][]) => rows.every((row: string[]) => row.length === 2),
        (rows: string[]) => `Unexpected length for some rows instead of 2`,
      ),
      (audit: Audit, rows: [string, string][]): [unknown, unknown] => {
        const errors: Record<string, unknown> = {}
        const keys = new Set<string>()
        let lastKey: string | undefined = undefined
        for (const [index, [key, value]] of rows.entries()) {
          if (key === "") {
            if (
              value !== "Ce traité n'est plus en vigueur." &&
              value.match(denonciationParLaFranceRegExp) === null &&
              value.match(entreeEnVigueurEnFranceRegExp) === null
            ) {
              errors[index] =
                lastKey === undefined
                  ? `Continuation line without previous field`
                  : `Continuation line for unexpected field: ${lastKey}`
            }
            continue
          }
          lastKey = key
          if (keys.has(key)) {
            errors[index] = `Duplicate key: ${key}`
          } else {
            keys.add(key)
          }
        }
        return [rows, Object.keys(errors).length === 0 ? null : errors]
      },
      auditFunction((rows: [string, string][]) => {
        let lastKey: string | undefined = undefined
        const treaty: Record<string, unknown> = {}
        for (const [key, value] of rows) {
          if (key === "") {
            if (value === "Ce traité n'est plus en vigueur.") {
              treaty["Ce traité n'est plus en vigueur"] = true
            } else {
              const match = value.match(denonciationParLaFranceRegExp)
              if (match !== null) {
                treaty["Dénonciation par la France"] = match[1]
              } else {
                const match = value.match(entreeEnVigueurEnFranceRegExp)
                if (match !== null) {
                  treaty["Entré(e) en vigueur en France"] = match[1]
                } else {
                  throw new Error(`Unexpected value with blank key: ${value}`)
                }
              }
            }
          } else {
            lastKey = key
            treaty[key] = value
          }
        }
        return treaty
      }),
      auditFunction((treaty: { [key: string]: unknown }) => {
        const pieces: { [id: string]: string } = {}
        for (const [key, value] of Object.entries(treaty)) {
          const match = key.match(pieceRegExp)
          if (match !== null) {
            pieces[match[1]] = value as string
            delete treaty[key]
          }
        }
        if (Object.keys(pieces).length > 0) {
          treaty["Pièces"] = pieces
        }
        return treaty
      }),
      auditPartial({
        Numéro: auditSetNullish(treatyId),
        Titre: auditSetNullish(title),
        "Titre (DE)": [auditTrimString, auditEmptyToNull],
        "Titre (EN)": [auditTrimString, auditEmptyToNull],
        "Titre (SP)": [auditTrimString, auditEmptyToNull],
        "Type de traité": [auditOptions(treatyTypes), auditRequire],
        "Date d'adoption": auditFrenchDateString,
        "Entré(e) en vigueur le": auditFrenchDateString,
        "Date de fin de vigueur": auditFrenchDateString,
        "Ce traité n'est plus en vigueur": auditOptions([true]),
        "Signé(e) par la France, le": auditFrenchDateString,
        "Signataire(s) français": [auditTrimString, auditEmptyToNull],
        "Signé par l', le, la, les": [auditTrimString, auditEmptyToNull],
        "Lieu d'adoption ou de signature": [auditTrimString, auditEmptyToNull],
        "Ratification par la France": [auditTrimString, auditEmptyToNull],
        "Entré(e) en vigueur en France": auditFrenchDateString,
        Dépositaire: [auditTrimString, auditEmptyToNull],
        // Often but not always an URL:
        "Accéder au site du dépositaire": [auditTrimString, auditEmptyToNull],
        Parties: [auditTrimString, auditEmptyToNull],
        "Modifie, remplace, abroge": [auditTrimString, auditEmptyToNull],
        "Modifié, remplacé, abrogé par": [auditTrimString, auditEmptyToNull],
        "Note juridique": [auditTrimString, auditEmptyToNull],
        "Texte de l'accord": [auditTrimString, auditEmptyToNull],
        "Texte et annexe(s)": [auditTrimString, auditEmptyToNull],
        "Conditions d'entrée en vigueur": [auditTrimString, auditEmptyToNull],
        "Conditions de durée": [auditTrimString, auditEmptyToNull],
        "Dénonciation par la France": auditFrenchDateString,
        "Publication au JORF": [
          auditTrimString,
          auditFunction((value) =>
            value
              .replace("Décret n° 20156448", "Décret n° 2015-448")
              .replace("Décret n° 201661651", "Décret n° 2016-1651")
              .replace("Décret n° 2016- 1776", "Décret n° 2016-1776")
              .replace("Décret n° 201961334", "Décret n° 2019-1334")
              .replace("Décret n° 20206842", "Décret n° 2020-842")
              .replace("Décret n° 70--1334", "Décret n° 70-1334")
              .replace("Décret n° 74-360||74_360", "Décret n° 74-360")
              .replace("Décret n° 861277", "Décret n° 86-1277")
              .replace("Décret n° 90-639,19900717", "Décret n° 90-639")
              .replace("Décret n° 99,305", "Décret n° 99-305")
              .replace(
                "Décret n° a du 12/10/2020",
                "Décret n° 2020-1248 du 12/10/2020",
              )
              .replace("Décret n° Ne pas publier", "")
              .replace("Décret n° pas de publication requise", "")
              .replace("Décret n° n° ", "Décret n° "),
          ),
          auditEmptyToNull,
          auditTest(
            (value) =>
              value.match(/^Décret n° \d+-\d+(\|\|\d+-\d+)?( |$)/) !== null,
            "Invalid decree reference",
          ),
        ],
        // Often an URL, but not always
        "Publication autre que JORF": [auditTrimString, auditEmptyToNull],
        Rectificatif: [auditTrimString, auditEmptyToNull],
        "Certificat ONU": [auditTrimString, auditEmptyToNull],
        Enregistrement: [auditTrimString, auditEmptyToNull],
        Pièces: auditKeyValueDictionary(
          auditTest(
            (id) => id.match(pieceNumberRegExp) !== null,
            "Invalid piece number",
          ),
          [auditTrimString, auditEmptyToNull, auditRequire],
        ),
        Bibliographie: [auditTrimString, auditEmptyToNull],
        "Contenu du dossier": [auditTrimString, auditEmptyToNull],
        Observations: [auditTrimString, auditEmptyToNull],
        Thématique: [auditTrimString, auditEmptyToNull],
      }),
      auditMatch(
        (treaty) => (treaty as Treaty)["Type de traité"].split("-")[0].trim(),
        {
          BILATERAL: auditObject({}, { ignore: treatyBaseKeys }),
          EUROPEEN: auditObject({}, { ignore: treatyBaseKeys }),
          MULTILATERAL: auditObject(
            {
              "Etat des ratifications": [auditTrimString, auditEmptyToNull],
            },
            { ignore: treatyBaseKeys },
          ),
        },
      ),
    )(strictAudit, rows)
    if (error !== null) {
      console.error(treaty, error)
      continue
    }

    await fs.writeJson(path.join(dataDir, `${treatyId}.json`), treaty, {
      encoding: "utf-8",
      spaces: 2,
    })
  }
  await browser.close()

  return 0
}

sade("download_traites_et_accords_internationaux", true)
  .describe(
    "Download the details of each treaty of « Traités et accords de la France » from the website of Ministère des affaires étrangères",
  )
  .option("-r, --resume", "Resume download at given treaty number")
  .example("--resume TRA00000297")
  .action(async (options) => {
    await downloadTreaties(options)
    process.exit(0)
  })
  .parse(process.argv)
