import assert from "assert"
import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures } from "@leximpact/echeancier-common"
import {
  dossierAssembleeHtmlFragmentFromTemplate,
  dossierAssembleeIFrameStyle,
  echeancierHtmlFragmentFromTemplate,
  fonctionnementHtmlFragmentFromTemplate,
  iFrameResizerScript,
  indexHtmlFragmentFromTemplate,
  standardIFrameStyle,
  statistiquesHtmlFragmentFromTemplate,
} from "@leximpact/echeancier-common/server"

import config from "$lib/server/config"

const { echeancierDataDir } = config
const frontOfficeUrl = process.argv.at(-2) as string
assert.notStrictEqual(frontOfficeUrl, undefined)
assert.notStrictEqual(frontOfficeUrl.match(/^https?:\/\//), null)
const fragmentsDir = process.argv.at(-1) as string
assert.notStrictEqual(fragmentsDir, undefined)
fs.removeSync(fragmentsDir)
fs.ensureDirSync(fragmentsDir)
const fragmentsAssetsDir = path.join(
  fragmentsDir,
  "_app",
  "immutable",
  "assets",
)
fs.ensureDirSync(fragmentsAssetsDir)

const buildDir = "build"
const embeddedDir = path.join(buildDir, "embedded")
const fragmentsEmbeddedDir = path.join(fragmentsDir, "embedded")
fs.ensureDirSync(fragmentsEmbeddedDir)

// Page index

fs.writeFileSync(
  path.join(fragmentsAssetsDir, "index.css"),
  standardIFrameStyle,
  {
    encoding: "utf-8",
  },
)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "index.js"),
  iFrameResizerScript,
  { encoding: "utf-8" },
)
{
  const html = fs.readFileSync(path.join(embeddedDir + ".html"), {
    encoding: "utf-8",
  })
  const echeancierHtml = indexHtmlFragmentFromTemplate({
    embeddedPage: html,
    frontOfficeUrl: frontOfficeUrl,
  })
  fs.writeFileSync(
    path.join(fragmentsEmbeddedDir + ".fragment.html"),
    echeancierHtml,
    {
      encoding: "utf-8",
    },
  )
}

// Fonctionnement

fs.writeFileSync(
  path.join(fragmentsAssetsDir, "fonctionnement.css"),
  standardIFrameStyle,
  { encoding: "utf-8" },
)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "fonctionnement.js"),
  iFrameResizerScript,
  { encoding: "utf-8" },
)
{
  const html = fs.readFileSync(path.join(embeddedDir, "fonctionnement.html"), {
    encoding: "utf-8",
  })
  const echeancierHtml = fonctionnementHtmlFragmentFromTemplate({
    embeddedPage: html,
    frontOfficeUrl: frontOfficeUrl,
  })
  fs.writeFileSync(
    path.join(fragmentsEmbeddedDir, "fonctionnement.fragment.html"),
    echeancierHtml,
    { encoding: "utf-8" },
  )
}

// Statistiques

fs.writeFileSync(
  path.join(fragmentsAssetsDir, "statistiques.css"),
  standardIFrameStyle,
  { encoding: "utf-8" },
)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "statistiques.js"),
  iFrameResizerScript,
  { encoding: "utf-8" },
)
{
  const html = fs.readFileSync(path.join(embeddedDir, "statistiques.html"), {
    encoding: "utf-8",
  })
  const echeancierHtml = statistiquesHtmlFragmentFromTemplate({
    embeddedPage: html,
    frontOfficeUrl: frontOfficeUrl,
  })
  fs.writeFileSync(
    path.join(fragmentsEmbeddedDir, "statistiques.fragment.html"),
    echeancierHtml,
    { encoding: "utf-8" },
  )
}

// Échéanciers des lois en dehors des dossiers législatifs

const embeddedFilenames = fs.readdirSync(embeddedDir)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "echeancier.css"),
  standardIFrameStyle,
  {
    encoding: "utf-8",
  },
)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "echeancier.js"),
  iFrameResizerScript,
  { encoding: "utf-8" },
)
for (const filename of embeddedFilenames) {
  if (
    filename.match(/^DLR\d+L\d+N\d+\.html$/) !== null ||
    filename.match(/^JORFTEXT\d{12}\.html$/) !== null
  ) {
    const html = fs.readFileSync(path.join(embeddedDir, filename), {
      encoding: "utf-8",
    })
    const filenameCore = filename.replace(/\.html$/, "")
    const dataFilePath = path.join(
      echeancierDataDir,
      "echeanciers",
      filenameCore + ".json",
    )
    const { dossierAssemblee } = fs.readJsonSync(dataFilePath) as LoiEtMesures
    const echeancierHtml = echeancierHtmlFragmentFromTemplate({
      dossierAssembleeUid: dossierAssemblee?.uid,
      embeddedPage: html,
      frontOfficeUrl,
      loiJorfId: filenameCore, // Caution: filenameCore can have the DLR5L16... syntax => not a LoiJorfId.
    })
    fs.writeFileSync(
      path.join(
        fragmentsEmbeddedDir,
        filename.replace(/\.html$/, ".fragment.html"),
      ),
      echeancierHtml,
      { encoding: "utf-8" },
    )
  }
}

// Échéanciers de chaque dossier législatif

const embeddedDossiersDir = path.join(embeddedDir, "dossiers")
const embeddedDossiersFilenames = fs.readdirSync(embeddedDossiersDir)
const fragmentsEmbeddedDossiersDir = path.join(fragmentsEmbeddedDir, "dossiers")
fs.ensureDirSync(fragmentsEmbeddedDossiersDir)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "echeancier_dossier.css"),
  dossierAssembleeIFrameStyle,
  { encoding: "utf-8" },
)
fs.writeFileSync(
  path.join(fragmentsAssetsDir, "echeancier_dossier.js"),
  iFrameResizerScript,
  { encoding: "utf-8" },
)
for (const filename of embeddedDossiersFilenames) {
  if (
    filename.match(/^DLR\d+L\d+N\d+\.html$/) !== null
    // || filename.match(/^JORFTEXT\d{12}\.html$/) !== null
  ) {
    const html = fs.readFileSync(path.join(embeddedDossiersDir, filename), {
      encoding: "utf-8",
    })
    const filenameCore = filename.replace(/\.html$/, "")
    const dataFilePath = path.join(
      echeancierDataDir,
      "echeanciers",
      filenameCore + ".json",
    )
    const { dossierAssemblee } = fs.readJsonSync(dataFilePath) as LoiEtMesures
    if (dossierAssemblee === undefined) {
      continue
    }
    const echeancierHtml = dossierAssembleeHtmlFragmentFromTemplate({
      dossierAssemblee,
      embeddedPage: html,
      frontOfficeUrl: frontOfficeUrl,
    })
    fs.writeFileSync(
      path.join(
        fragmentsEmbeddedDossiersDir,
        filename.replace(/\.html$/, ".fragment.html"),
      ),
      echeancierHtml,
      { encoding: "utf-8" },
    )
  }
}
