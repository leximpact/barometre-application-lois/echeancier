import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

const { echeancierDataDir } = config

const { commissionByUid, lois } = (await fs.readJson(
  path.join(echeancierDataDir, "echeanciers", "index.json"),
)) as MesuresData

const data = [
  [
    "ID loi JORF",
    "Titre loi JORF",
    "Titre complet loi JORF",
    "ID dossier DOLE",
    "Titre dossier DOLE",
    "UID dossier Assemblée",
    "Chemin dossier Assemblée",
    "UID commission fond",
    "Nom commission",
    "UID trouvé commission",
    "Nom trouvé commission",
    "URL Baromètre",
  ],
]
for (const {
  dossierAssembleeExtrait,
  loiJorfId,
  titreComplet,
} of lois.toSorted(
  (
    { datePublication: datePublication1, loiJorfId: loiJorfId1 },
    { datePublication: datePublication2, loiJorfId: loiJorfId2 },
  ) =>
    `${datePublication1}-${loiJorfId1}`.localeCompare(
      `${datePublication2}-${loiJorfId2}`,
    ),
)) {
  const { dossier, texteVersion } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", `${loiJorfId}.json`),
  )) as LoiEtMesures
  let commissionFondTrouveeUid = dossierAssembleeExtrait?.commissionFondUid
  if (commissionFondTrouveeUid === undefined) {
    const autorisationAccordInternational =
      titreComplet !== undefined &&
      [
        /\s+autorisant\s+l'accession\s+/i,
        /\s+autorisant\s+l'adhésion\s+/i,
        /\s+autorisant\s+l'approbation\s+/i,
        /\s+autorisant\s+la\s+ratification\s+/i,
      ].some((regExp) => titreComplet.match(regExp) !== null)
    if (autorisationAccordInternational) {
      commissionFondTrouveeUid = "PO59047"
    }
  }
  data.push([
    loiJorfId,
    texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITRE ?? "-",
    titreComplet ?? "-",
    dossier?.META.META_COMMUN.ID ?? "-",
    dossier?.META.META_DOSSIER_LEGISLATIF.TITRE ?? "-",
    dossierAssembleeExtrait?.uid ?? "-",
    dossierAssembleeExtrait?.titreChemin ?? "-",
    dossierAssembleeExtrait?.commissionFondUid ?? "",
    commissionByUid[dossierAssembleeExtrait?.commissionFondUid ?? ""]
      ?.libelleAbrege ?? "-",
    commissionFondTrouveeUid ?? "-",
    commissionByUid[commissionFondTrouveeUid ?? ""]?.libelleAbrege ?? "-",
    `https://barometre.leximpact.dev/${
      dossierAssembleeExtrait?.uid ?? loiJorfId
    }`,
  ])
}
console.log(Papa.unparse(data))

process.exit(0)
