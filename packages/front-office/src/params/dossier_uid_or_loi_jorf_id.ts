import type { ParamMatcher } from "@sveltejs/kit"

export const match: ParamMatcher = (param) =>
  /^DLR\d+L\d+N\d+$/.test(param) || /^JORFTEXT\d{12}$/.test(param)
