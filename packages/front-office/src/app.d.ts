import "unplugin-icons/types/svelte"

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    // interface PageData {}
    // interface Platform {}

    interface ContainerUrls {
      dossiers?: string | undefined
      echeanciers?: string | undefined
      fonctionnement?: string | undefined
      index?: string | undefined
      statistiques?: string | undefined
    }
  }
}

export {}
