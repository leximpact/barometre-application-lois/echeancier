import { browser } from "$app/environment"

const CATEGORY_FILTRES = "Filtres"

// const ACTION_FILTRES_RECHERCHE = "Recherche textuelle"
const ACTION_FILTRES_LEGISLATURES = "Législatures"
const ACTION_FILTRES_ANNEES = "Années"
const ACTION_FILTRES_DATES = "Dates"
const ACTION_FILTRES_COMMISSION = "Commission"
const ACTION_FILTRES_ETATS_STATUTS = "États et statuts"

function trackEvent(category: string, action: string, name: string) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push(["trackEvent", category, action, name])
  }
}

/**
 * Filters tracking - barre de filtres
 */

function trackRecherche(query: string, category: string) {
  if (browser) {
    // @ts-expect-error: global variable needed by Matomo.
    window._paq ??= []
    // @ts-expect-error: global variable needed by Matomo.
    const _paq = window._paq
    _paq.push([
      "setCustomUrl",
      encodeURI(`?search_query=${query}&search_category=${category}`),
    ])
    _paq.push(["trackPageView"])
  }
}

export function trackFiltresRecherche(query: string | undefined) {
  if (query === undefined) {
    return
  }

  trackRecherche(query, CATEGORY_FILTRES)
}

export function trackFiltresLegislatures(
  debutLegislature: string | undefined,
  finLegislature: string | undefined,
) {
  if (debutLegislature === undefined || finLegislature === undefined) {
    return
  }

  trackEvent(
    CATEGORY_FILTRES,
    ACTION_FILTRES_LEGISLATURES,
    debutLegislature === finLegislature
      ? debutLegislature
      : `${debutLegislature} - ${finLegislature}`,
  )
}

export function trackFiltresAnnees(
  debutAnnee: string | undefined,
  finAnnee: string | undefined,
) {
  if (debutAnnee === undefined || finAnnee === undefined) {
    return
  }

  trackEvent(
    CATEGORY_FILTRES,
    ACTION_FILTRES_ANNEES,
    debutAnnee === finAnnee ? debutAnnee : `${debutAnnee} - ${finAnnee}`,
  )
}

export function trackFiltresDates(
  debutDate: string | undefined,
  finDate: string | undefined,
) {
  if (debutDate === undefined || finDate === undefined) {
    return
  }
  trackEvent(
    CATEGORY_FILTRES,
    ACTION_FILTRES_DATES,
    `${debutDate.replaceAll("-", "/")} - ${finDate.replaceAll("-", "/")}`,
  )
}

export function trackFiltresCommission(commission: string) {
  trackEvent(CATEGORY_FILTRES, ACTION_FILTRES_COMMISSION, commission)
}

export function trackFiltresEtatsTypes(
  etats: string[] | undefined,
  types: string[] | undefined,
) {
  if (
    (etats === undefined || etats.length === 0) &&
    (types === undefined || types.length === 0)
  ) {
    return
  }
  const etatsString =
    etats === undefined
      ? "tous"
      : etats.length === 0
        ? "aucun"
        : etats
            .map((etat) => etat.toLowerCase())
            .sort()
            .join(", ")
  const typesString =
    types === undefined
      ? "tous"
      : types.length === 0
        ? "aucun"
        : types
            .map((type) => type.toLowerCase())
            .sort()
            .join(", ")
  trackEvent(
    CATEGORY_FILTRES,
    ACTION_FILTRES_ETATS_STATUTS,
    `États : ${etatsString}. Types : ${typesString}.`,
  )
}
