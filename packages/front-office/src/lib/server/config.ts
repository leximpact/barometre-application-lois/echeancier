import "dotenv/config"

import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  allowRobots: boolean
  backOffice: {
    url: string
  }
  classificationsFilename: string
  containerUrls: App.ContainerUrls
  echeancierDataDir: string
  matomo?: {
    prependDomain?: boolean
    siteId: number
    subdomains?: string
    url: string
  }
  powerMode: boolean
  title: string
}

const [config, error] = validateConfig({
  allowRobots: process.env["ALLOW_ROBOTS"],
  backOffice: {
    url: process.env["BACK_OFFICE_URL"],
  },
  classificationsFilename: process.env.CLASSIFICATIONS_FILENAME,
  containerUrls: {
    dossiers: process.env.CONTAINER_DOSSIERS_URL,
    echeanciers: process.env.CONTAINER_ECHEANCIERS_URL,
    fonctionnement: process.env.CONTAINER_FONCTIONNEMENT_URL,
    index: process.env.CONTAINER_INDEX_URL,
    statistiques: process.env.CONTAINER_STATISTIQUES_URL,
  },
  echeancierDataDir: process.env.ECHEANCIER_DATA_DIR,
  matomo:
    process.env["MATOMO_SITE_ID"] && process.env["MATOMO_URL"]
      ? {
          prependDomain: process.env["MATOMO_PREPEND_DOMAIN"],
          siteId: process.env["MATOMO_SITE_ID"],
          subdomains: process.env["MATOMO_SUBDOMAINS"],
          url: process.env["MATOMO_URL"],
        }
      : null,
  powerMode: process.env.POWER_MODE,
  title: process.env.TITLE,
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
export default config
