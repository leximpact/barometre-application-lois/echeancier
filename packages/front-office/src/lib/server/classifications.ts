import fs from "fs-extra"
import path from "path"

import type { Classification } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

const { classificationsFilename, echeancierDataDir } = config
const classificationsFilePath = path.join(
  echeancierDataDir,
  classificationsFilename,
)

export const classificationByDecret: { [decret: string]: Classification } =
  Object.fromEntries(
    (
      ((await fs.pathExists(classificationsFilePath))
        ? await fs.readJson(classificationsFilePath)
        : []) as Classification[]
    ).map((classification) => [classification.decret, classification]),
  )
