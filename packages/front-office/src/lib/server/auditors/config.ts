import {
  auditBoolean,
  auditInteger,
  auditRequire,
  auditSetNullish,
  auditStringToBoolean,
  auditStringToNumber,
  auditSwitch,
  auditTrimString,
  cleanAudit,
  auditHttpUrl,
  auditFunction,
  type Audit,
  auditEmptyToNull,
} from "@auditors/core"

function auditBackOfficeConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "allowRobots",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  audit.attribute(
    data,
    "backOffice",
    true,
    errors,
    remainingKeys,
    auditBackOfficeConfig,
    auditRequire,
  )
  for (const key of ["classificationsFilename", "echeancierDataDir", "title"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "containerUrls",
    true,
    errors,
    remainingKeys,
    auditContainerUrlsConfig,
    auditRequire,
  )
  audit.attribute(data, "matomo", true, errors, remainingKeys, auditMatomo)
  audit.attribute(
    data,
    "powerMode",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditContainerUrlsConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["dossiers", "echeanciers"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditFunction((url) => url.replace(/\/+$/, "") + "/"),
    )
  }
  for (const key of ["fonctionnement", "index", "statistiques"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditHttpUrl)
  }

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

function auditMatomo(audit: Audit, dataUnknown: unknown): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "prependDomain",
    true,
    errors,
    remainingKeys,
    auditSwitch([auditTrimString, auditStringToBoolean], auditBoolean),
    auditSetNullish(false),
  )
  audit.attribute(
    data,
    "siteId",
    true,
    errors,
    remainingKeys,
    auditStringToNumber,
    auditInteger,
    auditRequire,
  )
  audit.attribute(
    data,
    "subdomains",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditEmptyToNull,
  )
  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    // Ensure there is a single trailing "/" in URL.
    auditFunction((url) => url.replace(/\/$/, "") + "/"),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
