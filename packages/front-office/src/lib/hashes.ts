import type { Echeancier } from "@tricoteuses/legal-explorer"
import deepEqual from "deep-equal"

import { statutsLoiMutable, typesLoiMutable } from "$lib/mesures"

export function newDossierLegislatifHash({
  echeancierIndex,
  echeanciers,
}: {
  echeancierIndex?: number
  echeanciers: Echeancier[]
}): string {
  const query: URLSearchParams = new URLSearchParams()
  if (
    echeancierIndex !== undefined &&
    echeancierIndex !== echeanciers.length - 1
  ) {
    query.append("version", echeancierIndex.toString())
  }
  const queryString = query.toString()
  return queryString ? "#" + queryString : ""
}

export function newRootHash(
  {
    commission,
    debut,
    fin,
    firstValidDate,
    lastValidDate,
    search,
    statuts,
    types,
  }: {
    commission?: string
    debut?: Date | string
    fin?: Date | string
    firstValidDate?: string
    lastValidDate?: string
    search?: string
    statuts?: string
    types?: string
  },
  forParentIFrame: boolean,
): string {
  const query: URLSearchParams = new URLSearchParams()
  if (commission !== undefined) {
    query.append("commission", commission)
  }
  if (debut instanceof Date) {
    debut = debut.toISOString().split("T")[0]
  }
  if (debut !== undefined && debut !== firstValidDate) {
    query.append("debut", debut)
  }
  if (fin instanceof Date) {
    fin = fin.toISOString().split("T")[0]
  }
  if (fin !== undefined && fin !== lastValidDate) {
    query.append("fin", fin)
  }
  if (search !== undefined && search.trim().length > 0) {
    query.append("search", search)
  }
  if (
    statuts !== undefined &&
    !deepEqual(statuts.split(","), statutsLoiMutable)
  ) {
    query.append("statuts", statuts)
  }
  if (types !== undefined && !deepEqual(types.split(","), typesLoiMutable)) {
    query.append("types", types)
  }
  const queryString = query.toString()
  return queryString
    ? "#" +
        (forParentIFrame
          ? queryString.replace(/=/g, ":").replace(/&/g, "~").replace(/,/g, "!")
          : queryString)
    : ""
}
