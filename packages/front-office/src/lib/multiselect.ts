export interface MultiSelectConfig {
  text: string
  checkAllTitle: string
  checkSomeTitle: string
  sections: MultiSelectSection[]
}

export interface MultiSelectSection {
  id: string
  title: string
  values: readonly string[]
  labelsByValues: Map<string | undefined, string>
}

export interface MultiSelectValue {
  [id: string]: string | undefined
}
