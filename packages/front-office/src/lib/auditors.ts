import {
  auditChain,
  auditDateIso8601String,
  auditEmptyToNull,
  auditInteger,
  auditOptions,
  auditRequire,
  auditSingleton,
  auditStringToNumber,
  auditTest,
  auditTrimString,
  type Audit,
  type Auditor,
  auditSetNullish,
} from "@auditors/core"
import type { Organe } from "@tricoteuses/assemblee"
import type { Echeancier } from "@tricoteuses/legal-explorer"

export const auditDossierLegislatifHash = (
  echeanciers: Echeancier[],
): Auditor => {
  return (audit: Audit, hash: unknown): [unknown, unknown] => {
    if (typeof hash !== "string") {
      return audit.unexpectedType(hash, "string")
    }
    const query = new URLSearchParams((hash as string).replace(/^#/, ""))

    const data: { [key: string]: unknown } = {}
    for (const [key, value] of query.entries()) {
      let values = data[key] as string[] | undefined
      if (values === undefined) {
        values = data[key] = []
      }
      values.push(value)
    }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "version",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditChain(
          auditSingleton(
            auditTrimString,
            auditStringToNumber,
            auditInteger,
            auditTest(
              (version: number) => version >= 0,
              "Le numéro de version doit être positif ou nul",
            ),
            auditTest(
              (version: number) => version < echeanciers.length,
              `Le numéro de version doit être strictement inférieur à ${echeanciers.length}`,
            ),
          ),
          auditRequire,
        ),
        echeanciers.length - 1,
      ),
    )

    return audit.reduceRemaining(data, errors, remainingKeys)
  }
}

export const auditIgnoreError = (
  auditor: Auditor,
  errorData: unknown,
): Auditor => {
  return (audit: Audit, data: unknown): [unknown, unknown] => {
    const [value, error] = auditor(audit, data)
    return error === null ? [value, null] : [errorData, null]
  }
}

export const auditRootHash = (
  commissionByUid: { [uid: string]: Organe },
  firstValidDate: string,
  lastValidDate: string,
): Auditor => {
  return (audit: Audit, hash: unknown): [unknown, unknown] => {
    if (typeof hash !== "string") {
      return audit.unexpectedType(hash, "string")
    }
    const query = new URLSearchParams((hash as string).replace(/^#/, ""))

    const data: { [key: string]: unknown } = {}
    for (const [key, value] of query.entries()) {
      let values = data[key] as string[] | undefined
      if (values === undefined) {
        values = data[key] = []
      }
      values.push(value)
    }
    const errors: { [key: string]: unknown } = {}
    const remainingKeys = new Set(Object.keys(data))

    audit.attribute(
      data,
      "commission",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditSingleton(
          auditTrimString,
          auditOptions([
            "AUTRE",
            "CNPS",
            ...Object.values(commissionByUid)
              .filter((commission) => commission.codeType === "COMPER")
              .map(({ uid }) => uid),
          ]),
        ),
        null,
      ),
    )
    audit.attribute(
      data,
      "debut",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditChain(auditSingleton(auditDateIso8601String), auditRequire),
        firstValidDate,
      ),
    )
    audit.attribute(
      data,
      "fin",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditChain(auditSingleton(auditDateIso8601String), auditRequire),
        lastValidDate,
      ),
    )
    audit.attribute(
      data,
      "search",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditSingleton(auditTrimString, auditEmptyToNull),
        undefined,
      ),
    )
    audit.attribute(
      data,
      "statuts",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditSingleton(auditTrimString, auditEmptyToNull),
        undefined,
      ),
    )
    audit.attribute(
      data,
      "types",
      true,
      errors,
      remainingKeys,
      auditIgnoreError(
        auditSingleton(auditTrimString, auditEmptyToNull),
        undefined,
      ),
    )

    return audit.reduceRemaining(
      data,
      errors,
      remainingKeys,
      auditSetNullish({}),
    )
  }
}
