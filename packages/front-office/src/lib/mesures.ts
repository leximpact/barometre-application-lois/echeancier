import type { Organe } from "@tricoteuses/assemblee"

import {
  iterLois,
  type EtatLigne,
  type Loi,
  type LoiContext,
} from "@leximpact/echeancier-common"

export interface EcheancierLigneContext extends LoiContext {
  delaiApplication?: number
  etat: EtatLigne
}

export type StatutLoi = (typeof statutsLoi)[number]

export const labelByStatutLoi = new Map<StatutLoi | undefined, string>([
  ["application_directe", "Lois d'application directe"],
  ["totalement_appliquee", "Lois totalement appliquées"],
  ["non_appliquee", "Lois non appliquées"],
  ["partiellement_appliquee", "Lois partiellement appliquées"],
  ["sans_echeancier", "Lois sans échéancier"],
])

export const statutsLoi = [
  "application_directe",
  "totalement_appliquee",
  "partiellement_appliquee",
  "non_appliquee",
  "sans_echeancier",
] as const
export const statutsLoiMutable = [...statutsLoi]

export type TypeLoi = (typeof typesLoi)[number]

export const labelByTypeLoi = new Map<TypeLoi | undefined, string>([
  [
    "autorisation_accord_international",
    "Lois autorisant la ratification d’accords internationaux",
  ],
  ["habilitation_ordonnances", "Lois d’habilitation (ordonnances)"],
  ["ratification_ordonnances", "Lois de ratification (ordonnances)"],
  ["autre", "Autres lois"],
])

export const typesLoi = [
  "autorisation_accord_international",
  "habilitation_ordonnances",
  "ratification_ordonnances",
  "autre",
] as const
export const typesLoiMutable = [...typesLoi]

export function classBgFromRate(rate: number): string {
  const ratio = Math.round(rate * 100) / 100
  return ratio === 1
    ? "bg-[#D2EEBD]"
    : ratio >= 0.9
      ? "bg-[#E1F0C3]"
      : ratio >= 0.8
        ? "bg-[#FEF4CE]"
        : ratio >= 0.7
          ? "bg-[#FFECC7]"
          : ratio >= 0.6
            ? "bg-[#FFE4C2]"
            : ratio >= 0.5
              ? "bg-[#FBD4C2]"
              : ratio >= 0.4
                ? "bg-[#F9CDC2]"
                : ratio >= 0.3
                  ? "bg-[#F9CCC2]"
                  : ratio >= 0.2
                    ? "bg-[#F7C5C2]"
                    : ratio >= 0.1
                      ? "bg-[#EFBBBB]"
                      : ratio > 0
                        ? "bg-[#E9B2B9]"
                        : "bg-[#E9B2B9]"
}

export function classBgDarkFromRate(rate: number): string {
  const ratio = Math.round(rate * 100) / 100
  return ratio === 1
    ? "bg-[#6BC427]"
    : ratio >= 0.9
      ? "bg-[#A2D143]"
      : ratio >= 0.8
        ? "bg-[#F9CC25]"
        : ratio >= 0.7
          ? "bg-[#F9B42E]"
          : ratio >= 0.6
            ? "bg-[#F6A43D]"
            : ratio >= 0.5
              ? "bg-[#F8793F]"
              : ratio >= 0.4
                ? "bg-[#FA6945]"
                : ratio >= 0.3
                  ? "bg-[#F4694B]"
                  : ratio >= 0.2
                    ? "bg-[#EF645B]"
                    : ratio >= 0.1
                      ? "bg-[#F65765]"
                      : ratio > 0
                        ? "bg-[#E14356]"
                        : "bg-[#E14356]"
}

export function classTextFromRate(rate: number): string {
  const ratio = Math.round(rate * 100) / 100
  return ratio === 1
    ? "text-[#556e2d]"
    : ratio >= 0.9
      ? "text-[#556e2d]"
      : ratio >= 0.8
        ? "text-[#7f6e2f]"
        : ratio >= 0.7
          ? "text-[#836832]"
          : ratio >= 0.6
            ? "text-[#8a602e]"
            : ratio >= 0.5
              ? "text-[#89552b]"
              : ratio >= 0.4
                ? "text-[#99432e]"
                : ratio >= 0.3
                  ? "text-[#99432e]"
                  : ratio >= 0.2
                    ? "text-[#9c392f]"
                    : ratio >= 0.1
                      ? "text-[#9e2d30]"
                      : ratio > 0
                        ? "text-[#883131]"
                        : "text-[#883131]"
}

export function classTextSmallFromRate(rate: number): string {
  const ratio = Math.round(rate * 100) / 100
  return ratio === 1
    ? "text-[#424c2b]"
    : ratio >= 0.9
      ? "text-[#424c2b]"
      : ratio >= 0.8
        ? "text-[#5d522b]"
        : ratio >= 0.7
          ? "text-[#5d4d2b]"
          : ratio >= 0.6
            ? "text-[#604728]"
            : ratio >= 0.5
              ? "text-[#5c3e25]"
              : ratio >= 0.4
                ? "text-[#663325]"
                : ratio >= 0.3
                  ? "text-[#653325]"
                  : ratio >= 0.2
                    ? "text-[#652c25]"
                    : ratio >= 0.1
                      ? "text-[#5b2726]"
                      : ratio > 0
                        ? "text-[#522322]"
                        : "text-[#522322]"
}

export function* iterEcheanciersLignes({
  gouvernements,
  lois,
  presidences,
}: {
  gouvernements: Organe[]
  lois: Loi[]
  presidences: Organe[]
}): Generator<EcheancierLigneContext, void, unknown> {
  for (const loiContext of iterLois({
    gouvernements,
    lois,
    presidences,
  })) {
    for (const mesure of loiContext.loi.mesures ?? []) {
      yield {
        ...loiContext,
        ...mesure,
      }
    }
  }
}
