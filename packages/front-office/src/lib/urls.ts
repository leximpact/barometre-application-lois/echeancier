export function legifranceUrlFromId(id: string): string {
  if (id.match(/^JORFARTI\d{12}$/) !== null) {
    return `https://www.legifrance.gouv.fr/jorf/article_jo/${id}`
  } else if (id.match(/^JORFTEXT\d{12}$/) !== null) {
    return `https://www.legifrance.gouv.fr/jorf/id/${id}`
  } else if (id.match(/^LEGIARTI\d{12}$/) !== null) {
    return `https://www.legifrance.gouv.fr/loda/id/${id}`
  } else if (id.match(/^LEGITEXT\d{12}$/) !== null) {
    return `https://www.legifrance.gouv.fr/loda/id/${id}`
  } else {
    console.error(`Can't (yet) generate Légifrance URL of ${id}`)
    return "https://www.legifrance.gouv.fr/TODO"
  }
}
