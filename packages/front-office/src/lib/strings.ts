export const diacritiquesMinuscule: { [letter: string]: string } = {
  ae: "(ae|æ)",
  oe: "(oe|œ)",
  a: "(a|â|ä|à)",
  c: "(c|ç)",
  e: "(e|é|ê|ë|è)",
  i: "(i|î|ï)",
  o: "(o|ô|ö)",
  u: "(u|û|ü|ù)",
  y: "(y|ÿ)",
  "'": "('|‘|’)",
  "‘": "(‘|'|’)",
  "’": "(’|'|‘)",
}
