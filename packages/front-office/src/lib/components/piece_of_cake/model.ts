import { scaleLinear, type ScaleLinear } from "d3-scale"

export class CurveModel {
  public readonly xNice?: boolean | number = undefined
  public readonly xScaler = scaleLinear
  public readonly yNice?: boolean | number = undefined
  public readonly yScaler = scaleLinear

  private _height?: number
  private _valuesScaled?: Array<[number, number]>
  private _width?: number
  private _xDomain?: [number, number]
  private _xRange?: [number, number]
  private _xScale?: ScaleLinear<number, number, never>
  private _xValues?: number[]
  private _xValuesScaled?: number[]
  private _yDomain?: [number, number]
  private _yRange?: [number, number]
  private _yScale?: ScaleLinear<number, number, never>
  private _yValues?: number[]
  private _yValuesScaled?: number[]
  public readonly containerHeight: number = 240
  public readonly containerWidth: number = 320
  public readonly padding: {
    bottom: number
    left: number
    right: number
    top: number
  } = { bottom: 0, left: 0, right: 0, top: 0 }

  constructor(
    public readonly values: Array<[number, number]>,
    {
      containerHeight,
      containerWidth,
      padding,
      xDomain,
      xNice,
      xScaler,
      yDomain,
      yNice,
      yScaler,
    }: {
      containerHeight?: number
      containerWidth?: number
      padding?: {
        bottom: number
        left: number
        right: number
        top: number
      }
      xDomain?: [number, number]
      xNice?: boolean | number
      xScaler?: typeof scaleLinear
      yDomain?: [number, number]
      yNice?: boolean | number
      yScaler?: typeof scaleLinear
    } = {},
  ) {
    if (
      containerHeight !== undefined &&
      containerHeight !== this.containerHeight
    ) {
      this.containerHeight = containerHeight
    }
    if (
      containerWidth !== undefined &&
      containerWidth !== this.containerWidth
    ) {
      this.containerWidth = containerWidth
    }
    if (padding !== undefined && padding !== this.padding) {
      this.padding = padding
    }
    if (xDomain !== undefined) {
      this._xDomain = xDomain
    }
    if (xNice !== undefined) {
      this.xNice = xNice
    }
    this.xScaler = xScaler ?? scaleLinear
    if (yDomain !== undefined) {
      this._yDomain = yDomain
    }
    if (yNice !== undefined) {
      this.yNice = yNice
    }
    this.yScaler = yScaler ?? scaleLinear
  }

  get height(): number {
    if (this._height === undefined) {
      const { bottom, top } = this.padding
      this._height = this.containerHeight - top - bottom
    }
    return this._height
  }

  setContainerDimensions(
    containerWidth: number,
    containerHeight: number,
  ): CurveModel {
    return new CurveModel(this.values, {
      containerHeight,
      containerWidth,
      padding: this.padding,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  setPadding(padding: {
    bottom: number
    left: number
    right: number
    top: number
  }): CurveModel {
    return new CurveModel(this.values, {
      containerHeight: this.containerHeight,
      containerWidth: this.containerWidth,
      padding,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  setValues(values: Array<[number, number]>): CurveModel {
    return new CurveModel(values, {
      containerHeight: this.containerHeight,
      containerWidth: this.containerWidth,
      padding: this.padding,
      xDomain: this.xDomain,
      xNice: this.xNice,
      xScaler: this.xScaler,
      yDomain: this.yDomain,
      yNice: this.yNice,
      yScaler: this.yScaler,
    })
  }

  get valuesScaled(): Array<[number, number]> {
    if (this._valuesScaled === undefined) {
      const xScale = this.xScale
      const yScale = this.yScale
      this._valuesScaled = this.values.map(([x, y]) => [xScale(x), yScale(y)])
    }
    return this._valuesScaled
  }

  get width(): number {
    if (this._width === undefined) {
      const { left, right } = this.padding
      this._width = this.containerWidth - left - right
    }
    return this._width
  }

  get xDomain(): [number, number] {
    if (this._xDomain === undefined) {
      this._xDomain = this.xValues.reduce(
        ([min, max]: [number | null, number | null], x): [number, number] => {
          if (max === null || max < x) {
            max = x
          }
          if (min === null || min > x) {
            min = x
          }
          return [min, max]
        },
        [null, null],
      ) as [number, number]
    }
    return this._xDomain
  }

  get xRange() {
    if (this._xRange === undefined) {
      this._xRange = [0, this.width]
    }
    return this._xRange
  }

  get xScale() {
    if (this._xScale === undefined) {
      const xScale = this.xScaler().domain(this.xDomain).range(this.xRange)
      if (this.xNice !== undefined && this.xNice !== false) {
        if (typeof xScale.nice === "function") {
          xScale.nice(typeof this.xNice === "number" ? this.xNice : undefined)
        } else {
          console.error(
            "[Piece of Cake] Can't use `xNice`, because `xScale` doesn't have a `nice` method. Ignoring…",
          )
        }
      }
      this._xScale = xScale
    }
    return this._xScale
  }

  get xValues(): number[] {
    if (this._xValues === undefined) {
      this._xValues = this.values.map(([x]) => x)
    }
    return this._xValues
  }

  get xValuesScaled(): number[] {
    if (this._xValuesScaled === undefined) {
      const xScale = this.xScale
      this._xValuesScaled = this.xValues.map((x) => xScale(x))
    }
    return this._xValuesScaled
  }

  get yDomain(): [number, number] {
    if (this._yDomain === undefined) {
      this._yDomain = this.yValues.reduce(
        ([min, max]: [number | null, number | null], y): [number, number] => {
          if (max === null || max < y) {
            max = y
          }
          if (min === null || min > y) {
            min = y
          }
          return [min, max]
        },
        [null, null],
      ) as [number, number]
    }
    return this._yDomain
  }

  get yRange() {
    if (this._yRange === undefined) {
      this._yRange = [this.height, 0]
    }
    return this._yRange
  }

  get yScale() {
    if (this._yScale === undefined) {
      const yScale = this.yScaler().domain(this.yDomain).range(this.yRange)
      if (this.yNice !== undefined && this.yNice !== false) {
        if (typeof yScale.nice === "function") {
          yScale.nice(typeof this.yNice === "number" ? this.yNice : undefined)
        } else {
          console.error(
            "[Piece of Cake] Can't use `yNice`, because `yScale` doesn't have a `nice` method. Ignoring…",
          )
        }
      }
      this._yScale = yScale
    }
    return this._yScale
  }

  get yValues(): number[] {
    if (this._yValues === undefined) {
      this._yValues = this.values.map(([, y]) => y)
    }
    return this._yValues
  }

  get yValuesScaled(): number[] {
    if (this._yValuesScaled === undefined) {
      const yScale = this.yScale
      this._yValuesScaled = this.yValues.map((y) => yScale(y))
    }
    return this._yValuesScaled
  }
}
