import { error } from "@sveltejs/kit"
import type { Echeancier } from "@tricoteuses/legal-explorer"
import dedent from "dedent-js"
import fs from "fs-extra"
import path from "path"

import {
  etatFromLigne,
  hashLigne,
  labelByEtatLigne,
  lignesSorted,
  type LoiEtMesures,
  type MesuresData,
} from "@leximpact/echeancier-common"

import { newDossierLegislatifHash } from "$lib/hashes"
import { classificationByDecret } from "$lib/server/classifications"
import config from "$lib/server/config"

import type { EntryGenerator, RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config
const formatDate = new Intl.DateTimeFormat("fr-FR", {
  day: "numeric",
  month: "numeric",
  year: "numeric",
}).format
const pluralRules = new Intl.PluralRules("fr-FR")

export const entries = (async () => {
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  return lois.map(({ dossierAssembleeExtrait, loiJorfId }) => ({
    id: dossierAssembleeExtrait?.uid ?? loiJorfId,
  }))
}) satisfies EntryGenerator

function escapeXml(s: string): string {
  return s
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
  // .replace(/'/g, "&apos;")
}

export const GET: RequestHandler = async ({ params, url }) => {
  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${params.id}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(
      404,
      `Identifiant de loi ou de dossier législatif Assemblée incorrect : ${params.id}`,
    )
  }
  const { echeanciers, texteVersion } = (await fs.readJson(
    loiEtMesuresFilePath,
  )) as LoiEtMesures

  // Note: echeanciers are sorted in reverse chronological order.
  const lastEcheancier = echeanciers[0] as Echeancier

  const entries = echeanciers
    .map((echeancier, echeancierIndex) => {
      // Note: echeanciers are sorted in reverse chronological order.
      const previousLignesHash = new Set(
        (echeanciers[echeancierIndex + 1]?.LIGNE ?? []).map(hashLigne),
      )
      const lignes = lignesSorted(echeancier.LIGNE)
      const lignesChanged = lignes.filter(
        (ligne) => !previousLignesHash.has(hashLigne(ligne)),
      )

      if (lignesChanged.length === 0) {
        return undefined
      }

      const trArray = lignesChanged
        .map((ligne) => {
          const etat = etatFromLigne(classificationByDecret, ligne)
          return dedent`
          <tr>
            <td>
              ${escapeXml(ligne.ARTICLE?.replace("<br/>".trim(), "") ?? "")}
              ${
                ligne.BASE_LEGALE === undefined
                  ? ""
                  : `<br />\nModifie <i>${escapeXml(ligne.BASE_LEGALE)}</i>`
              }
            </td>
            <td>
              ${escapeXml(ligne.OBJET ?? "")}
            </td>
            <td${
              etat === "applique"
                ? ' style="background-color: #D2EEBD; color: #4b911a"'
                : etat === "en_attente_application"
                  ? ' style="background-color: #C8B6CF; color: #4B0B5E"'
                  : ""
            }>
              ${escapeXml(labelByEtatLigne.get(etat) ?? "")}
            </td>
            <td>
              ${escapeXml(ligne.DECRET ?? "")}
            </td>
          </tr>
        `
            .split("\n")
            .join("\n          ")
        })
        .join("\n          ")

      const echeancierUrl = new URL(
        `/${params.id}${newDossierLegislatifHash({
          echeancierIndex: echeanciers.length - 1 - echeancierIndex,
          echeanciers,
        })}`,
        url,
      ).toString()
      return dedent`
      <entry>
        <title>${
          echeancier["@derniere_maj"] === undefined
            ? "Nouvelle version de l'échéancier"
            : `Échéancier du ${formatDate(
                new Date(echeancier["@derniere_maj"]),
              )}`
        }</title>
        <link rel="alternate" type="text/html" href="${escapeXml(
          echeancierUrl,
        )}"/>
        <id>${escapeXml(echeancierUrl)}</id>
        <updated>${escapeXml(
          (echeancier["@derniere_maj"] === undefined
            ? new Date()
            : new Date(echeancier["@derniere_maj"])
          ).toISOString(),
        )}</updated>
        <summary>${lignesChanged.length} ${
          pluralRules.select(lignesChanged.length) === "one"
            ? "mesure"
            : "mesures"
        }.</summary>
        <content type="xhtml">
          <div xmlns="http://www.w3.org/1999/xhtml">
            <table>
              <thead>
                <tr>
                  <th>Article</th>
                  <th>Objet de la mesure</th>
                  <th>Statut</th>
                  <th>Objectif initial de publication / Décrets publiés / Observations</th>
                </tr>
              </thead>
              <tbody>
                ${trArray}
              </tbody>
            </table>
          </div>
        </content>
        <author>
          <name>LexImpact</name>
          <email>leximpact@assemblee-nationale.fr</email>
        </author>
      </entry>
    `
        .split("\n")
        .join("\n  ")
    })
    .filter((entry) => entry !== undefined)

  return new Response(
    dedent`
      <?xml version="1.0" encoding="UTF-8"?>
      <feed xmlns="http://www.w3.org/2005/Atom">
        <title>${escapeXml(
          texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL?.replace(
            /\s*\(\d+\)$/,
            "",
          ) ?? "",
        )}</title>
        <subtitle>Échéancier</subtitle>
        <link href="${escapeXml(url.toString())}" rel="self" />
        <link href="${escapeXml(new URL(`/${params.id}`, url).toString())}" />
        <id>${escapeXml(params.id)}</id>
        <updated>${escapeXml(
          (lastEcheancier?.["@derniere_maj"] === undefined
            ? new Date()
            : new Date(lastEcheancier?.["@derniere_maj"])
          ).toISOString(),
        )}</updated>
        ${entries}
      </feed>
    `,
    { headers: { "Content-Type": "application/xml" } },
  )
}
