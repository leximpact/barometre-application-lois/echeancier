import type { MesuresData } from "@leximpact/echeancier-common"
import fs from "fs-extra"
import path from "path"

import config from "$lib/server/config"

const { echeancierDataDir } = config

export const load = async () =>
  (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
