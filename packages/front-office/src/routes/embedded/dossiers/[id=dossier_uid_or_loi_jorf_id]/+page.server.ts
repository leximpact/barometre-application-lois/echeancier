import { error } from "@sveltejs/kit"
import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import { classificationByDecret } from "$lib/server/classifications"
import config from "$lib/server/config"

import type { EntryGenerator, PageServerLoad } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config

export const entries = (async () => {
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  return lois.map(({ dossierAssembleeExtrait, loiJorfId }) => ({
    id: dossierAssembleeExtrait?.uid ?? loiJorfId,
  }))
}) satisfies EntryGenerator

export const load: PageServerLoad = async ({ params }) => {
  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${params.id}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(
      404,
      `Identifiant de loi ou de dossier législatif Assemblée incorrect : ${params.id}`,
    )
  }
  const loiEtMesures = (await fs.readJson(loiEtMesuresFilePath)) as LoiEtMesures

  return {
    classificationByDecret,
    loiEtMesures,
  }
}
