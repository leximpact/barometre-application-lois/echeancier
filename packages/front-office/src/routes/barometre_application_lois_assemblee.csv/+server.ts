import { text } from "@sveltejs/kit"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"

import { getOrganeAtDate, type MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

import type { RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config

const csvStringFromBoolean = (value: boolean | undefined): string =>
  value ? "oui" : "non"

export const GET: RequestHandler = async () => {
  const { commissionByUid, gouvernements, lois, presidences } =
    (await fs.readJson(
      path.join(echeancierDataDir, "echeanciers", "index.json"),
    )) as MesuresData

  const loisCsv = [
    [
      "Date de publication de la loi",
      "Titre de la loi",
      "Identifiant Légifrance de la loi",
      "Identifiant Légifrance du dossier législatif ",
      "Identifiant Assemblée du dossier législatif ",
      "Chemin Assemblée du dossier législatif",
      "Identifiant Sénat du dossier législatif",
      "Identifiant de la commission de l'Assemblée saisie au fond",
      "Libellé de la commission de l'Assemblée saisie au fond",
      "Législature",
      "Présidence",
      "Gouvernement",
      "Loi sans échéancier",
      "Loi d'application directe",
      "Loi autorisantt la ratification d'accords internationaux",
      "Loi d'abilitation (ordonnances)",
      "Loi de ratification (ordonnances)",
      "Nombre de mesures à appliquer",
      "Nombre de mesures appliquées",
      "Taux d'application des mesures",
      "Mots-clés",
    ],
  ]
  for (const {
    applicationDirecte,
    autorisationAccordInternational,
    commissionFondAssembleeUid,
    datePublication,
    dossierAssembleeExtrait,
    dossierId,
    habilitationOrdonnances,
    legislature,
    loiJorfId,
    motsCles,
    nombreMesuresAAppliquer,
    nombreMesuresAppliquees,
    ratificationOrdonnances,
    sansEcheancier,
    tauxApplicationMesures,
    titreComplet,
  } of lois.toSorted(
    (
      { datePublication: datePublication1, loiJorfId: loiJorfId1 },
      { datePublication: datePublication2, loiJorfId: loiJorfId2 },
    ) =>
      `${datePublication1}-${loiJorfId1}`.localeCompare(
        `${datePublication2}-${loiJorfId2}`,
      ),
  )) {
    loisCsv.push([
      datePublication,
      titreComplet ?? "",
      loiJorfId,
      dossierId ?? "",
      dossierAssembleeExtrait?.uid ?? "",
      dossierAssembleeExtrait?.titreChemin ?? "",
      dossierAssembleeExtrait?.dossierSenatId ?? "",
      commissionFondAssembleeUid ?? "",
      commissionByUid[commissionFondAssembleeUid ?? ""]?.libelle ?? "",
      legislature.toString(),
      getOrganeAtDate(presidences, datePublication)?.libelle.replace(
        /^Présidence de la République : M(\.|Mme) /,
        "",
      ) ?? "",
      getOrganeAtDate(gouvernements, datePublication)?.libelleAbrege ?? "",
      csvStringFromBoolean(sansEcheancier),
      csvStringFromBoolean(applicationDirecte),
      csvStringFromBoolean(autorisationAccordInternational),
      csvStringFromBoolean(habilitationOrdonnances),
      csvStringFromBoolean(ratificationOrdonnances),
      nombreMesuresAAppliquer.toString(),
      nombreMesuresAppliquees.toString(),
      tauxApplicationMesures.toString(),
      motsCles?.join(",") ?? "",
    ])
  }

  return text(Papa.unparse(loisCsv, { delimiter: ";" }), {
    headers: {
      "Content-Type": "text/csv; charset=utf-8",
      "Content-Disposition": `attachment; filename=barometre_application_lois_${
        new Date().toISOString().split("T")[0]
      }.csv"`,
    },
  })
}
