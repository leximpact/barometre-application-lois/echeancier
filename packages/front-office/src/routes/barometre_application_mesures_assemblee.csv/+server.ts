import { text } from "@sveltejs/kit"
import fs from "fs-extra"
import Papa from "papaparse"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

import type { RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config

export const GET: RequestHandler = async () => {
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData

  const mesuresCsv = [
    [
      "Titre loi",
      "Identifiant loi",
      "N° ordre",
      "Articles",
      "Article 1",
      "Identifiant article 1",
      "Article 2",
      "Identifiant article 2",
      "Article 3",
      "Identifiant article 3",
      "Base légale",
      "Objet",
      "Objectif initial de publication, observations",
      "Identifiant décret",
      "Date prévue",
      "État",
      "Délai d'application (en jours)",
    ],
  ]

  for (const { loiJorfId, mesures, titreComplet } of lois.toSorted(
    (
      { datePublication: datePublication1, loiJorfId: loiJorfId1 },
      { datePublication: datePublication2, loiJorfId: loiJorfId2 },
    ) =>
      `${datePublication1}-${loiJorfId1}`.localeCompare(
        `${datePublication2}-${loiJorfId2}`,
      ),
  )) {
    const { echeanciers } = (await fs.readJson(
      path.join(echeancierDataDir, "echeanciers", `${loiJorfId}.json`),
    )) as LoiEtMesures
    if (echeanciers.length === 0) {
      continue
    }
    const { LIGNE: lignes } = echeanciers[0]
    for (const [index, ligne] of lignes.entries()) {
      const liensArticles = ligne.LIEN_ARTICLE ?? []
      const { delaiApplication, etat } = mesures![index]
      mesuresCsv.push([
        titreComplet ?? "",
        loiJorfId,
        ligne.NUMERO_ORDRE ?? "",
        ligne.ARTICLE?.replace("<br/>".trim(), "") ?? "",
        liensArticles[0]?.["#text"],
        liensArticles[0]?.["@id"],
        liensArticles[1]?.["#text"],
        liensArticles[1]?.["@id"],
        liensArticles[2]?.["#text"],
        liensArticles[2]?.["@id"],
        ligne.BASE_LEGALE ?? "",
        ligne.OBJET ?? "",
        ligne.DECRET ?? "",
        ligne.CID_LOI_CIBLE ?? "",
        ligne.DATE_PREVUE ?? "",
        {
          applique: "appliqué",
          caduc: "sans objet",
          en_attente_application: "en attente d'application",
        }[etat] ?? "",
        delaiApplication?.toString() ?? "",
      ])
    }
  }

  return text(Papa.unparse(mesuresCsv, { delimiter: ";" }), {
    headers: {
      "Content-Type": "text/csv; charset=utf-8",
      "Content-Disposition": `attachment; filename=barometre_application_mesures_${
        new Date().toISOString().split("T")[0]
      }.csv"`,
    },
  })
}
