import config from "$lib/server/config"

// For @sveltejs/adapter-static:
export const prerender = true

export const load = () => {
  return {
    appTitle: config.title,
    backOfficeUrl: config.backOffice.url,
    containerUrls: config.containerUrls,
    date: new Date(),
    matomo: config.matomo,
    powerMode: config.powerMode,
  }
}
