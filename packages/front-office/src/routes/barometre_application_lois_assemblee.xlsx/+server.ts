import { error } from "@sveltejs/kit"
import Papa from "papaparse"
import XLSX from "xlsx-js-style"

import type { RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

export const GET: RequestHandler = async ({ fetch }) => {
  const [loisCsvString, mesuresCsvString] = await Promise.all(
    [
      "/barometre_application_lois_assemblee.csv",
      "/barometre_application_mesures_assemblee.csv",
    ].map(async (pathname) => {
      const response = await fetch(pathname)
      if (!response.ok) {
        error(response.status, await response.text())
      }
      return await response.text()
    }),
  )

  const workbook = XLSX.utils.book_new()
  workbook.Props = {
    Author: "LexImpact",
    // Note: Comments must be one line only.
    Comments:
      "Données du baromètre de l'application des lois <https://www.assemblee-nationale.fr/dyn/taux-application-lois/>. La dernière version de ce fichier peut être téléchargée à l'URL https://barometre.leximpact.dev/barometre_application_lois_assemblee.xlsx. La mise à jour est quotidienne.",
    Company: "Assemblée nationale",
    CreatedDate: new Date(),
    Title: `Baromètre de l'application des lois`,
  }
  for (const [label, csv] of [
    ["Lois", loisCsvString],
    ["Mesures", mesuresCsvString],
  ]) {
    const worksheet = XLSX.utils.aoa_to_sheet(
      Papa.parse(csv, { delimiter: ";" }).data as string[][],
    )
    XLSX.utils.book_append_sheet(workbook, worksheet, label)
  }

  const xlsxBuffer = XLSX.writeXLSX(workbook, {
    compression: true,
    type: "buffer",
  })
  return new Response(xlsxBuffer, {
    headers: {
      "Content-Type":
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "Content-Disposition": `attachment; filename=barometre_application_lois_${
        new Date().toISOString().split("T")[0]
      }.xlsx"`,
    },
  })
}
