import { sveltekit } from "@sveltejs/kit/vite"
import morgan from "morgan"
import Icons from "unplugin-icons/vite"
import { defineConfig } from "vite"

const middlewaresPlugin = {
  name: "echeancier-middlewares",
  async configureServer(server) {
    server.middlewares.use(morgan("dev"))
  },
}

export default defineConfig({
  plugins: [
    sveltekit(),
    Icons({
      compiler: "svelte",
    }),
    middlewaresPlugin,
  ],
})
