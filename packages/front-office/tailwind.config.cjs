const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {
      boxShadow: {
        button: "2px 2px 8px 0 rgba(0,0,0,.25)",
        inner_title: "inset  0px 0px 16px rgba(0,0,0,.15)",
        law: "0px 0px 3px 1px rgba(0,0,0,0.1)",
      },
      colors: {
        "le-jaune": "#ded500",
        "bleu-an": {
          50: "#e6ecf0",
          100: "#ccd8e1",
          200: "#99b1c3",
          300: "#678aa4",
          400: "#346386",
          500: "#013c68",
          600: "#01365e",
          700: "#013053",
          800: "#01243e",
          900: "#00182a",
          950: "#000c15",
        },
        "on-bleu-an": {
          50: "#000000",
          100: "#000000",
          200: "#000000",
          300: "#ffffff",
          400: "#ffffff",
          500: "#ffffff",
          600: "#ffffff",
          700: "#ffffff",
          800: "#ffffff",
          900: "#ffffff",
          950: "#ffffff",
        },
        "bleu-an-dark": "#233f6b",
        selected: {
          50: "#e8edf6",
          100: "#d0dced",
          200: "#a2b8db",
          300: "#7395ca",
          400: "#4571b8",
          500: "#164ea6",
          600: "#144695",
          700: "#123e85",
          800: "#0d2f64",
          900: "#091f42",
          950: "#041021",
        },
        "gray-an": "#e6e6e6",
      },
      fontFamily: {
        sans: ["Lato", "sans-serif"],
        serif: ["Lora", "serif"],
      },
    },
  },
}

module.exports = config
