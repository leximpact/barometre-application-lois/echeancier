/*!
  * Stickyfill – `position: sticky` polyfill
  * v. 2.1.0 | https://github.com/wilddeer/stickyfill
  * MIT License
  */
 !function(a,b){"use strict";function c(a,b){if(!(a instanceof b))throw new TypeError("Cannot call a class as a function")}function d(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])}function e(a){return parseFloat(a)||0}function f(a){for(var b=0;a;)b+=a.offsetTop,a=a.offsetParent;return b}function g(){function c(){a.pageXOffset!=m.left?(m.top=a.pageYOffset,m.left=a.pageXOffset,p.refreshAll()):a.pageYOffset!=m.top&&(m.top=a.pageYOffset,m.left=a.pageXOffset,n.forEach(function(a){return a._recalcPosition()}))}function d(){f=setInterval(function(){n.forEach(function(a){return a._fastCheck()})},500)}function e(){clearInterval(f)}if(!k){k=!0,c(),a.addEventListener("scroll",c),a.addEventListener("resize",p.refreshAll),a.addEventListener("orientationchange",p.refreshAll);var f=void 0,g=void 0,h=void 0;"hidden"in b?(g="hidden",h="visibilitychange"):"webkitHidden"in b&&(g="webkitHidden",h="webkitvisibilitychange"),h?(b[g]||d(),b.addEventListener(h,function(){b[g]?e():d()})):d()}}var h=function(){function a(a,b){for(var c=0;c<b.length;c++){var d=b[c];d.enumerable=d.enumerable||!1,d.configurable=!0,"value"in d&&(d.writable=!0),Object.defineProperty(a,d.key,d)}}return function(b,c,d){return c&&a(b.prototype,c),d&&a(b,d),b}}(),i=!1,j="undefined"!=typeof a;j&&a.getComputedStyle?!function(){var a=b.createElement("div");["","-webkit-","-moz-","-ms-"].some(function(b){try{a.style.position=b+"sticky"}catch(a){}return""!=a.style.position})&&(i=!0)}():i=!0;var k=!1,l="undefined"!=typeof ShadowRoot,m={top:null,left:null},n=[],o=function(){function g(a){if(c(this,g),!(a instanceof HTMLElement))throw new Error("First argument must be HTMLElement");if(n.some(function(b){return b._node===a}))throw new Error("Stickyfill is already applied to this node");this._node=a,this._stickyMode=null,this._active=!1,n.push(this),this.refresh()}return h(g,[{key:"refresh",value:function(){if(!i&&!this._removed){this._active&&this._deactivate();var c=this._node,g=getComputedStyle(c),h={position:g.position,top:g.top,display:g.display,marginTop:g.marginTop,marginBottom:g.marginBottom,marginLeft:g.marginLeft,marginRight:g.marginRight,cssFloat:g.cssFloat};if(!isNaN(parseFloat(h.top))&&"table-cell"!=h.display&&"none"!=h.display){this._active=!0;var j=c.style.position;"sticky"!=g.position&&"-webkit-sticky"!=g.position||(c.style.position="static");var k=c.parentNode,m=l&&k instanceof ShadowRoot?k.host:k,n=c.getBoundingClientRect(),o=m.getBoundingClientRect(),p=getComputedStyle(m);this._parent={node:m,styles:{position:m.style.position},offsetHeight:m.offsetHeight},this._offsetToWindow={left:n.left,right:b.documentElement.clientWidth-n.right},this._offsetToParent={top:n.top-o.top-e(p.borderTopWidth),left:n.left-o.left-e(p.borderLeftWidth),right:-n.right+o.right-e(p.borderRightWidth)},this._styles={position:j,top:c.style.top,bottom:c.style.bottom,left:c.style.left,right:c.style.right,width:c.style.width,marginTop:c.style.marginTop,marginLeft:c.style.marginLeft,marginRight:c.style.marginRight};var q=e(h.top);this._limits={start:n.top+a.pageYOffset-q,end:o.top+a.pageYOffset+m.offsetHeight-e(p.borderBottomWidth)-c.offsetHeight-q-e(h.marginBottom)};var r=p.position;"absolute"!=r&&"relative"!=r&&(m.style.position="relative"),this._recalcPosition();var s=this._clone={};s.node=b.createElement("div"),d(s.node.style,{width:n.right-n.left+"px",height:n.bottom-n.top+"px",marginTop:h.marginTop,marginBottom:h.marginBottom,marginLeft:h.marginLeft,marginRight:h.marginRight,cssFloat:h.cssFloat,padding:0,border:0,borderSpacing:0,fontSize:"1em",position:"static"}),k.insertBefore(s.node,c),s.docOffsetTop=f(s.node)}}}},{key:"_recalcPosition",value:function(){if(this._active&&!this._removed){var a=m.top<=this._limits.start?"start":m.top>=this._limits.end?"end":"middle";if(this._stickyMode!=a){switch(a){case"start":d(this._node.style,{position:"absolute",left:this._offsetToParent.left+"px",right:this._offsetToParent.right+"px",top:this._offsetToParent.top+"px",bottom:"auto",width:"auto",marginLeft:0,marginRight:0,marginTop:0});break;case"middle":d(this._node.style,{position:"fixed",left:this._offsetToWindow.left+"px",right:this._offsetToWindow.right+"px",top:this._styles.top,bottom:"auto",width:"auto",marginLeft:0,marginRight:0,marginTop:0});break;case"end":d(this._node.style,{position:"absolute",left:this._offsetToParent.left+"px",right:this._offsetToParent.right+"px",top:"auto",bottom:0,width:"auto",marginLeft:0,marginRight:0})}this._stickyMode=a}}}},{key:"_fastCheck",value:function(){this._active&&!this._removed&&(Math.abs(f(this._clone.node)-this._clone.docOffsetTop)>1||Math.abs(this._parent.node.offsetHeight-this._parent.offsetHeight)>1)&&this.refresh()}},{key:"_deactivate",value:function(){var a=this;this._active&&!this._removed&&(this._clone.node.parentNode.removeChild(this._clone.node),delete this._clone,d(this._node.style,this._styles),delete this._styles,n.some(function(b){return b!==a&&b._parent&&b._parent.node===a._parent.node})||d(this._parent.node.style,this._parent.styles),delete this._parent,this._stickyMode=null,this._active=!1,delete this._offsetToWindow,delete this._offsetToParent,delete this._limits)}},{key:"remove",value:function(){var a=this;this._deactivate(),n.some(function(b,c){if(b._node===a._node)return n.splice(c,1),!0}),this._removed=!0}}]),g}(),p={stickies:n,Sticky:o,forceSticky:function(){i=!1,g(),this.refreshAll()},addOne:function(a){if(!(a instanceof HTMLElement)){if(!a.length||!a[0])return;a=a[0]}for(var b=0;b<n.length;b++)if(n[b]._node===a)return n[b];return new o(a)},add:function(a){if(a instanceof HTMLElement&&(a=[a]),a.length){for(var b=[],c=function(c){var d=a[c];return d instanceof HTMLElement?n.some(function(a){if(a._node===d)return b.push(a),!0})?"continue":void b.push(new o(d)):(b.push(void 0),"continue")},d=0;d<a.length;d++){c(d)}return b}},refreshAll:function(){n.forEach(function(a){return a.refresh()})},removeOne:function(a){if(!(a instanceof HTMLElement)){if(!a.length||!a[0])return;a=a[0]}n.some(function(b){if(b._node===a)return b.remove(),!0})},remove:function(a){if(a instanceof HTMLElement&&(a=[a]),a.length)for(var b=function(b){var c=a[b];n.some(function(a){if(a._node===c)return a.remove(),!0})},c=0;c<a.length;c++)b(c)},removeAll:function(){for(;n.length;)n[0].remove()}};i||g(),"undefined"!=typeof module&&module.exports?module.exports=p:j&&(a.Stickyfill=p)}(window,document);
/*!
* Expander - v1.4.7 - 2013-08-30
* http://plugins.learningjquery.com/expander/
* Copyright (c) 2013 Karl Swedberg
* Licensed MIT (http://www.opensource.org/licenses/mit-license.php)
*/

(function($) {
  $.expander = {
    version: '1.4.7',
    defaults: {
      // the number of characters at which the contents will be sliced into two parts.
      slicePoint: 100,

      // whether to keep the last word of the summary whole (true) or let it slice in the middle of a word (false)
      preserveWords: true,

      // a threshold of sorts for whether to initially hide/collapse part of the element's contents.
      // If after slicing the contents in two there are fewer words in the second part than
      // the value set by widow, we won't bother hiding/collapsing anything.
      widow: 4,

      // text displayed in a link instead of the hidden part of the element.
      // clicking this will expand/show the hidden/collapsed text
      expandText: 'read more',
      expandPrefix: '&hellip; ',

      expandAfterSummary: false,

      // class names for summary element and detail element
      summaryClass: 'summary',
      detailClass: 'details',

      // class names for <span> around "read-more" link and "read-less" link
      moreClass: 'read-more',
      lessClass: 'read-less',

      // number of milliseconds after text has been expanded at which to collapse the text again.
      // when 0, no auto-collapsing
      collapseTimer: 0,

      // effects for expanding and collapsing
      expandEffect: 'slideDown',
      expandSpeed: 250,
      collapseEffect: 'slideUp',
      collapseSpeed: 200,

      // allow the user to re-collapse the expanded text.
      userCollapse: true,

      // text to use for the link to re-collapse the text
      userCollapseText: 'read less',
      userCollapsePrefix: ' ',


      // all callback functions have the this keyword mapped to the element in the jQuery set when .expander() is called

      onSlice: null, // function() {}
      beforeExpand: null, // function() {},
      afterExpand: null, // function() {},
      onCollapse: null, // function(byUser) {}
      afterCollapse: null // function() {}
    }
  };

  $.fn.expander = function(options) {
    var meth = 'init';

    if (typeof options === 'string') {
      meth = options;
      options = {};
    }

    var opts = $.extend({}, $.expander.defaults, options),
        rSelfClose = /^<(?:area|br|col|embed|hr|img|input|link|meta|param).*>$/i,
        rAmpWordEnd = opts.wordEnd || /(&(?:[^;]+;)?|[a-zA-Z\u00C0-\u0100]+)$/,
        rOpenCloseTag = /<\/?(\w+)[^>]*>/g,
        rOpenTag = /<(\w+)[^>]*>/g,
        rCloseTag = /<\/(\w+)>/g,
        rLastCloseTag = /(<\/[^>]+>)\s*$/,
        rTagPlus = /^(<[^>]+>)+.?/,
        delayedCollapse;

    var methods = {
      init: function() {
        this.each(function() {
          var i, l, tmp, newChar, summTagless, summOpens, summCloses,
              lastCloseTag, detailText, detailTagless, html, expand,
              $thisDetails, $readMore,
              openTagsForDetails = [],
              closeTagsForsummaryText = [],
              defined = {},
              thisEl = this,
              $this = $(this),
              $summEl = $([]),
              o = $.extend({}, opts, $this.data('expander') || $.meta && $this.data() || {}),
              hasDetails = !!$this.find('.' + o.detailClass).length,
              hasBlocks = !!$this.find('*').filter(function() {
                var display = $(this).css('display');
                return (/^block|table|list/).test(display);
              }).length,
              el = hasBlocks ? 'div' : 'span',
              detailSelector = el + '.' + o.detailClass,
              moreClass = o.moreClass + '',
              lessClass = o.lessClass + '',
              expandSpeed = o.expandSpeed || 0,
              allHtml = $.trim( $this.html() ),
              allText = $.trim( $this.text() ),
              summaryText = allHtml.slice(0, o.slicePoint);

          // allow multiple classes for more/less links
          o.moreSelector = 'span.' + moreClass.split(' ').join('.');
          o.lessSelector = 'span.' + lessClass.split(' ').join('.');
          // bail out if we've already set up the expander on this element
          if ( $.data(this, 'expanderInit') ) {
            return;
          }

          $.data(this, 'expanderInit', true);
          $.data(this, 'expander', o);
          // determine which callback functions are defined
          $.each(['onSlice','beforeExpand', 'afterExpand', 'onCollapse', 'afterCollapse'], function(index, val) {
            defined[val] = $.isFunction(o[val]);
          });

          // back up if we're in the middle of a tag or word
          summaryText = backup(summaryText);

          // summary text sans tags length
          summTagless = summaryText.replace(rOpenCloseTag, '').length;

          // add more characters to the summary, one for each character in the tags
          while (summTagless < o.slicePoint) {
            newChar = allHtml.charAt(summaryText.length);
            if (newChar === '<') {
              newChar = allHtml.slice(summaryText.length).match(rTagPlus)[0];
            }
            summaryText += newChar;
            summTagless++;
          }

          summaryText = backup(summaryText, o.preserveWords);

          // separate open tags from close tags and clean up the lists
          summOpens = summaryText.match(rOpenTag) || [];
          summCloses = summaryText.match(rCloseTag) || [];

          // filter out self-closing tags
          tmp = [];
          $.each(summOpens, function(index, val) {
            if ( !rSelfClose.test(val) ) {
              tmp.push(val);
            }
          });
          summOpens = tmp;

          // strip close tags to just the tag name
          l = summCloses.length;
          for (i = 0; i < l; i++) {
            summCloses[i] = summCloses[i].replace(rCloseTag, '$1');
          }

          // tags that start in summary and end in detail need:
          // a). close tag at end of summary
          // b). open tag at beginning of detail
          $.each(summOpens, function(index, val) {
            var thisTagName = val.replace(rOpenTag, '$1');
            var closePosition = $.inArray(thisTagName, summCloses);
            if (closePosition === -1) {
              openTagsForDetails.push(val);
              closeTagsForsummaryText.push('</' + thisTagName + '>');

            } else {
              summCloses.splice(closePosition, 1);
            }
          });

          // reverse the order of the close tags for the summary so they line up right
          closeTagsForsummaryText.reverse();

          // create necessary summary and detail elements if they don't already exist
          if ( !hasDetails ) {

            // end script if there is no detail text or if detail has fewer words than widow option
            detailText = allHtml.slice(summaryText.length);
            detailTagless = $.trim( detailText.replace(rOpenCloseTag, '') );

            if ( detailTagless === '' || detailTagless.split(/\s+/).length < o.widow ) {
              return;
            }
            // otherwise, continue...
            lastCloseTag = closeTagsForsummaryText.pop() || '';
            summaryText += closeTagsForsummaryText.join('');
            detailText = openTagsForDetails.join('') + detailText;

          } else {
            // assume that even if there are details, we still need readMore/readLess/summary elements
            // (we already bailed out earlier when readMore el was found)
            // but we need to create els differently

            // remove the detail from the rest of the content
            detailText = $this.find(detailSelector).remove().html();

            // The summary is what's left
            summaryText = $this.html();

            // allHtml is the summary and detail combined (this is needed when content has block-level elements)
            allHtml = summaryText + detailText;

            lastCloseTag = '';
          }
          o.moreLabel = $this.find(o.moreSelector).length ? '' : buildMoreLabel(o);

          if (hasBlocks) {
            detailText = allHtml;
          }
          summaryText += lastCloseTag;

          // onSlice callback
          o.summary = summaryText;
          o.details = detailText;
          o.lastCloseTag = lastCloseTag;

          if (defined.onSlice) {
            // user can choose to return a modified options object
            // one last chance for user to change the options. sneaky, huh?
            // but could be tricky so use at your own risk.
            tmp = o.onSlice.call(thisEl, o);

          // so, if the returned value from the onSlice function is an object with a details property, we'll use that!
            o = tmp && tmp.details ? tmp : o;
          }

          // build the html with summary and detail and use it to replace old contents
          html = buildHTML(o, hasBlocks);

          $this.html( html );

          // set up details and summary for expanding/collapsing
          $thisDetails = $this.find(detailSelector);
          $readMore = $this.find(o.moreSelector);

          // Hide details span using collapseEffect unless
          // expandEffect is NOT slideDown and collapseEffect IS slideUp.
          // The slideUp effect sets span's "default" display to
          // inline-block. This is necessary for slideDown, but
          // problematic for other "showing" animations.
          // Fixes #46
          if (o.collapseEffect === 'slideUp' && o.expandEffect !== 'slideDown' || $this.is(':hidden')) {
            $thisDetails.css({display: 'none'});
          } else {
            $thisDetails[o.collapseEffect](0);
          }

          $summEl = $this.find('div.' + o.summaryClass);

          expand = function(event) {
            event.preventDefault();
            $readMore.hide();
            $summEl.hide();
            if (defined.beforeExpand) {
              o.beforeExpand.call(thisEl);
            }

            $thisDetails.stop(false, true)[o.expandEffect](expandSpeed, function() {
              $thisDetails.css({zoom: ''});
              if (defined.afterExpand) {o.afterExpand.call(thisEl);}
              delayCollapse(o, $thisDetails, thisEl);
            });
          };

          $readMore.find('a').unbind('click.expander').bind('click.expander', expand);

          if ( o.userCollapse && !$this.find(o.lessSelector).length ) {
            $this
            .find(detailSelector)
            .append('<span class="' + o.lessClass + '">' + o.userCollapsePrefix + '<a href="#">' + o.userCollapseText + '</a></span>');
          }

          $this
          .find(o.lessSelector + ' a')
          .unbind('click.expander')
          .bind('click.expander', function(event) {
            event.preventDefault();
            clearTimeout(delayedCollapse);
            var $detailsCollapsed = $(this).closest(detailSelector);
            reCollapse(o, $detailsCollapsed);
            if (defined.onCollapse) {
              o.onCollapse.call(thisEl, true);
            }
          });

        }); // this.each
      },
      destroy: function() {

        this.each(function() {
          var o, details,
              $this = $(this);

          if ( !$this.data('expanderInit') ) {
            return;
          }

          o = $.extend({}, $this.data('expander') || {}, opts);
          details = $this.find('.' + o.detailClass).contents();

          $this.removeData('expanderInit');
          $this.removeData('expander');

          $this.find(o.moreSelector).remove();
          $this.find('.' + o.summaryClass).remove();
          $this.find('.' + o.detailClass).after(details).remove();
          $this.find(o.lessSelector).remove();

        });
      }
    };

    // run the methods (almost always "init")
    if ( methods[meth] ) {
      methods[ meth ].call(this);
    }

    // utility functions
    function buildHTML(o, blocks) {
      var el = 'span',
          summary = o.summary;
      if ( blocks ) {
        el = 'div';
        // if summary ends with a close tag, tuck the moreLabel inside it
        if ( rLastCloseTag.test(summary) && !o.expandAfterSummary) {
          summary = summary.replace(rLastCloseTag, o.moreLabel + '$1');
        } else {
        // otherwise (e.g. if ends with self-closing tag) just add moreLabel after summary
        // fixes #19
          summary += o.moreLabel;
        }

        // and wrap it in a div
        summary = '<div class="' + o.summaryClass + '">' + summary + '</div>';
      } else {
        summary += o.moreLabel;
      }

      return [
        summary,
        ' <',
          el + ' class="' + o.detailClass + '"',
        '>',
          o.details,
        '</' + el + '>'
        ].join('');
    }

    function buildMoreLabel(o) {
      var ret = '<span class="' + o.moreClass + '">' + o.expandPrefix;
      ret += '<a href="#">' + o.expandText + '</a></span>';
      return ret;
    }

    function backup(txt, preserveWords) {
      if ( txt.lastIndexOf('<') > txt.lastIndexOf('>') ) {
        txt = txt.slice( 0, txt.lastIndexOf('<') );
      }
      if (preserveWords) {
        txt = txt.replace(rAmpWordEnd,'');
      }

      return $.trim(txt);
    }

    function reCollapse(o, el) {
      el.stop(true, true)[o.collapseEffect](o.collapseSpeed, function() {
        var prevMore = el.prev('span.' + o.moreClass).show();
        if (!prevMore.length) {
          el.parent().children('div.' + o.summaryClass).show()
            .find('span.' + o.moreClass).show();
        }
        if (o.afterCollapse) {o.afterCollapse.call(el);}
      });
    }

    function delayCollapse(option, $collapseEl, thisEl) {
      if (option.collapseTimer) {
        delayedCollapse = setTimeout(function() {
          reCollapse(option, $collapseEl);
          if ( $.isFunction(option.onCollapse) ) {
            option.onCollapse.call(thisEl, false);
          }
        }, option.collapseTimer);
      }
    }

    return this;
  };

  // plugin defaults
  $.fn.expander.defaults = $.expander.defaults;
})(jQuery);
function actifInactif(obj)
{
    if (obj.hasClass('etat-actif')){
        obj.removeClass('etat-actif');
        obj.addClass('etat-inactif');
    }
}

function actifInactifReverse(obj)
{
    obj.parent().find("div.slick-slide").each(function( index ) {
        $(this).removeClass('etat-actif');
        $(this).addClass('etat-inactif');
    });
    obj.addClass('etat-actif');
}

function inactifActif(obj)
{
    if (obj.hasClass('etat-inactif')){
        obj.removeClass('etat-inactif');
        obj.addClass('etat-actif');
    }
}

function inactifActifComplet(obj)
{
    if (obj.hasClass('etat-inactif')){
        obj.removeClass('etat-inactif');
        obj.addClass('etat-actif');
        var a_etape = $('div[data-etape="' + obj.find('a').attr('data-etape') + '"]');
        if ($(a_etape).hasClass('etat-inactif')) {
            $(a_etape).removeClass('etat-inactif');
            $(a_etape).addClass('etat-actif');
        }
    }
}

function flipAnimation(obj,contenu,fleche)
{
    if (obj.hasClass('flip')) {
        var _velocityEffectOut="transition.slideRightOut";
        var _velocityEffectIn="transition.slideRightIn";
    } else {
        var _velocityEffectOut="transition.slideLeftOut";
        var _velocityEffectIn="transition.slideLeftIn";
    }
    animation('.fiche-procedure-contenu:visible',_velocityEffectIn,_velocityEffectOut,contenu);
    animation('.fiche-procedure-haut-bas:visible',_velocityEffectIn,_velocityEffectOut,fleche);
}

function etapeLinkSlick($carouselEtapes, $currentSlide)
{
    var index = $currentSlide.index();
    var codeActe = $currentSlide.attr('data-codeacte');
    var contenu_etape = $('.fiche-procedure-contenu[data-codeacte="' + codeActe + '"]');
    var fleche_etape = $('.fiche-procedure-haut-bas[data-codeacte="' + codeActe + '"]');
    var stateObject = {};
    var title = " ";
    var uri = window.location.href.split("#")[0];
    var newUrl = '?etape=' + codeActe;
    var icons = false;

    history.pushState(stateObject,title,newUrl);
    actifInactifReverse($currentSlide);

    if (contenu_etape.length > 0) {
        if (icons) {
            inactifActif($currentSlide);
        } else {
            inactifActifComplet($currentSlide);
        }
        if (!contenu_etape.is(":visible")) {
            flipAnimation($(this),contenu_etape,fleche_etape);
        }
        contenu_etape.find(".tab-pane.active").trigger('amendement');
        $carouselEtapes.parent().next("div.les-procedures-bas").find(".etapes-select").val(codeActe);
    }

    if (!$currentSlide.hasClass("slick-active")) {
        $carouselEtapes.slick('slickGoTo', index);
    }
}

function animation(objet,effetIn,effetOut,container)
{
    $.Velocity.RegisterEffect("transition.flipXReverseIn", {
        defaultDuration: 150,
        calls: [[ { opacity: [ 1, 0 ]/*, transformPerspective: [ 800, 800 ], rotateY: [ 0, 55 ]*/ } ]
        ],
        reset: { transformPerspective: 0 }
    });
    $.Velocity.RegisterEffect("transition.flipXReverseOut", {
        defaultDuration: 150,
        calls: [[ { opacity: [ 0, 1 ]/*, transformPerspective: [ 800, 800 ], rotateY: -55 */} ]
        ],
        reset: { transformPerspective: 0, rotateY: 0 }
    });
    $(objet).velocity(
        "fadeOut",
        {
            duration:150,
            complete:function(){container.find(".carrousel-auteurs-rapporteurs").trigger('carrousel');}
        }
    );
    setTimeout(function() {
        container.velocity("fadeIn", { duration:150,queue: false,complete:function(){container.find(".carrousel-auteurs-rapporteurs").trigger('carrousel');} });
    },300);
}

function cam(obj)
{
    obj.hover(function () {
        this.sector.stop();
        this.sector.scale(1.1, 1.1, this.cx, this.cy);
        if (this.label) {
            this.label[0].stop();
            this.label[0].attr({ r: 7.5 });
            this.label[1].attr({ "font-weight": 800 });
        }
    }, function () {
        this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");
        if (this.label) {
            this.label[0].animate({ r: 5 }, 500, "bounce");
            this.label[1].attr({ "font-weight": 400 });
        }
    });
}

function initCarouseEtapes($carouselEtapes)
{
    $carouselEtapes.slick({
        dots: false,
        infinite: false,
        speed: 400,
        swipe: false,
        touchMove: false,
        slidesToShow: 6,
        slidesToScroll: 6
    });

    // A défaut on affiche l'étape active
    let $currentIndexActive = $carouselEtapes.find("div.etat-actif").index();
    $carouselEtapes.slick('slickGoTo', $currentIndexActive, true);

    //Evenements
    $carouselEtapes.on('click', 'a.image-procedure', function() {
        var $currentSlide = $(this).closest("div.slick-slide");
        etapeLinkSlick($carouselEtapes, $currentSlide);
    });

    $carouselEtapes.closest("div.les-procedures").on('change', 'select.etapes-select', function() {
        var $currentSlide = $carouselEtapes.find('a[data-codeacte="' + $(this).val() + '"]').closest("div.slick-slide");
        etapeLinkSlick($carouselEtapes, $currentSlide);
    });

    $('.fiche-procedure-haut-bas').on('click', 'a', function() {
        var $currentSlide = $carouselEtapes.find('a[data-codeacte="' + $(this).attr("data-codeacte") + '"]').closest("div.slick-slide");
        etapeLinkSlick($carouselEtapes, $currentSlide);

        //Pour ne pas avoir le lien href # dans l'url
        return false;
    });
}

function keyArrow()
{
    $(document).keydown(function(e) {
        switch(e.which) {
            case 37: // left
                $('a.precedente:visible').eq(0).click();
                break;
            case 39: // right
                $('a.suivante:visible').eq(0).click();
                break;
            default: return; // exit this handler for other keys
        }
        e.preventDefault(); // prevent the default action (scroll / move caret)
    });
}

function normalizeHeights(items, heights, tallest)
{
	items.each(function() { //add heights to array
		heights.push($(this).height());
	});
	tallest = Math.max.apply(null, heights); //cache largest value
	items.each(function() {
		$(this).css('min-height',tallest + 'px');
	});
}

function carouselNormalization(items)
{
    var heights = [], //create empty array to store height values
        tallest; //create variable to make note of the tallest slide
    if (items.length) {
        normalizeHeights(items, heights, tallest);
        $(window).on('resize orientationchange', function () {
            tallest = 0, heights.length = 0; //reset vars
            items.each(function() {
                $(this).css('min-height','0'); //reset min-height
            });
            normalizeHeights(items, heights, tallest); //run it again
        });
    }
}

function activateCollapsedItems()
{
	$('.collapse').on('show.bs.collapse', function () {
		var _this=$(this),
			_trigger = $(this).attr('data-trigger');
		if(_this.hasClass('contenucr')) {
			$(_trigger).attr("title","Fermer le sommaire du compte-rendu");
			$(_trigger).html('<i class="fa fa-minus-circle" aria-hidden="true"></i>' + $(_trigger).text().replace("Afficher le sommaire du compte-rendu","Fermer le sommaire du compte-rendu"));
		} 
		else if(_this.hasClass('co-signataires')) {
			$(_trigger).attr("title","Fermer");
			$(_trigger).html('<i class="fa fa-minus-circle" aria-hidden="true"></i>' + $(_trigger).text().replace("Afficher tous les co-signataires","Fermer").replace('...',''));
		} else {
			$(_trigger).attr("title","Fermer");
			$(_trigger).html('<i class="fa fa-minus-square-o" aria-hidden="true"></i>');
		}
	});

	$('.collapse').on('hide.bs.collapse', function () {
		var _this=$(this),
			_trigger = $(this).attr('data-trigger');
		if(_this.hasClass('contenucr')) {
			$(_trigger).attr("title","Afficher le sommaire du compte-rendu");
			$(_trigger).html('<i class="fa fa-plus-circle" aria-hidden="true"></i> ' + $(_trigger).text().replace("Fermer le sommaire du compte-rendu","Afficher le sommaire du compte-rendu"));
		} else if(_this.hasClass('co-signataires')) {
			$(_trigger).attr("title","Afficher tous les co-signataires");
			$(_trigger).html('<i class="fa fa-plus-circle" aria-hidden="true"></i> ' + $(_trigger).text().replace("Fermer","Afficher tous les co-signataires"));
		} else {
			$(_trigger).attr("title","Ouvrir");
			$(_trigger).html('<i class="fa fa-plus-square-o" aria-hidden="true"></i>');
		}
	});

	$('.sommaire').click(function(event) {
		event.preventDefault();
		var _this=$(this),
			url = $(this).attr('data-href'),
			target = $(this).attr('href');
		if(_this.hasClass('som-ajax')) {
			var elmt = $(target).find('.image-tabmat-ajax');
			elmt.show().animate({opacity: 1}, 200);
			$.get( url, function( data ) {
				_this.removeClass('som-ajax');
				$(target).html(data);
			});
		}
	});
}

function activateAllSommairesCr()
{
	$('button.tous-som').click( function(event) {
		var target = $(this).attr('data-target');
		if ($(this).hasClass('btn-outline-success')) {
			$(target).find('.sommaire').click();
			$(this).removeClass('btn-outline-success').addClass('btn-outline-danger').html('<i class="fa fa-minus-circle" aria-hidden="true"></i> Fermer tous les sommaires disponibles');
		} else {
			$(target).find('.sommaire').click();
			$(this).removeClass('btn-outline-danger').addClass('btn-outline-success').html('<i class="fa fa-plus-circle" aria-hidden="true"></i> Afficher tous les sommaires disponibles');
		}
	});
}

function initVersionDepliee()
{
	// Hack pour IE pour gérer le menu de gauche fixe
    if ($('.bd-toc').length > 0) {
		var elements = $('.bd-toc');
		Stickyfill.add(elements);
	}

	// Rend la liste des co-signataires "étendable". Permet d'afficher la liste complète des co-signataires lorsque la liste dépasse les 4000 caractères
	$('div.expandable').expander({
		slicePoint:      4000,  // default is 100
		expandText: 'afficher la suite',
  		expandPrefix: ' … ',// default is 'read more'
		collapseTimer:    0, // re-collapses after 5 seconds; default is 0, so no re-collapsing
		userCollapseText: 'fermer',
		userCollapsePrefix: '...'
	});

	$(".image-tabmat-ajax").animate({opacity: 0}, 200, function(){
		$(".image-tabmat-ajax").hide();
		$(".carrousel-auteurs-rapporteurs").trigger('carrousel');
		$(".auteurs").trigger('carrousel');
		$('.hideAll').show().animate({opacity: 1}, 200);
		carouselNormalization($('#Une .carousel-item'));
		showVideo($('#page'));
	});

	$('.hideAll').show();
}

function initVersionPliee()
{
	// déplace les div.modal de Bootstrap et les attache au body pour éliminer les problèmes de z-index
	$(".modal").appendTo("body");

	// le nombre de slide de la navigation principale entre les étapes
	var slideCount = $('#nav-etapes').find('div.carousel-item').length;

	// active le carousel Bs : navigation principale
	$('#nav-etapes').each(function(index) {carrousel($(this),index, slideCount);});
    
    //On initialise le carousel de la navigation par étape
	initCarouseEtapes($('.nav-etapes-carousel'));
	
	// Active le premier onglet
	$('.nav-tabs').each(function(index) { $(this).children('li').first().children('a').addClass('active'); });  
	$('.tab-content').each(function(index) { $(this).children('.tab-pane').first().addClass('active'); });

	//Graphique lié aux scrutins
	$(document).on('piechart', function(evt) {  
		$(evt.target).find('.chart').each(function(index) {
			var s1 =[],
				ticks = [],
				s1label = [];
			s1[0]=parseFloat($(this).attr('data-pour'));
			s1[1]=parseFloat($(this).attr('data-contre'));
			if (parseFloat($(this).attr('data-pour')) > parseFloat($(this).attr('data-contre'))) {
				ticks[0]="Pour l'adoption : "+$(this).attr('data-pour');
				ticks[1]="Contre : "+$(this).attr('data-contre');
				s1label[0]='Pour l\'adoption';
				s1label[1]='Contre';
			} else {
				ticks[0]="Pour : "+$(this).attr('data-pour');
				ticks[1]="Contre l'adoption : "+$(this).attr('data-contre');
				s1label[0]='Pour';
				s1label[1]='Contre l\'adoption';
			}
			if (parseFloat($(this).attr('data-abstention'))>=0) {
				s1[2]=parseFloat($(this).attr('data-abstention'));
				ticks[2]="Abstention : "+ s1[2];
				s1label[2]="Abstention : " + s1[2];
			}
			var id=$(this).attr('id');
			if (s1[0]==Math.max(s1[0],s1[1],s1[2])) {
				if (s1[1]>s1[2]) {
					var couleurs=["#155B87","#FF5353", "#dadada"];
				} else {
					var couleurs=["#155B87","#dadada","#FF5353"];
				}
			}
			if (s1[1]==Math.max(s1[0],s1[1],s1[2]))	{
				if (s1[0]>s1[2]) {
					var couleurs=["#FF5353","#155B87", "#dadada"];
				} else {
					var couleurs=["#FF5353", "#dadada", "#155B87"];
				}
			}
			if (s1[2]==Math.max(s1[0],s1[1],s1[2])) {
				if (s1[0]>s1[1]) {
					var couleurs=["#dadada", "#155B87","#FF5353"];
				} else {
					var couleurs=["#dadada","#155B87","#FF5353"];
				}
			}
			if (s1[2] == 0) s1.remove(s1[2]);
			if (s1[1] == 0) s1.remove(s1[1]);
			if (s1[0] == 0) s1.remove(s1[0]);
			r = Raphael(document.getElementById(id),290,250);
			pie = r.piechart(130, 100, 85, s1, { legend: ticks, legendpos: "south", colors: couleurs});
			cam(pie);
		});
	});

	$(".image-tabmat-ajax").animate({opacity: 0}, 200, function(){
		$(".image-tabmat-ajax").hide();
		$(".carrousel-auteurs-rapporteurs").trigger('carrousel');
		$(".auteurs").trigger('carrousel');
		$(".vote").trigger('piechart');
		$('.hideAll').show().animate({opacity: 1}, 200);
		carouselNormalization($('#Une .carousel-item'));
		showVideo($('#page'));
	});

	$('.hideAll').show();
}

$(window).bind('load',function()
{
	// Gestion des "collpase"
	activateCollapsedItems();
	activateAllSommairesCr()

	// gère l'affichage des uid des actes législatifs, documents et du dossier législatif
	keyArrow();

	// égalise la hauteur des item du carousel des articles éditoriaux mis en une
	carouselNormalization($('#Une .carousel-item'));

	// slider des rapporteurs, auteurs, etc...
	$(document).on('carrousel',function(evt) {			
		$(evt.target).find('.carrousel-auto-false:visible').each(function(index) {
			var SelectorItem = $(this),
				j=Math.floor((Math.random()*100)+1),
				ulClass = 'liste-carrousel' + j + index,
				automatique = false,
				infiniteloop = false,
				autohover = false;
			SelectorItem.find('> ul').addClass(ulClass);
			$(this).children('ul').css('text-indent',0);
			$(this).removeClass('carrousel-auto-false').addClass('carrousel-auto-false-ok');
			if ($(this).hasClass('auto')) {
				automatique = true;
				infiniteloop = true;
				autohover = true;
			}
			var SelectorPagination = SelectorItem.find(".page-pagination");

			let carrouselOptions = {
                controls: false,
                auto: automatique,
                stopAutoOnClick: true,
                pager: true,
                infiniteLoop:  infiniteloop,
                mode: 'fade',
                autoHidePager: true,
                autoHover:autohover,
                pause : 3000,
				touchEnabled: false
			};

            //Initiate pager
            // if number of elements is equal to 1 don't show pager, else if number of elements is upper than 4 elements use custom pager, else use bxslider dotted.
			if(SelectorItem.find('> ul > li').length === 1){
                carrouselOptions.pager = false;
			}
			else if(SelectorPagination.length > 0){
                carrouselOptions.pagerSelector = SelectorPagination;
                carrouselOptions.pagerType = 'short';
            }

            var carrousel = $('ul.liste-carrousel' + j + index).bxSlider(carrouselOptions);
			var slideCount = carrousel.getSlideCount();
			if (slideCount==1) {
				SelectorItem.find('.controle-pagination').hide();
				SelectorItem.find('.titre-carrousel').css('padding-top','0px');
			}
			SelectorItem.find('.controle-pagination .precedent').addClass('inactif');
			if (SelectorItem.find('a.actif').parents('li').last().index()>0) {
				carrousel.goToSlide(SelectorItem.find('a.actif').parents('li').last().index());
				if ( carrousel.getCurrentSlide() + 1 == slideCount) {
					SelectorItem.find('.controle-pagination .suivant').addClass('inactif');
					SelectorItem.find('.controle-pagination .precedent').removeClass('inactif');
				}
			}
			SelectorItem.find('.controle-pagination .precedent').click(function(){
				carrousel.goToPrevSlide();
				var slideCurrent = carrousel.getCurrentSlide() + 1 ;
				if (slideCurrent==1) $(this).addClass('inactif');
				if(slideCurrent<=slideCount) SelectorItem.find('.controle-pagination .suivant').removeClass('inactif');
				return false;
			});
			SelectorItem.find('.controle-pagination .suivant').click(function(){
				carrousel.goToNextSlide();
				var slideCurrent = carrousel.getCurrentSlide() + 1 ;
				if (slideCurrent==slideCount) $(this).addClass('inactif');
				if(slideCurrent>=1) SelectorItem.find('.controle-pagination .precedent').removeClass('inactif');
				return false;
			});

            /* Rappel Animation de l'opacité des carrousel (telle que prévue dans an-front-loader.js)
            * Ce rappel est nécessaire pour IE car celui-ci passe d'abord dans l'event load ci-présent et ensuite dans
            * l'event ajax dans common.js hors ceci n'est pas le cas sur chrome et firefox ou elle est chargé
            **/
            let $carrousel_auto_false = $(this);
            let attr_carrousel = $carrousel_auto_false.attr('opacity');
            if (typeof attr_carrousel == "undefined" || attr_carrousel != "1") {
				$carrousel_auto_false.animate({opacity: 1}, 1500);
            }
		});
	});

	if ($('body').hasClass('flat')) {
		initVersionDepliee();
	}
	else {
		initVersionPliee();
	}

});