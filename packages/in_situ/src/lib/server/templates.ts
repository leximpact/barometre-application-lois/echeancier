import {
  dossierAssembleeHtmlFragmentFromTemplate,
  dossierAssembleeIFrameStyle,
  echeancierHtmlFragmentFromTemplate,
  iFrameResizerScript,
  standardHtmlFragmentFromTemplate,
  standardIFrameStyle,
} from "@leximpact/echeancier-common/server"
import type { DossierParlementaire } from "@tricoteuses/assemblee"
import fs from "fs-extra"

import config from "$lib/server/config"

export async function dossierAssembleeHtmlFromTemplate({
  alt,
  dossierAssemblee,
  fetch,
}: {
  alt: boolean
  dossierAssemblee: DossierParlementaire
  fetch: (
    input: RequestInfo | URL,
    init?: RequestInit | undefined,
  ) => Promise<Response>
}): Promise<string> {
  const embeddedResponse = await fetch(
    new URL(
      `/embedded/dossiers/${dossierAssemblee.uid}`,
      config.frontOffice.url,
    ),
  )
  const embeddedPage = embeddedResponse.ok
    ? await embeddedResponse.text()
    : undefined

  let html = await fs.readFile(
    `templates/dossiers/${
      alt ? "alt/" : ""
    }calculer_retraite_base_nonsalaries_agricoles.html`,
    { encoding: "utf-8" },
  )
  html = html
    .replace(
      '<link href="/dyn/css/compiled/dossiers_legislatifs.css" rel="stylesheet" type="text/css" />',
      `
        <link href="/dyn/css/compiled/dossiers_legislatifs.css" rel="stylesheet" type="text/css" />
        <style>${dossierAssembleeIFrameStyle}</style>
      `,
    )
    .replaceAll(
      "https://www.assemblee-nationale.fr/dyn/16/dossiers",
      "/dyn/16/dossiers",
    )
    .replace(
      /calculer_retraite_base_nonsalaries_agricoles/g,
      dossierAssemblee.titreDossier.titreChemin as string,
    )
    .replace(
      /Calculer la retraite de base des non-salariés agricoles en fonction de leurs seules vingt-cinq meilleures années de revenus/g,
      dossierAssemblee.titreDossier.titre,
    )
    .replaceAll("DLR5L16N46472", dossierAssemblee.uid)
    .replace('<script src="/assets/build/leximpact_dosleg.js"></script>', "")
    .replace(
      /<iframe.*?<\/script>/s,
      ` <script>
          ${iFrameResizerScript}
        </script>
        ${dossierAssembleeHtmlFragmentFromTemplate({
          dossierAssemblee,
          embeddedPage,
          frontOfficeUrl: config.frontOffice.url,
        })}
      `,
    )

  return html
}

export async function echeancierHtmlFromTemplate({
  dossierAssembleeUid,
  fetch,
  loiJorfId,
}: {
  dossierAssembleeUid?: string | null
  fetch: (
    input: RequestInfo | URL,
    init?: RequestInit | undefined,
  ) => Promise<Response>
  loiJorfId: string
}): Promise<string> {
  const embeddedResponse = await fetch(
    new URL(`/embedded/${loiJorfId}`, config.frontOffice.url),
  )
  const embeddedPage = embeddedResponse.ok
    ? await embeddedResponse.text()
    : undefined

  let html = await fs.readFile("templates/cec/index.html", {
    encoding: "utf-8",
  })
  html = html
    .replace(
      '<link rel="stylesheet" href="/assets/build/organes_show.css">',
      `
        <style>${standardIFrameStyle}</style>
      `,
    )
    .replace('<script src="/assets/build/organes_show.js"></script>', "")
    .replaceAll(
      "https://www.assemblee-nationale.fr/dyn/16/dossiers",
      "/dyn/16/dossiers",
    )
    .replace(/\s*<nav class="page-breadcrumb">.*?<\/nav>/s, "")
    .replace(
      /<main id="main" role="main">.*?<\/main>/s,
      `
        <main id="main" role="main">
        <script>
          ${iFrameResizerScript}
        </script>
        ${echeancierHtmlFragmentFromTemplate({
          dossierAssembleeUid,
          embeddedPage,
          frontOfficeUrl: config.frontOffice.url,
          loiJorfId,
        })}
        </main>
      `,
    )
  return html
}

export async function standardHtmlFromTemplate({
  fragmentUrl,
}: {
  fragmentUrl: string
}): Promise<string> {
  let html = await fs.readFile("templates/cec/index.html", {
    encoding: "utf-8",
  })
  html = html
    .replace(
      '<link rel="stylesheet" href="/assets/build/organes_show.css">',
      `
        <style>${standardIFrameStyle}</style>
      `,
    )
    .replace('<script src="/assets/build/organes_show.js"></script>', "")
    .replaceAll(
      "https://www.assemblee-nationale.fr/dyn/16/dossiers",
      "/dyn/16/dossiers",
    )
    .replace(/\s*<nav class="page-breadcrumb">.*?<\/nav>/s, "")
    .replace(
      /<main id="main" role="main">.*?<\/main>/s,
      `
        <main id="main" role="main">
        <script>
          ${iFrameResizerScript}
        </script>
          ${standardHtmlFragmentFromTemplate({
            fragmentUrl,
            frontOfficeUrl: config.frontOffice.url,
          })}
        </main>
      `,
    )
  return html
}
