import "dotenv/config"

import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  echeancierDataDir: string
  frontOffice: {
    password?: string
    url: string
    username?: string
  }
}

const [config, error] = validateConfig({
  echeancierDataDir: process.env.ECHEANCIER_DATA_DIR,
  frontOffice: {
    password: process.env["FRONT_OFFICE_PASWWORD"],
    url: process.env["FRONT_OFFICE_URL"],
    username: process.env["FRONT_OFFICE_USERNAME"],
  },
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
export default config
