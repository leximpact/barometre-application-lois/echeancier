import { error } from "@sveltejs/kit"
import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"
import { dossierAssembleeHtmlFromTemplate } from "$lib/server/templates"

import type { EntryGenerator, RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config

export const entries = (async () => {
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  return lois
    .filter(
      ({ dossierAssembleeExtrait }) => dossierAssembleeExtrait !== undefined,
    )
    .map(({ dossierAssembleeExtrait, legislature }) => ({
      dossier:
        dossierAssembleeExtrait!.titreChemin ?? dossierAssembleeExtrait!.uid,
      legislature: legislature.toString(),
    }))
}) satisfies EntryGenerator

export const GET: RequestHandler = async ({ fetch, params }) => {
  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${params.dossier}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(404, `Dossier Assemblée ${params.dossier} non trouvé`)
  }
  const { dossierAssemblee } = (await fs.readJson(
    loiEtMesuresFilePath,
  )) as LoiEtMesures
  if (dossierAssemblee === undefined) {
    error(404, `Dossier Assemblée ${params.dossier} non trouvé`)
  }

  const html = await dossierAssembleeHtmlFromTemplate({
    alt: false,
    dossierAssemblee,
    fetch,
  })
  return new Response(html, {
    headers: { "Content-Type": "text/html; charset=UTF-8" },
  })
}
