import { error } from "@sveltejs/kit"
import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"
import { echeancierHtmlFromTemplate } from "$lib/server/templates"

import type { EntryGenerator, RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

const { echeancierDataDir } = config

export const entries = (async () => {
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  return lois.map(({ dossierAssembleeExtrait, loiJorfId }) => ({
    id: dossierAssembleeExtrait?.uid ?? loiJorfId,
  }))
}) satisfies EntryGenerator

export const GET: RequestHandler = async ({ fetch, params }) => {
  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${params.id}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(
      404,
      `Identifiant de loi ou de dossier législatif Assemblée incorrect : ${params.id}`,
    )
  }
  const { dossierAssemblee, loiJorfId } = (await fs.readJson(
    loiEtMesuresFilePath,
  )) as LoiEtMesures
  if (dossierAssemblee === undefined) {
    error(404, `Dossier législatif Assemblée ${params.id} non trouvé`)
  }

  const html = await echeancierHtmlFromTemplate({
    dossierAssembleeUid: dossierAssemblee.uid,
    fetch,
    loiJorfId,
  })
  return new Response(html, {
    headers: { "Content-Type": "text/html; charset=UTF-8" },
  })
}
