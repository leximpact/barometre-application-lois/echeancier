import { standardHtmlFromTemplate } from "$lib/server/templates"

import type { RequestHandler } from "./$types"

// For @sveltejs/adapter-static:
export const prerender = true

export const GET: RequestHandler = async () => {
  const html = await standardHtmlFromTemplate({
    fragmentUrl: "/embedded/fonctionnement",
  })

  return new Response(html, {
    headers: { "Content-Type": "text/html; charset=UTF-8" },
  })
}
