import { sveltekit } from "@sveltejs/kit/vite"
import morgan from "morgan"
import { defineConfig } from "vitest/config"

const middlewaresPlugin = {
  name: "echeancier-middlewares",
  async configureServer(server) {
    server.middlewares.use(morgan("dev"))
  },
}

export default defineConfig({
  plugins: [sveltekit(), middlewaresPlugin],
  test: {
    include: ["src/**/*.{test,spec}.{js,ts}"],
  },
})
