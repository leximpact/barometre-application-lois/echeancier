import type { DossierParlementaire, Organe } from "@tricoteuses/assemblee"
import {
  type DossierLegislatif,
  type Echeancier,
  type JorfArticle,
  type JorfSectionTa,
  type JorfTexteVersion,
  type JorfTextelr,
  type LegiArticle,
  type LegiTexteVersion,
} from "@tricoteuses/legal-explorer"
import fs from "fs-extra"
import { JSDOM } from "jsdom"
import path from "path"
import sade from "sade"

import {
  etatFromLigne,
  getOrganeAtDate,
  hashEcheancier,
  type EcheancierLigneEtendue,
  type EtatLigne,
  type Loi,
  type LoiEtMesures,
  type LoiStats,
  type MesuresData,
} from "@leximpact/echeancier-common"

import { classificationByDecret } from "$lib/server/classifications"
import config from "$lib/server/config"
import {
  assembleeDb as assembleeDbOrUndefined,
  legiDb as legiDbOrUndefined,
} from "$lib/server/databases"

if (assembleeDbOrUndefined === undefined) {
  throw new Error("Assemblee database is not configured.")
}
const assembleeDb = assembleeDbOrUndefined
if (legiDbOrUndefined === undefined) {
  throw new Error("LEGI database is not configured.")
}
const legiDb = legiDbOrUndefined

const {
  doleEcheanciersHtmlDir: doleEcheanciersHtmlDirUndefined,
  echeancierDataDir,
} = config
if (doleEcheanciersHtmlDirUndefined === undefined) {
  throw new Error(
    "Directory of data extracted from DOLE HTML pages of échéanciers is not configured.",
  )
}
const doleEcheanciersHtmlDir = doleEcheanciersHtmlDirUndefined

const decretRegexp =
  /^Décret\s+n°\s*(\d+-\d+)\s+du\s+((1er|\d{1,2})\s+[^\s]+\s+\d{4}|\d{1,2}\s*\/\s*\d{1,2}\s*\/\s*\d{4})$/
const decretWithOptionalNumberRegexp =
  /^Décret(\s+n°\s*(\d+-\d+))?\s+du\s+((1er|\d{1,2})\s+[^\s]+\s+\d{4}|\d{1,2}\s*\/\s*\d{1,2}\s*\/\s*\d{4})$/
const htmlLabelsAlternatives = [
  [
    "Article",
    "Base légale",
    "Objet",
    "Objectif de publication",
    "Entrée en vigueur différée", // optional
  ],
  [
    "Article",
    "Base légale",
    "Objet",
    "Objectif de publication / Décrets publiés",
    "Entrée en vigueur différée", // optional
  ],
  [
    "Article",
    "Base légale",
    "Objet",
    "Objectif initial de publication /Mesures publiées / Observations",
    "Entrée en vigueur différée", // optional
  ],
  [
    "Article",
    "Base légale",
    "Objet",
    "Objectif initial de publication /Mesures publiées / Observation s",
    "Entrée en vigueur différée", // optional
  ],
]
const yesterday = new Date()
yesterday.setDate(yesterday.getDate() - 1)
const yesterdayString = yesterday.toISOString().split("T")[0]

async function addLignes({
  ARTICLE,
  BASE_LEGALE,
  DATE_PREVUE,
  dossier,
  lignes,
  objectifTdElement,
  OBJET,
  trIndex,
  withHtml,
}: {
  ARTICLE?: string
  BASE_LEGALE?: string
  DATE_PREVUE?: string
  dossier: DossierLegislatif
  lignes: EcheancierLigneEtendue[]
  objectifTdElement: HTMLTableCellElement
  OBJET?: string
  trIndex: number
  withHtml: boolean
}): Promise<void> {
  const lignesInfos: Array<{
    aElementOrText: HTMLAnchorElement | Text
    cidLoiCible?: string
  }> = []
  for (const aElementOrText of walkAElementsOrTexts(
    dossier,
    objectifTdElement,
    trIndex,
  )) {
    switch (aElementOrText.nodeType) {
      case 1: {
        // Node.ELEMENT_NODE with tagName === "A"
        const aElement = aElementOrText as HTMLAnchorElement
        const decret = aElement.textContent?.trim() || undefined
        if (
          decret !== undefined &&
          decretWithOptionalNumberRegexp.test(decret)
        ) {
          const aElementHref = aElement.getAttribute("href")
          const match = aElementHref?.match(
            /^(https:\/\/www\.legifrance\.gouv\.fr)?\/(jorf|loda)\/id\/(JORFTEXT\d{12})(\/\d{4}-\d{2}-\d{2})?\/?(\?|$)/,
          )
          if (match == null) {
            throw new Error(
              `Dans le dossier ${dossier.META.META_COMMUN.ID}, à la ligne ${trIndex} et la colonne Objectif/Décret de l'échéancier, le lien ${aElementHref} ne pointe pas vers un JORFTEXT`,
            )
          }
          lignesInfos.push({
            aElementOrText,
            cidLoiCible: match[3],
          })
        }
        break
      }

      case 3: {
        // Node.TEXT_NODE
        const decret = aElementOrText.nodeValue?.trim() || undefined
        if (decret !== undefined) {
          const match = decret.match(decretRegexp)
          if (match !== null) {
            const cidLoiCible = (
              await legiDb<{ id: string }[]>`
                SELECT id
                FROM texte_version
                WHERE
                  nature_et_num = ${`DECRET.${match[1]}`}
                  AND data -> 'META' -> 'META_COMMUN' ->> 'ORIGINE' = 'JORF'
              `
            )[0]?.id
            lignesInfos.push({
              aElementOrText,
              cidLoiCible,
            })
          }
        }
        break
      }

      default: {
        throw new Error(
          `Dans le dossier ${dossier.META.META_COMMUN.ID}, à la ligne ${trIndex}, nœud de type inattendu ${aElementOrText.nodeType} dans :\n${objectifTdElement.innerHTML}`,
        )
      }
    }
  }

  if (lignesInfos.length === 0) {
    const decretText = objectifTdElement.textContent?.trim() || undefined
    const decretHtml = withHtml
      ? cleanDecretElement(objectifTdElement).innerHTML.trim() || undefined
      : undefined
    lignes.push({
      ARTICLE,
      BASE_LEGALE,
      CID_LOI_CIBLE: undefined,
      DATE_PREVUE,
      DECRET: decretText,
      DECRET_HTML: decretHtml === decretText ? undefined : decretHtml,
      NUMERO_ORDRE: (lignes.length + 1).toString(),
      OBJET,
    })
  } else {
    for (const [
      index,
      { aElementOrText, cidLoiCible },
    ] of lignesInfos.entries()) {
      // Look for largest HTML element containing only this decree.
      let containingNode: HTMLElement | Text = objectifTdElement
      iterOthers: for (const [
        otherIndex,
        { aElementOrText: otherAElementOrText },
      ] of lignesInfos.entries()) {
        if (otherIndex == index) {
          continue
        }
        for (
          let ancestor: HTMLElement | Text | null = aElementOrText;
          ancestor !== null && ancestor !== containingNode;
          ancestor = ancestor.parentElement
        ) {
          for (
            let otherAncestor: HTMLElement | Text | null = otherAElementOrText;
            otherAncestor !== null;
            otherAncestor = otherAncestor.parentElement
          ) {
            if (ancestor.parentElement === otherAncestor) {
              containingNode = ancestor
              continue iterOthers
            }
            if (otherAncestor === objectifTdElement) {
              break
            }
          }
        }
      }

      const decretText = containingNode.textContent?.trim() || undefined
      const decretHtml = withHtml
        ? containingNode.nodeType === 1 /* Node.Element */
          ? cleanDecretElement(containingNode as HTMLElement)[
              containingNode === objectifTdElement ? "innerHTML" : "outerHTML"
            ].trim() || undefined
          : decretText
        : undefined
      lignes.push({
        ARTICLE,
        BASE_LEGALE,
        CID_LOI_CIBLE: cidLoiCible,
        DATE_PREVUE,
        DECRET: decretText,
        DECRET_HTML: decretHtml === decretText ? undefined : decretHtml,
        NUMERO_ORDRE: (lignes.length + 1).toString(),
        OBJET,
      })
    }
  }
}

function assertNever(name: string, value: never): never {
  throw `Unexpected value for "${name}" : ${value}`
}

function cleanDecretElement(containingElement: HTMLElement): HTMLElement {
  containingElement.querySelectorAll("a").forEach((aElement) => {
    const url = aElement.getAttribute("href")
    if (url !== null) {
      aElement.setAttribute(
        "href",
        new URL(url, "https://www.legifrance.gouv.fr/").toString(),
      )
      aElement.setAttribute(
        "onmouseout",
        'this.style.color="rgb(63 117 175 / 1)"',
      )
      aElement.setAttribute(
        "onmouseover",
        'this.style.color="rgb(35 63 107 / 1)"',
      )
      aElement.setAttribute(
        "style",
        "color: rgb(63 117 175 / 1); text-decoration-line: underline",
      )
      aElement.setAttribute("target", "_blank")

      const svg = aElement.ownerDocument.createElement("svg")
      svg.setAttribute("class", "inline-block align-[-0.15rem]")
      svg.setAttribute("height", "1em")
      svg.setAttribute("viewBox", "0 0 24 24")
      svg.setAttribute("width", "1em")
      // <svg viewBox="0 0 24 24" width="1.2em" height="1.2em" class="inline-block align-[-0.15rem]">
      svg.innerHTML =
        '<path fill="currentColor" d="M10 6v2H5v11h11v-5h2v6a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1zm11-3v8h-2V6.413l-7.793 7.794l-1.414-1.414L17.585 5H13V3z"></path>'
      aElement.append(svg)
    }
  })
  return containingElement
}

async function generateEcheanciersJson({
  only,
}: { only?: string } = {}): Promise<void> {
  const echeanciersDir = path.join(echeancierDataDir, "echeanciers")
  await fs.ensureDir(echeanciersDir)
  const existingEcheanciersFilenames = new Set(await fs.readdir(echeanciersDir))

  // Note: "2002-06-19" below is the first day of the XIIth legislature.
  // There is no Dila "dossier législatif" before this date.
  const texteVersionCursor = legiDb<
    {
      assemblee_uid: string | null
      commission_fond_assemblee_uid: string | null
      data: JorfTexteVersion
      id: string
    }[]
  >`
    SELECT assemblee_uid, commission_fond_assemblee_uid, data, texte_version.id
    FROM texte_version
    LEFT JOIN texte_version_dossier_legislatif_assemblee_associations
      ON texte_version.id = texte_version_dossier_legislatif_assemblee_associations.id
    WHERE
      data -> 'META' -> 'META_COMMUN' ->> 'NATURE' IN (
        'LOI',
        'LOI_CONSTIT',
        'LOI_ORGANIQUE'
      )
      AND data -> 'META' -> 'META_COMMUN' ->> 'ORIGINE' = 'JORF'
      AND data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_CHRONICLE' ->> 'DATE_TEXTE' >= '2002-06-19'
      AND data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_VERSION' ->> 'TITREFULL' NOT LIKE '%(rectificatif)%'
      ${only === undefined ? legiDb`` : legiDb`AND texte_version.id = ${only}`}
  `.cursor(100)
  const commissionFondAssembleeUidByLoiJorfId: { [loiJorfId: string]: string } =
    {}
  const dossierAssembleeUidByLoiJorfId: { [id: string]: string } = {}
  const loiJorfIdByDossierAssembleeUid: { [uid: string]: string } = {}
  const texteVersionByJorfId = new Map<string, JorfTexteVersion>()
  for await (const rows of texteVersionCursor) {
    for (const {
      assemblee_uid,
      commission_fond_assemblee_uid,
      data: texteVersion,
      id,
    } of rows) {
      if (assemblee_uid !== null) {
        dossierAssembleeUidByLoiJorfId[id] = assemblee_uid
        loiJorfIdByDossierAssembleeUid[assemblee_uid] = id
      }
      if (commission_fond_assemblee_uid !== null) {
        commissionFondAssembleeUidByLoiJorfId[id] =
          commission_fond_assemblee_uid
      }
      // Remove content from "dossier législatif" to shrink its size.
      delete texteVersion.ABRO
      delete texteVersion.ENTREPRISE
      delete texteVersion.NOTICE
      delete texteVersion.RECT
      delete texteVersion.SIGNATAIRES
      delete texteVersion.SM
      delete texteVersion.TP
      delete texteVersion.VISAS
      texteVersionByJorfId.set(id, texteVersion)
    }
  }

  const dossierByLoiJorfId = Object.fromEntries(
    (
      await legiDb<
        { data: DossierLegislatif; jorf_texte_principal_id: string }[]
      >`
        SELECT data, jorf_texte_principal_id
        FROM dossier_legislatif
        WHERE
          jorf_texte_principal_id IS NOT NULL
          AND data -> 'META' -> 'META_DOSSIER_LEGISLATIF' ->> 'TYPE' = 'LOI_PUBLIEE'
          ${only === undefined ? legiDb`` : legiDb`AND jorf_texte_principal_id = ${only}`}
        ORDER BY data -> 'CONTENU' -> 'ECHEANCIER' ->> '@derniere_maj' DESC NULLS LAST
      `
    ).map(({ data: dossier, jorf_texte_principal_id }) => {
      // Remove content from "dossier législatif" to shrink its size.
      // @ts-expect-error: The operand of a 'delete' operator must be optional.
      delete dossier.CONTENU.ARBORESCENCE
      delete dossier.CONTENU.CONTENU_DOSSIER_1
      delete dossier.CONTENU.CONTENU_DOSSIER_2
      delete dossier.CONTENU.CONTENU_DOSSIER_3
      delete dossier.CONTENU.CONTENU_DOSSIER_4
      delete dossier.CONTENU.EXPOSE_MOTIF
      return [jorf_texte_principal_id, dossier]
    }),
  )
  for (const [loiJorfId, dossier] of Object.entries(dossierByLoiJorfId)) {
    // Replace dossier.CONTENU.ECHEANCIER with its normalized version.

    // Normalize new version of échéancier.
    const echeancierLegifranceHtmlFilePath = path.join(
      doleEcheanciersHtmlDir,
      `${dossier.META.META_COMMUN.ID}_echeancier_table.html`,
    )
    const echeancierLegifranceHtmlText = (await fs.pathExists(
      echeancierLegifranceHtmlFilePath,
    ))
      ? await fs.readFile(echeancierLegifranceHtmlFilePath, {
          encoding: "utf-8",
        })
      : undefined
    const echeancier = await normalizeEcheancier(
      dossier,
      echeancierLegifranceHtmlText,
    )

    if (echeancier !== undefined) {
      // Retrieve latest normalized version of échéancier.
      const echeancierFilename = `${loiJorfId}.json`
      const echeancierFilePath = path.join(echeanciersDir, echeancierFilename)
      const echeanciers = (await fs.pathExists(echeancierFilePath))
        ? ((await fs.readJson(echeancierFilePath)) as LoiEtMesures).echeanciers
        : []
      const latestEcheancier = echeanciers[0]

      // if the latest échéancier in history is the same as the new one,
      // use the latest échéancier in history, to preserve @derniere_maj.
      dossier.CONTENU.ECHEANCIER =
        latestEcheancier !== undefined &&
        hashEcheancier(latestEcheancier) === hashEcheancier(echeancier)
          ? latestEcheancier
          : echeancier
    }
  }

  const commissionByUid = Object.fromEntries(
    (
      await assembleeDb<{ data: Organe; uid: string }[]>`
        SELECT data, uid
        FROM organes
        WHERE uid IN ${legiDb([
          ...new Set(Object.values(commissionFondAssembleeUidByLoiJorfId)),
        ])}
        ORDER BY data -> 'viMoDe' ->> 'dateDebut'
      `
    ).map(({ data, uid }) => [uid, data]),
  )

  const assembleeByLegislature = Object.fromEntries(
    (
      await assembleeDb<{ data: Organe }[]>`
        SELECT data
        FROM organes
        WHERE data ->> 'codeType' = 'ASSEMBLEE'
        ORDER BY data -> 'viMoDe' ->> 'dateDebut'
      `
    ).map(({ data }) => [data.legislature, data]),
  ) as { [legislature: string]: Organe }
  const assemblees = Object.values(assembleeByLegislature)

  const gouvernements = (
    await assembleeDb<{ data: Organe }[]>`
      SELECT data
      FROM organes
      WHERE data ->> 'codeType' = 'GOUVERNEMENT'
      ORDER BY data -> 'viMoDe' ->> 'dateDebut'
    `
  ).map(({ data }) => data)

  const presidences = (
    await assembleeDb<{ data: Organe }[]>`
      SELECT data
      FROM organes
      WHERE data ->> 'codeType' = 'PRESREP'
      ORDER BY data -> 'viMoDe' ->> 'dateDebut'
    `
  ).map(({ data }) => data)

  const loisJorfIdByDecretId = new Map<string, string[]>()
  for (const [loiJorfId, dossier] of Object.entries(dossierByLoiJorfId)) {
    for (const ligne of dossier.CONTENU.ECHEANCIER?.LIGNE ?? []) {
      if (ligne.CID_LOI_CIBLE !== undefined) {
        let loisJorfId = loisJorfIdByDecretId.get(ligne.CID_LOI_CIBLE)
        if (loisJorfId === undefined) {
          loisJorfId = []
          loisJorfIdByDecretId.set(ligne.CID_LOI_CIBLE, loisJorfId)
        }
        loisJorfId.push(loiJorfId)
      }
    }
  }

  const oneDay = 24 * 60 * 60 * 1000 // hours * minutes * seconds * milliseconds
  const delaiApplicationByLoiJorfIdByDecretId: {
    [decretId: string]: { [loiJorfId: string]: number }
  } = {}
  for (const { data, id } of await legiDb<{ data: JorfTextelr; id: string }[]>`
    SELECT data, id
    FROM textelr
    WHERE
      id IN ${legiDb([...loisJorfIdByDecretId.keys()])}
  `) {
    const decretDatePubli = data.META.META_SPEC.META_TEXTE_CHRONICLE.DATE_PUBLI
    const loisJorfId = loisJorfIdByDecretId.get(id) as string[]
    delaiApplicationByLoiJorfIdByDecretId[id] = Object.fromEntries(
      loisJorfId.map((loiJorfId) => {
        const texteVersion = texteVersionByJorfId.get(loiJorfId)
        return [
          loiJorfId,
          texteVersion === undefined
            ? 0
            : Math.max(
                Math.round(
                  ((new Date(decretDatePubli) as unknown as number) -
                    (new Date(
                      texteVersion.META.META_SPEC.META_TEXTE_CHRONICLE.DATE_TEXTE,
                    ) as unknown as number)) /
                    oneDay,
                ),
                0,
              ),
        ]
      }),
    )
  }

  const dossierAssembleeExtraitByUid: {
    [uid: string]: {
      commissionFondUid?: string
      dossierSenatId?: string
      titreChemin?: string
      uid: string
    }
  } = {}
  const mesuresByLoiJorfId: {
    [loiJorfId: string]:
      | Array<{
          delaiApplication?: number
          etat: EtatLigne
        }>
      | undefined
  } = {}
  const statsByLoiJorfId: { [loiJorfId: string]: LoiStats } = {}
  for (const [loiJorfId, texteVersion] of texteVersionByJorfId.entries()) {
    const dossier = dossierByLoiJorfId[loiJorfId]
    const echeancierFilename = `${loiJorfId}.json`
    const echeancierFilePath = path.join(echeanciersDir, echeancierFilename)
    const echeanciers = (await fs.pathExists(echeancierFilePath))
      ? ((await fs.readJson(echeancierFilePath)) as LoiEtMesures).echeanciers
      : []
    let echeancierLegifranceUrl: string | undefined = undefined
    if (dossier !== undefined) {
      const echeancierLegifranceDataFilePath = path.join(
        doleEcheanciersHtmlDir,
        `${dossier.META.META_COMMUN.ID}_echeancier.json`,
      )
      ;({ legifranceUrl: echeancierLegifranceUrl } = (
        (await fs.pathExists(echeancierLegifranceDataFilePath))
          ? await fs.readJson(echeancierLegifranceDataFilePath)
          : {}
      ) as {
        legifranceUrl?: string
      })
      const echeancier = dossier.CONTENU.ECHEANCIER
      // Caution: Echeanciers are in reverse chronological order.
      if (
        echeancier !== undefined &&
        (echeanciers.length == 0 ||
          // JSON.parse(JSON.stringify(echeancier) is used to remove undefined values.
          hashEcheancier(echeancier) !== hashEcheancier(echeanciers[0]))
      ) {
        echeanciers.unshift(echeancier)
      }
    }

    const mesures = dossier?.CONTENU.ECHEANCIER?.LIGNE.map((ligne) => {
      // Note: delaiApplicationByLoiJorfIdByDecretId[ligne.CID_LOI_CIBLE] is equal to undefined
      // When a decree is not in JORF database
      return {
        delaiApplication:
          ligne.CID_LOI_CIBLE === undefined
            ? undefined
            : delaiApplicationByLoiJorfIdByDecretId[ligne.CID_LOI_CIBLE]?.[
                loiJorfId
              ],
        etat: etatFromLigne(classificationByDecret, ligne),
      }
    })
    mesuresByLoiJorfId[loiJorfId] = mesures

    const dossierAssembleeUid = dossierAssembleeUidByLoiJorfId[loiJorfId]
    const { commission_fond_uid, data: dossierAssemblee } =
      (dossierAssembleeUid === undefined
        ? undefined
        : (
            await assembleeDb<
              {
                commission_fond_uid: string | null
                data: DossierParlementaire
              }[]
            >`
              SELECT commission_fond_uid, data
              FROM dossiers
              WHERE uid = ${dossierAssembleeUid}
            `
          )[0]) ?? {}

    if (dossierAssemblee !== undefined) {
      dossierAssembleeExtraitByUid[dossierAssembleeUid] = {
        commissionFondUid: commission_fond_uid ?? undefined,
        dossierSenatId: /dossier-legislatif\/(.*)\.html/.exec(
          dossierAssemblee.titreDossier.senatChemin ?? "",
        )?.[1],
        titreChemin: dossierAssemblee.titreDossier.titreChemin,
        uid: dossierAssembleeUid,
      }
    }

    const loiStats = generateLoiStats({
      dossier,
      mesures,
      texteVersion,
    })
    statsByLoiJorfId[loiJorfId] = loiStats

    const acteAnAppliRapport = dossierAssemblee?.actesLegislatifs
      ?.find((acte) => (acte.codeActe as unknown as string) === "AN-APPLI")
      ?.actesLegislatifs?.find(
        (acte) => (acte.codeActe as unknown as string) === "AN-APPLI-RAPPORT",
      )
    const rapportApplication =
      acteAnAppliRapport?.texteAssocieRef === undefined
        ? undefined
        : (
            await assembleeDb<{ data: Document }[]>`
              SELECT data FROM documents
              WHERE uid = ${acteAnAppliRapport.texteAssocieRef}
            `
          ).map(({ data }) => data)[0]

    const referenceArticleParId: { [id: string]: JorfArticle | LegiArticle } =
      {}
    const referenceSectionTaParId: { [id: string]: JorfSectionTa } = {}
    const referenceTexteVersionParId: {
      [id: string]: JorfTexteVersion | LegiTexteVersion
    } = {}
    for (const { data, id } of await legiDb<
      { data: JorfTexteVersion | LegiTexteVersion; id: string }[]
    >`
      SELECT data, texte_version.id
      FROM texte_version_lien
      INNER JOIN texte_version
        ON texte_version_lien.texte_version_id = texte_version.id
      WHERE
        (
          texte_version_lien.id = ${loiJorfId}
          OR texte_version_lien.cidtexte = ${loiJorfId}
        )
    `) {
      referenceTexteVersionParId[id] = data
    }
    for (const { cidtexte, data, id, typelien } of await legiDb<
      {
        cidtexte: string | null
        data: JorfTexteVersion
        id: string
        typelien: string
      }[]
    >`
      SELECT data, texte_version_lien.cidtexte, texte_version_lien.id, texte_version_lien.typelien
      FROM texte_version_lien
      INNER JOIN texte_version
        ON texte_version_lien.cidtexte = texte_version.id
      WHERE
        texte_version_lien.texte_version_id = ${loiJorfId}
    `) {
      if (cidtexte !== null) {
        referenceTexteVersionParId[cidtexte] = data
      }
      if (id !== cidtexte) {
        if (id.match(/^(JORF|LEGI)ARTI\d{12}$/) !== null) {
          if (referenceArticleParId[id] === undefined) {
            const article = (
              await legiDb<{ data: JorfArticle | LegiArticle }[]>`
                SELECT data
                FROM article
                WHERE id = ${id}
              `
            ).map(({ data }) => data)[0]
            referenceArticleParId[id] = article
          }
        } else if (id.match(/^KALIARTI\d{12}$/) !== null) {
          // TODO: Handle KALIARTI.
          console.warn(
            `Currently ignoring link containing a KALI ID: ${id} (cidtexte: ${cidtexte}, typelien: ${typelien})`,
          )
        } else if (id.match(/^JORFSCTA\d{12}$/) !== null) {
          if (referenceSectionTaParId[id] === undefined) {
            const sectionTa = (
              await legiDb<{ data: JorfSectionTa }[]>`
                SELECT data
                FROM section_ta
                WHERE id = ${id}
              `
            ).map(({ data }) => data)[0]
            referenceSectionTaParId[id] = sectionTa
          }
        } else if (id.match(/^LEGITEXT\d{12}$/) !== null) {
          if (referenceTexteVersionParId[id] === undefined) {
            const texteVersion = (
              await legiDb<{ data: LegiTexteVersion }[]>`
                SELECT data
                FROM texte_version
                WHERE id = ${id}
              `
            ).map(({ data }) => data)[0]
            referenceTexteVersionParId[id] = texteVersion
          }
        } else {
          throw new Error(
            `Link contains an unexpected ID: ${id} (cidtexte: ${cidtexte}, typelien: ${typelien})`,
          )
        }
      }
    }
    for (const { data, id } of await legiDb<
      { data: JorfArticle | LegiArticle; id: string }[]
    >`
      SELECT data, article.id
      FROM article_lien
      INNER JOIN article
        ON article_lien.id = article.id
      WHERE
        article_lien.cidtexte = ${loiJorfId}
    `) {
      referenceArticleParId[id] = data
    }
    // JORF Articles have no LIENS.LIEN, so it is useless to search for them.
    // => We don't look for article_lien whose article_id belongs to the law.

    await fs.writeJson(
      echeancierFilePath,
      {
        ...loiStats,
        dossier,
        dossierAssemblee,
        echeancierLegifranceUrl,
        echeanciers,
        loiJorfId,
        rapportApplication,
        referenceArticleParId:
          Object.keys(referenceArticleParId).length === 0
            ? undefined
            : referenceArticleParId,
        referenceSectionTaParId:
          Object.keys(referenceSectionTaParId).length === 0
            ? undefined
            : referenceSectionTaParId,
        referenceTexteVersionParId:
          Object.keys(referenceTexteVersionParId).length === 0
            ? undefined
            : referenceTexteVersionParId,
        texteVersion,
      } as LoiEtMesures,
      { replacer, spaces: 2 },
    )
    existingEcheanciersFilenames.delete(echeancierFilename)

    if (dossierAssemblee !== undefined) {
      const echeancierUidFilename = `${dossierAssemblee.uid}.json`
      const echeancierUidFilePath = path.join(
        echeanciersDir,
        echeancierUidFilename,
      )
      await fs.remove(echeancierUidFilePath)
      await fs.ensureSymlink(echeancierFilePath, echeancierUidFilePath)
      existingEcheanciersFilenames.delete(echeancierUidFilename)

      if (dossierAssemblee.titreDossier.titreChemin !== undefined) {
        const echeancierTitreCheminFilename = `${dossierAssemblee.titreDossier.titreChemin}.json`
        const echeancierTitreCheminFilePath = path.join(
          echeanciersDir,
          echeancierTitreCheminFilename,
        )
        await fs.remove(echeancierTitreCheminFilePath)
        await fs.ensureSymlink(
          echeancierFilePath,
          echeancierTitreCheminFilePath,
        )
        existingEcheanciersFilenames.delete(echeancierTitreCheminFilename)
      }
    }
  }

  if (only === undefined) {
    const lois: Loi[] = [...texteVersionByJorfId.values()].map(
      (texteVersion) => {
        const datePublication =
          texteVersion.META.META_SPEC.META_TEXTE_CHRONICLE.DATE_TEXTE
        const loiJorfId = texteVersion.META.META_COMMUN.ID
        const dossier = dossierByLoiJorfId[loiJorfId]
        const dossierAssembleeUid = dossierAssembleeUidByLoiJorfId[loiJorfId]
        const motsCles =
          texteVersion.META.META_SPEC.META_TEXTE_VERSION.MCS_TXT?.MC
        return {
          ...statsByLoiJorfId[loiJorfId],
          commissionFondAssembleeUid:
            commissionFondAssembleeUidByLoiJorfId[loiJorfId],
          datePublication,
          dossierAssembleeExtrait:
            dossierAssembleeUid === undefined
              ? undefined
              : dossierAssembleeExtraitByUid[dossierAssembleeUid],
          dossierId: dossier?.META.META_COMMUN.ID,
          legislature:
            dossier?.META.META_DOSSIER_LEGISLATIF.LEGISLATURE?.NUMERO ??
            parseInt(
              (getOrganeAtDate(assemblees, datePublication) as Organe)
                .legislature as string,
            ),
          loiJorfId,
          mesures: mesuresByLoiJorfId[loiJorfId],
          motsCles,
          titreComplet:
            texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL?.replace(
              /\s*\(\d+\)$/,
              "",
            ),
        }
      },
    )

    await fs.writeJson(
      path.join(echeanciersDir, "index.json"),
      {
        assembleeByLegislature,
        commissionByUid,
        gouvernements,
        lois,
        presidences,
      } as MesuresData,
      { replacer, spaces: 2 },
    )
    existingEcheanciersFilenames.delete("index.json")

    for (const obsoleteEcheancierFilename of existingEcheanciersFilenames) {
      await fs.remove(path.join(echeanciersDir, obsoleteEcheancierFilename))
    }
  }
}

function generateLoiStats({
  dossier,
  mesures,
  texteVersion,
}: {
  dossier?: DossierLegislatif
  mesures?: Array<{ etat: EtatLigne }>
  texteVersion: JorfTexteVersion
}): LoiStats {
  const titreFull = texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL
  const autorisationAccordInternational =
    titreFull !== undefined &&
    [
      /\s+autorisant\s+l['’]accession\s+/i,
      /\s+autorisant\s+l['’]adhésion\s+/i,
      /\s+autorisant\s+l['’]approbation\s+/i,
      /\s+autorisant\s+la\s+ratification\s+/i,
    ].some((regExp) => titreFull.match(regExp) !== null)
  const echeancier = dossier?.CONTENU.ECHEANCIER
  const libelleTexte1 = dossier?.CONTENU.LIBELLE_TEXTE_1
  const applicationDirecteDeclaree =
    autorisationAccordInternational ||
    (libelleTexte1 !== undefined &&
      libelleTexte1.match(
        /Loi\s+n['’]appelant\s+pas\s+de\s+décret\s+d['’]application/i,
      ) !== null &&
      echeancier === undefined)
  const ratificationOrdonnances =
    titreFull !== undefined &&
    [
      /\s+ratifiant\s+l['’]ordonnance\s+/i,
      /\s+ratifiant\s+diverses\s+ordonnances\s+/i,
      /\s+ratifiant\s+les\s+ordonnances\s+/i,
    ].some((regExp) => titreFull.match(regExp) !== null)
  const habilitationOrdonnances =
    !ratificationOrdonnances &&
    titreFull !== undefined &&
    [
      /\s+habilitant\s+le\s+Gouvernement\s+/i,
      /\s+d['’]habilitation\s+à\s+prendre\s+par\s+ordonnances\s+/i,
      /\s+portant\s+habilitation\s+du\s+Gouvernement\s+/i,
    ].some((regExp) => titreFull.match(regExp) !== null)
  let nombreMesuresAAppliquer = 0
  let nombreMesuresAppliquees = 0
  let tauxApplicationMesures: number
  if (applicationDirecteDeclaree) {
    // Law doesn't need any decree, consider it to be fully applied.
    tauxApplicationMesures = 1
  } else if (mesures === undefined) {
    // Law has no schedule and we don't know whether it does need any decree.
    // => Consider that law is not applied.
    tauxApplicationMesures = 0
  } else {
    for (const { etat } of mesures) {
      switch (etat) {
        case "applique":
          nombreMesuresAAppliquer++
          nombreMesuresAppliquees++
          break
        case "caduc":
          // Since no mesure is needed, we don't count this line as a mesure.
          break
        case "en_attente_application":
          // Assume that the mesure needs a decret and that it has not been published.
          nombreMesuresAAppliquer++
          break
        default:
          assertNever("etat", etat)
      }
    }
    // If all mesures are caduc =>  consider that law is "d'application directe"
    tauxApplicationMesures =
      nombreMesuresAAppliquer === 0
        ? 1
        : nombreMesuresAppliquees / nombreMesuresAAppliquer
  }
  return {
    applicationDirecte:
      nombreMesuresAAppliquer === 0 && tauxApplicationMesures === 1,
    autorisationAccordInternational,
    habilitationOrdonnances,
    nombreMesuresAAppliquer,
    nombreMesuresAppliquees,
    ratificationOrdonnances,
    sansEcheancier: echeancier === undefined && !applicationDirecteDeclaree,
    tauxApplicationMesures,
  }
}

async function normalizeEcheancier(
  dossier: DossierLegislatif,
  echeancierLegifranceHtmlText?: string,
): Promise<Echeancier | undefined> {
  if (echeancierLegifranceHtmlText !== undefined) {
    // HTML harvested from Légifrance web site
    const { document } = new JSDOM(echeancierLegifranceHtmlText).window
    const tableElements = document.querySelectorAll("table")
    if (tableElements.length == 0) {
      throw new Error(
        `Dans le dossier ${dossier.META.META_COMMUN.ID}, la page échéancier n'a pas de table`,
      )
    }

    const lignes: EcheancierLigneEtendue[] = []
    const tableElement = tableElements[0]
    const theadElement = tableElement.querySelector("thead")
    if (theadElement !== null) {
      // Pre-2024 syntax
      const labels = [
        "Articles",
        "Base légale",
        "Objet",
        "Objectif initial de publication / Décrets publiés / Observations",
      ]
      const thElements = theadElement?.querySelectorAll("tr th")
      const texts = [...thElements].map((tdElement) =>
        tdElement?.textContent?.trim(),
      )
      if (
        texts.length !== 4 ||
        texts.some((text, index) => text !== labels[index])
      ) {
        throw new Error(
          `Dans le dossier ${dossier.META.META_COMMUN.ID}, les entêtes de l'échéancier (${texts.join(", ")}) ne correspondent pas à ceux attendus`,
        )
      }

      for (const [trIndex, trElement] of document
        .querySelectorAll("table tbody tr")
        .entries()) {
        const tdElements = trElement.querySelectorAll("td")
        if (tdElements.length !== 4) {
          throw new Error(
            `Dans le dossier ${dossier.META.META_COMMUN.ID}, la ligne ${trIndex} de l'échéancier n'a pas le nombre de cellules attendu`,
          )
        }
        let cidLoiCible: string | undefined = undefined
        const aElements = tdElements[3].querySelectorAll("a")
        if (aElements.length > 1) {
          throw new Error(
            `Dans le dossier ${dossier.META.META_COMMUN.ID}, à la ligne ${trIndex} et la colonne 3 de l'échéancier, se trouvent plusieurs liens au lieu d'un`,
          )
        }
        if (aElements.length === 1) {
          const aElementHref = aElements[0].getAttribute("href")
          const match = aElementHref?.match(/^\/loda\/id\/(JORFTEXT\d{12})$/)
          if (match == null) {
            throw new Error(
              `Dans le dossier ${dossier.META.META_COMMUN.ID}, à la ligne ${trIndex} et la colonne 3 de l'échéancier, le lien ${aElementHref} ne pointe pas vers un décret`,
            )
          }
          cidLoiCible = match[1]
        }
        lignes.push({
          ARTICLE: tdElements[0]?.textContent?.trim() || undefined,
          BASE_LEGALE: tdElements[1]?.textContent?.trim() || undefined,
          CID_LOI_CIBLE: cidLoiCible,
          // DATE_PREVUE: tdElements[4]?.textContent?.trim() || undefined,
          DECRET: tdElements[3]?.textContent?.trim() || undefined,
          // DECRET_HTML is kept empty.
          NUMERO_ORDRE: (trIndex + 1).toString(),
          OBJET: tdElements[2]?.textContent?.trim() || undefined,
        })
      }
    } else {
      // 2024 and later syntax
      let skipNumeroOrdreColumn = false
      for (const [trIndex, trElement] of document
        .querySelectorAll("table tbody tr")
        .entries()) {
        const tdElements = [...trElement.querySelectorAll("td")]
        if (trIndex === 0) {
          const texts = tdElements.map((tdElement) =>
            tdElement?.textContent?.trim(),
          )
          if (texts[0] === "N° d’ordre") {
            texts.shift()
            skipNumeroOrdreColumn = true
          }
          if (
            (texts.length !== 4 && texts.length !== 5) ||
            htmlLabelsAlternatives.every((labels) =>
              texts.some((text, index) => text !== labels[index]),
            )
          ) {
            throw new Error(
              `Dans le dossier ${dossier.META.META_COMMUN.ID}, les entêtes de l'échéancier (${texts.join(", ")}) ne correspondent pas à ceux attendus`,
            )
          }
        } else if (tdElements.length <= 2) {
          // This is a followup line: same article but different decret.
          const { ARTICLE, BASE_LEGALE, OBJET } = lignes.at(
            -1,
          ) as EcheancierLigneEtendue
          await addLignes({
            ARTICLE,
            BASE_LEGALE,
            DATE_PREVUE: tdElements[1]?.textContent?.trim() || undefined,
            dossier,
            lignes,
            objectifTdElement: tdElements[0],
            OBJET,
            trIndex,
            withHtml: true,
          })
        } else {
          if (skipNumeroOrdreColumn) {
            tdElements.shift()
          }
          await addLignes({
            ARTICLE: tdElements[0]?.textContent?.trim() || undefined,
            BASE_LEGALE: tdElements[1]?.textContent?.trim() || undefined,
            DATE_PREVUE: tdElements[4]?.textContent?.trim() || undefined,
            dossier,
            lignes,
            objectifTdElement: tdElements[3],
            OBJET: tdElements[2]?.textContent?.trim() || undefined,
            trIndex,
            withHtml: true,
          })
        }
      }
    }
    return {
      "@derniere_maj": yesterdayString,
      // Remove undefined values:
      LIGNE: JSON.parse(JSON.stringify(lignes)),
    }
  }

  const echeancierHtmlText = dossier.CONTENU.CONTENU_DOSSIER_5
  if (echeancierHtmlText !== undefined) {
    // HTML in XML
    let skipNumeroOrdreColumn = false
    const { document } = new JSDOM(echeancierHtmlText).window
    const lignes: EcheancierLigneEtendue[] = []
    for (const [trIndex, trElement] of document
      .querySelectorAll("table tbody tr")
      .entries()) {
      const tdElements = [...trElement.querySelectorAll("td")]
      if (trIndex === 0) {
        const texts = tdElements.map((tdElement) =>
          tdElement?.textContent?.trim(),
        )
        if (texts[0] === "N° d’ordre") {
          texts.shift()
          skipNumeroOrdreColumn = true
        }
        if (
          (texts.length !== 4 && texts.length !== 5) ||
          htmlLabelsAlternatives.every((labels) =>
            texts.some((text, index) => text !== labels[index]),
          )
        ) {
          throw new Error(
            `Les entêtes de l'échéancier (${texts.join(", ")}) ne correspondent pas à ceux attendus`,
          )
        }
      } else if (tdElements.length <= 2) {
        // This is a followup line: same article but different decret.
        const { ARTICLE, BASE_LEGALE, OBJET } = lignes.at(
          -1,
        ) as EcheancierLigneEtendue
        await addLignes({
          ARTICLE,
          BASE_LEGALE,
          DATE_PREVUE: tdElements[1]?.textContent?.trim() || undefined,
          dossier,
          lignes,
          objectifTdElement: tdElements[0],
          OBJET,
          trIndex,
          withHtml: false,
        })
      } else {
        if (skipNumeroOrdreColumn) {
          tdElements.shift()
        }
        await addLignes({
          ARTICLE: tdElements[0]?.textContent?.trim() || undefined,
          BASE_LEGALE: tdElements[1]?.textContent?.trim() || undefined,
          DATE_PREVUE: tdElements[4]?.textContent?.trim() || undefined,
          dossier,
          lignes,
          objectifTdElement: tdElements[3],
          OBJET: tdElements[2]?.textContent?.trim() || undefined,
          trIndex,
          withHtml: false,
        })
      }
    }
    return {
      "@derniere_maj": yesterdayString,
      // Remove undefined values:
      LIGNE: JSON.parse(JSON.stringify(lignes)),
    }
  }

  return dossier.CONTENU.ECHEANCIER
}

// Replacer to use in JSON.stringify() to sort output.
function replacer(_key: string, value: unknown) {
  return value instanceof Object && !(value instanceof Array)
    ? Object.keys(value)
        .sort()
        .reduce(
          (sorted, key) => {
            sorted[key] = (value as { [key: string]: unknown })[key]
            return sorted
          },
          {} as { [key: string]: unknown },
        )
    : value
}

function* walkAElementsOrTexts(
  dossier: DossierLegislatif,
  element: Element,
  trIndex: number,
): Generator<HTMLAnchorElement | Text, void, unknown> {
  if (element.tagName === "A") {
    yield element as HTMLAnchorElement
  } else {
    for (const childNode of element.childNodes) {
      switch (childNode.nodeType) {
        case 1: {
          // Node.ELEMENT_NODE
          yield* walkAElementsOrTexts(dossier, childNode as Element, trIndex)
          break
        }

        case 3: {
          // Node.TEXT_NODE
          yield childNode as Text
          break
        }

        default: {
          throw new Error(
            `Dans le dossier ${dossier.META.META_COMMUN.ID}, à la ligne ${trIndex}, nœud de type inattendu ${childNode.nodeType} dans :\n${element.innerHTML}`,
          )
        }
      }
    }
  }
}

sade("generate_echeanciers_json", true)
  .describe(
    "Extract data from Légifrance et Assemblée nationale to generate JSON files containing the whole data needed by every pages of Échéancier",
  )
  .option("-o, --only", "Generate JSON of a single échéancier")
  .example("--only JORFTEXT000048727345")
  .action(async (options) => {
    await generateEcheanciersJson(options)
    process.exit(0)
  })
  .parse(process.argv)
