import fs from "fs-extra"
import path from "path"
import sade from "sade"

import type { Classification } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

const { classificationsFilename, echeancierDataDir } = config

const formatPercentage = new Intl.NumberFormat("fr-FR", { style: "percent" })
  .format

async function analyze() {
  const classificationsFilePath = path.join(
    echeancierDataDir,
    classificationsFilename,
  )
  const classifications = (
    (await fs.pathExists(classificationsFilePath))
      ? await fs.readJson(classificationsFilePath)
      : []
  ) as Classification[]
  console.log(
    "Nombre total de champs DECRET différents :",
    classifications.length,
  )
  const algorithmicClassifications = classifications.filter(
    (classification) => classification.algorithmic_etat !== undefined,
  )
  console.log(
    "Nombre de champs DECRET classés par algorithme :",
    algorithmicClassifications.length,
  )
  const humanClassifications = classifications.filter(
    (classification) =>
      classification.algorithmic_etat === undefined &&
      classification.human_etat !== undefined,
  )
  console.log(
    "Nombre de champs DECRET classés par traitement humain:",
    humanClassifications.length,
  )
  const sameAlgorithmAndClassifierClassifications =
    algorithmicClassifications.filter(
      (classification) =>
        classification.classifier_etat === classification.algorithmic_etat,
    )
  const sameHumanAndClassifierClassifications = humanClassifications.filter(
    (classification) =>
      classification.classifier_etat === classification.human_etat,
  )
  console.log(
    "Nombre de champs DECRET classés par zero-shot classifier de la même manière que par algorithme ou par humain :",
    sameAlgorithmAndClassifierClassifications.length +
      sameHumanAndClassifierClassifications.length,
    `=> pourcentage d'erreur : ${formatPercentage(
      1 -
        (sameAlgorithmAndClassifierClassifications.length +
          sameHumanAndClassifierClassifications.length) /
          (algorithmicClassifications.length + humanClassifications.length),
    )}`,
  )
  console.log(
    "Nombre de champs DECRET classés par zero-shot classifier de la même manière que par algorithme :",
    sameAlgorithmAndClassifierClassifications.length,
    `=> pourcentage d'erreur : ${formatPercentage(
      1 -
        sameAlgorithmAndClassifierClassifications.length /
          algorithmicClassifications.length,
    )}`,
  )
  console.log(
    "Nombre de champs DECRET classés par zero-shot classifier de la même manière que par traitement humain :",
    sameHumanAndClassifierClassifications.length,
    `=> pourcentage d'erreur : ${formatPercentage(
      1 -
        sameHumanAndClassifierClassifications.length /
          humanClassifications.length,
    )}`,
  )
}

sade("analyze_classifications", true)
  .describe("Generate some stats to compare classification methods")
  .action(async () => {
    await analyze()
    process.exit(0)
  })
  .parse(process.argv)
