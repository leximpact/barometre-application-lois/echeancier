import fs from "fs-extra"
import path from "path"
import sade from "sade"

import type {
  Classification,
  EtatLigne,
  LoiEtMesures,
  MesuresData,
} from "@leximpact/echeancier-common"

import config from "$lib/server/config"

const { classificationsFilename, echeancierDataDir } = config

async function classify() {
  const classificationsFilePath = path.join(
    echeancierDataDir,
    classificationsFilename,
  )
  const classificationByDecret: { [decret: string]: Classification } =
    Object.fromEntries(
      (
        ((await fs.pathExists(classificationsFilePath))
          ? await fs.readJson(classificationsFilePath)
          : []) as Classification[]
      ).map((classification) => [classification.decret, classification]),
    )

  const decrets = new Set<string>()
  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  for (const { loiJorfId } of lois) {
    const { echeanciers } = (await fs.readJson(
      path.join(echeancierDataDir, "echeanciers", `${loiJorfId}.json`),
    )) as LoiEtMesures
    for (const { LIGNE } of echeanciers) {
      for (const ligne of LIGNE) {
        if (ligne.CID_LOI_CIBLE === undefined && ligne.DECRET !== undefined) {
          decrets.add(ligne.DECRET)
        }
      }
    }
  }

  const classifications: Classification[] = []
  for (const decret of [...decrets].sort((decret1, decret2) =>
    decret1.localeCompare(decret2),
  )) {
    const classification = classificationByDecret[decret] ?? { decret }
    const etat: EtatLigne | undefined =
      decret.includes("Arrêté du ") ||
      decret.includes("Décret devenu sans objet") ||
      decret.includes("Mesure abrogée") ||
      decret.includes("Mesure appliquée par voie de circulaire") ||
      decret.includes("Mesure appliquée par voie réglementaire") ||
      decret.includes("Mesure déjà appliquée") ||
      decret.includes("Mesure déjà existante") ||
      decret.includes("Mesure devenue sans objet") ||
      decret.includes("Mesure éventuelle non utilisée") ||
      decret.includes("Mesure non nécessaire") ||
      decret.includes("Mesure sans objet")
        ? "caduc"
        : decret.includes("Mesure avec entrée en vigueur différée") ||
            decret.includes(
              "Mesure différée pour présomption d’incompatibilité avec le droit européen",
            ) ||
            decret.includes("Publication du décret envisagée") ||
            decret.includes("Publication envisagée") ||
            decret.includes("Publication éventuelle au plus tard ") ||
            decret.includes("Publication éventuelle en ") ||
            decret.includes("Publication éventuelle envisagée")
          ? "en_attente_application"
          : undefined
    if (etat !== undefined) {
      classification.algorithmic_etat = etat
    }
    classifications.push(classification)
  }
  await fs.writeJson(classificationsFilePath, classifications, {
    spaces: 2,
  })
}

sade("extract_classifications", true)
  .describe("Generate or update file containing Échéancier data to classify")
  .action(async () => {
    await classify()
    process.exit(0)
  })
  .parse(process.argv)
