import type { JorfTexteVersion } from "@tricoteuses/legal-explorer"
import Papa from "papaparse"

import { legiDb as legiDbOrUndefined } from "$lib/server/databases"
if (legiDbOrUndefined === undefined) {
  throw new Error("LEGI database is not configured.")
}
const legiDb = legiDbOrUndefined

const decretsIdByLoiId = new Map<string, string[]>()
{
  const texteVersionCursor = legiDb<{ data: JorfTexteVersion; id: string }[]>`
    SELECT data, texte_version.id
    FROM texte_version
    WHERE
    data -> 'META' -> 'META_COMMUN' ->> 'NATURE' = 'DECRET'
    AND data -> 'META' -> 'META_COMMUN' ->> 'ORIGINE' = 'JORF'
    AND data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_CHRONICLE' ->> 'DATE_TEXTE' >= '2002-06-19'
  `.cursor(100)
  for await (const rows of texteVersionCursor) {
    for (const { data: texteVersion, id } of rows) {
      const liens = texteVersion.META.META_SPEC.META_TEXTE_VERSION.LIENS?.LIEN
      const loisId = new Set(
        (liens
          ?.filter(
            (lien) =>
              ["LOI", "LOI_CONSTIT", "LOI_ORGANIQUE"].includes(
                lien["@naturetexte"] as string,
              ) &&
              lien["@sens"] === "source" &&
              lien["@typelien"] === "APPLICATION",
          )
          .map((lien) => lien["@id"])
          .filter((loiId) => loiId !== undefined) ?? []) as string[],
      )
      for (const loiId of loisId) {
        let decretsId = decretsIdByLoiId.get(loiId)
        if (decretsId === undefined) {
          decretsId = []
          decretsIdByLoiId.set(loiId, decretsId)
        }
        decretsId.push(id)
      }
    }
  }
}

const texteVersionCursor = legiDb<
  { assemblee_uid: string | null; data: JorfTexteVersion; id: string }[]
>`
  SELECT assemblee_uid, data, texte_version.id
  FROM texte_version
  LEFT JOIN texte_version_dossier_legislatif_assemblee_associations
    ON texte_version.id = texte_version_dossier_legislatif_assemblee_associations.id
  WHERE
    data -> 'META' -> 'META_COMMUN' ->> 'NATURE' IN (
      'LOI',
      'LOI_CONSTIT',
      'LOI_ORGANIQUE'
    )
    AND data -> 'META' -> 'META_COMMUN' ->> 'ORIGINE' = 'JORF'
    AND data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_CHRONICLE' ->> 'DATE_TEXTE' >= '2002-06-19'
    AND data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_VERSION' ->> 'TITREFULL' NOT LIKE '%(rectificatif)%'
    AND (
      data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_VERSION' ->> 'TITREFULL' LIKE '%autorisant l''approbation%'
      OR data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_VERSION' ->> 'TITREFULL' LIKE '%autorisant l'adhésion%'
      OR data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_VERSION' ->> 'TITREFULL' LIKE '%autorisant la ratification%'
    )
    ORDER BY data -> 'META' -> 'META_SPEC' -> 'META_TEXTE_CHRONICLE' ->> 'DATE_TEXTE' DESC
`.cursor(100)
const data = [
  [
    "ID loi JORF",
    "UID dossier Assemblée",
    "décrets",
    "décret d'application",
    "décrets d'application référençant la loi",
    "titre",
  ],
]
for await (const rows of texteVersionCursor) {
  for (const { assemblee_uid, data: texteVersion, id } of rows) {
    const liens = texteVersion.META.META_SPEC.META_TEXTE_VERSION.LIENS?.LIEN
    const decrets = liens?.filter((lien) => lien["@naturetexte"] === "DECRET")

    data.push([
      id,
      assemblee_uid ?? "",
      [...new Set(decrets?.map((decret) => decret["@id"]))].join(" "),
      [
        ...new Set(
          decrets
            ?.filter(
              (lien) =>
                lien["@sens"] === "cible" &&
                lien["@typelien"] === "APPLICATION",
            )
            .map((decret) => decret["@id"]),
        ),
      ].join(" "),
      (decretsIdByLoiId.get(id) ?? []).join(" "),
      texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL ?? "",
    ])
  }
}
console.log(Papa.unparse(data))
process.exit(0)
