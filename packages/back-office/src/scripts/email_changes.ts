import assert from "assert"
import dedent from "dedent-js"
import fs from "fs-extra"
import * as htmlToText from "html-to-text"
import mjml2html from "mjml"
import nodemailer from "nodemailer"
import path from "path"
import sade from "sade"

import {
  etatFromLigne,
  hashLigne,
  labelByEtatLigne,
  lignesSorted,
  type LoiEtMesures,
  type MesuresData,
} from "@leximpact/echeancier-common"

import type { AbonnementLoi, Compte } from "$lib/abonnements"
import { readClassificationByDecret } from "$lib/server/classifications"
import config from "$lib/server/config"
import { db } from "$lib/server/databases"
import { escapeXml, generateEmailFooterMjmlSections } from "$lib/server/emails"

const { containerUrls, echeancierDataDir, frontOffice, smtp } = config
const formatDate = new Intl.DateTimeFormat("fr-FR", {
  day: "numeric",
  month: "numeric",
  year: "numeric",
}).format

async function main({
  silent,
}: { silent?: boolean; verbose?: boolean } = {}): Promise<number> {
  const classificationByDecret = await readClassificationByDecret()

  const loisJorfId = (
    await db<{ loi_jorf_id: string }[]>`
      SELECT DISTINCT loi_jorf_id
      FROM abonnements_lois
    `
  ).map(({ loi_jorf_id }) => loi_jorf_id)

  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  const loisSubscribed = lois.filter(({ loiJorfId }) =>
    loisJorfId.includes(loiJorfId),
  )

  for (const {
    dossierAssembleeExtrait,
    loiJorfId,
    titreComplet,
  } of loisSubscribed) {
    const dossierAssembleeUid = dossierAssembleeExtrait?.uid
    const { echeanciers } = (await fs.readJson(
      path.join(echeancierDataDir, "echeanciers", `${loiJorfId}.json`),
    )) as LoiEtMesures
    echeanciers.reverse()

    const abonnements = await db<AbonnementLoi[]>`
      SELECT *
      FROM abonnements_lois
      WHERE loi_jorf_id = ${loiJorfId}
    `
    for (const abonnement of abonnements) {
      const compte = (
        await db<Compte[]>`
          SELECT *
          FROM comptes
          WHERE email = ${abonnement.email}
        `
      )[0]
      if (compte === undefined && !silent) {
        console.error(
          `L'utilisateur ${abonnement.email} est abonné à la loi ${loiJorfId}, mais il n'a pas de compte.`,
        )
        continue
      }

      const index =
        abonnement.derniere_maj === null
          ? 0
          : echeanciers.findIndex(
              (echeancier) =>
                echeancier["@derniere_maj"] !== undefined &&
                echeancier["@derniere_maj"] >
                  (abonnement.derniere_maj as string),
            )
      if (index === -1) {
        continue
      }
      let previousLignesHash = new Set(
        lignesSorted(echeanciers[index - 1]?.LIGNE ?? []).map(hashLigne),
      )
      for (const echeancier of echeanciers.slice(index)) {
        const lignes = lignesSorted(echeancier.LIGNE)
        const lignesChanged = lignes.filter(
          (ligne) => !previousLignesHash.has(hashLigne(ligne)),
        )
        if (lignesChanged.length === 0) {
          continue
        }

        const trArray = lignesChanged
          .map((ligne) => {
            const etat = etatFromLigne(classificationByDecret, ligne)
            return dedent`
            <mj-section padding="0px 25px 0px 25px" border-bottom="2px solid #233f6b" css-class="box-shadow">
              <mj-column vertical-align="middle">
                <mj-text mj-class="sans-padding">
                <p style="font-weight: bold;">
                  ${escapeXml(ligne.ARTICLE?.replace("<br/>".trim(), "") ?? "")}
                  ${
                    ligne.BASE_LEGALE === undefined
                      ? ""
                      : `<br />---<br />\n<span style="font-weight: normal; font-style: serif;  font-size: 14px;">Modifie <i>${escapeXml(
                          ligne.BASE_LEGALE,
                        )}</i></span>`
                  }
                  </p>
                </mj-text>
              </mj-column>
              <mj-column vertical-align="middle">
                <mj-text mj-class="sans-padding">
                  <p style=" font-size: 12px; line-height: 18px;">
                  ${escapeXml(ligne.OBJET ?? "")}
                  </p>
                </mj-text>
              </mj-column>
              <mj-column vertical-align="middle">
               <mj-text mj-class="sans-padding">
                  <p ${
                    etat === "applique"
                      ? 'style="background-color: #D2EEBD; color: #4b911a; text-align: center;"'
                      : etat === "en_attente_application"
                        ? 'style="background-color: #C8B6CF; color: #4B0B5E; text-align: center; margin: 0px 10px 0px 10px; border: 2px solid #4B0B5E; border-radius: 5px; "'
                        : ""
                  }>
                  ${escapeXml(labelByEtatLigne.get(etat) ?? "")}
                  </p>
                </mj-text>
              </mj-column>
              <mj-column vertical-align="middle">
                <mj-text mj-class="sans-padding">
                  <p style="font-size: 14px; color: #233f6b;">
                    ${escapeXml(ligne.DECRET ?? "")}
                  </p>
                </mj-text>
              </mj-column>
            </mj-section>
            `
              .split("\n")
              .join("\n          ")
          })
          .join("\n          ")
        const echeancierMjmlSection = dedent`
          <mj-section css-class="box-shadow">
            <mj-column>
              <mj-text>
                <h2 style="color: #000000">Mesures ayant changé ${
                  echeancier["@derniere_maj"] === undefined
                    ? ""
                    : ` le ${formatDate(new Date(echeancier["@derniere_maj"]))}`
                }&nbsp;:</h2>
              </mj-text>
            </mj-column>
          </mj-section>
          <mj-section padding="0px 25px 0px 25px" border-bottom="2px solid #233f6b" mj-class="section-echeancier-entete" css-class="box-shadow">
            <mj-column vertical-align="middle">
              <mj-text mj-class="sans-padding">
                <p style="font-weight: bold; color: #ffffff; text-align: center;">
                Article
                <p>
              </mj-text>
            </mj-column>
            <mj-column vertical-align="middle">
              <mj-text mj-class="sans-padding">
              <p style="font-weight: bold; color: #ffffff; text-align: center;">
              Objet de la mesure
              </p>
              </mj-text>
            </mj-column>
            <mj-column vertical-align="middle">
              <mj-text mj-class="sans-padding">
              <p style="font-weight: bold; color: #ffffff; text-align: center;">
              Statut
              </p>
              </mj-text>
            </mj-column>
            <mj-column vertical-align="middle">
              <mj-text mj-class="sans-padding">
              <p style="font-weight: bold; color: #ffffff; text-align: center;">
              Décrets publiés / Observations
              </p>
              </mj-text>
            </mj-column>
          </mj-section>
            ${trArray}
          <mj-section css-class="box-shadow">
            <mj-column>
              <mj-text>
                <p style="text-align: right;">
                <a style="color: #233f6b; text-decoration: underline;" href="${escapeXml(
                  containerUrls.echeanciers !== undefined
                    ? new URL(
                        `${dossierAssembleeUid ?? loiJorfId}`,
                        containerUrls.echeanciers,
                      ).toString()
                    : new URL(
                        `${dossierAssembleeUid ?? loiJorfId}`,
                        frontOffice.url,
                      ).toString(),
                )}">
                  Voir l'échéancier complet (toutes les mesures)
                </a>
                </p>
              </mj-text>
            </mj-column>
          </mj-section>
          <mj-section background-color="#FFFFFF" css-class="box-shadow" >
          <mj-group css-class="divider-drapeau">
            <mj-column width="33%">
              <mj-spacer container-background-color="#2A327D" height="4px" />
            </mj-column>
            <mj-column width="34%">
              <mj-spacer container-background-color="white" height="4px" />
            </mj-column>
            <mj-column width="33%">
              <mj-spacer container-background-color="#EE0C0C" height="4px" />
            </mj-column>  
          </mj-group>
        </mj-section>
        `

        const {
          abonnementsMjmlSection,
          barometreMjmlSection,
          suppressionMjmlSection,
        } = await generateEmailFooterMjmlSections(compte)

        const htmlOutput = mjml2html(
          dedent`
            <mjml>
              <mj-head>
                <mj-font
                name="Lato"
                href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,900;1,300;1,400;1,900&display=swap" />
                <mj-attributes>
                  <mj-text font-size="15px" font-family="Lato, Helvetica Neue, Helvetica, sans-serif" padding="0px 25px 0px 25px"/>
                  <mj-all font-family="Lato, Helvetica Neue, Helvetica, sans-serif"/>
                  <mj-section background-color="#ffffff" padding="0px 0px 0px 0px" />
                  <mj-divider
                    border-color="#a6b0d2"
                    border-style="solid"
                    border-width="1px"
                    padding="0px 0px 0px 0px"
                    width="100%" />
                  <mj-button
                    background-color="#233f6b"
                    border-radius="0px"
                    color="#ffffff"
                    font-size="16px"
                    inner-padding="10px 25px 10px 25px"
                    padding="10px 25px 10px 25px" />
                  <mj-image fluid-on-mobile="false" />
                  <mj-class name="section-block-gris" background-color="#ededf2" padding="10px 5px 60px 5px" />
                  <mj-class name="section-drapeau" padding="0px 0px 0px 0px" background-color="#233f6b" border-style="solid" border-width="1px" border-color="#233f6b" />
                  <mj-class name="button-secondary" color="#233f6b" background-color="white" border="2px solid #233f6b" />
                  <mj-class name="sans-padding" padding="0px 0px 0px 0px" />
                  <mj-class name="section-echeancier-entete" background-color="#435c84" color="#ffffff" opacity="0.2"/>
                </mj-attributes>
                <mj-style>
                  @media (max-width: 768px) {
                    h1 {
                      font-size: 23px;
                      line-height: 30px;
                      margin-bottom: 20px;
                      font-weight: 400;
                      text-align: center;
                    }
                  }
                  @media (min-width: 768px) {
                    h1 {
                      font-size: 25px;
                      line-height: 40px;
                      margin-top: 20px;
                      margin-bottom: 20px;
                      font-weight: 400;
                      text-align: center;
                    }
                  }
                  h2 {
                    font-size: 21px;
                    line-height: 26px;
                    font-weight: 600;
                  }
                  h3 {
                    font-size: 18px;
                    line-height: 26px;
                  }
                  p {
                    font-size: 16px;
                    line-height: 23px;
                  }
                  li {
                    line-height: 26px;
                    margin-bottom: 10px;
                  }
                  b {
                    font-weight: 900;
                  }
                  .box-shadow {
                    box-shadow: 0px 5px 3px gray; 
                  }
                  .box-shadow-drapeau {
                    box-shadow: 0px 0px 5px gray; 
                  }
                  .divider-drapeau {
                    box-shadow: 0px 0px 5px gray !important; 
                    width: 100px !important;
                    margin-top: 25px;
                    margin-bottom: 25px;
                    border-top: 0.5px solid #bfbfbf; 
                    border-bottom: 0.5px solid #bfbfbf;
                  }
                </mj-style>
                <mj-preview>${escapeXml(titreComplet ?? "loi")}</mj-preview>
                <mj-title>🗞 Mise à jour de l'application de la ${escapeXml(
                  titreComplet ?? "loi",
                )}</mj-title>
              </mj-head>
              <mj-body background-color="#e6e6e6">
                <mj-section padding="20px 0px 20px 0px" css-class="box-shadow">
                  <mj-column>
                    <mj-text align="center" font-size="25px" color="#233f6b">
                      Assemblée nationale
                    </mj-text>
                  </mj-column>
                </mj-section>
                <mj-section mj-class="section-drapeau" background-color="#233f6b" css-class="box-shadow-drapeau" >
                  <mj-group>
                    <mj-column width="33%">
                      <mj-spacer container-background-color="#2A327D" height="4px" />
                    </mj-column>
                    <mj-column width="34%">
                      <mj-spacer container-background-color="white" height="4px" />
                    </mj-column>
                    <mj-column width="33%">
                      <mj-spacer container-background-color="#EE0C0C" height="4px" />
                      </mj-column>  
                    <mj-column width="100%">
                      <mj-spacer container-background-color="#233f6b" height="30px" />
                    </mj-column>
                  </mj-group>
                </mj-section>
                <mj-section css-class="box-shadow">
                  <mj-column>
                    <mj-text>
                      <h1 style="color: #000000;">Mise à jour du ${
                        echeancier["@derniere_maj"] === undefined
                          ? ""
                          : `${formatDate(
                              new Date(echeancier["@derniere_maj"]),
                            )} `
                      }de l'échéancier d'application de la
                        <a style="color: #233f6b;" href="${escapeXml(
                          containerUrls.echeanciers !== undefined
                            ? new URL(
                                `${dossierAssembleeUid ?? loiJorfId}`,
                                containerUrls.echeanciers,
                              ).toString()
                            : new URL(
                                `${dossierAssembleeUid ?? loiJorfId}`,
                                frontOffice.url,
                              ).toString(),
                        )}">${escapeXml(titreComplet ?? "loi")}</a>
                      </h1>
                      <p>
                        Vous recevez ce courriel car vous êtes abonné<sup>*</sup> au suivi de l'application de cette loi sur le
                        <a style="color: #233f6b; text-decoration: underline;" href="${escapeXml(
                          containerUrls.index ?? frontOffice.url,
                        )}">baromètre de l'Assemblée nationale</a>.
                      </p>
                      <p style="font-size: 12px; line-height: 16px;">
                        <sup>*</sup> Si vous ne souhaitez plus être informé des actualités de cette loi, vous trouverez en bas de ce courriel
                        toutes les informations utiles pour résilier vos abonnements ou supprimer votre compte.
                      </p>
                    </mj-text>
                  </mj-column>
                </mj-section>
                <mj-section background-color="#FFFFFF" css-class="box-shadow" >
                  <mj-group css-class="divider-drapeau">
                    <mj-column width="33%">
                      <mj-spacer container-background-color="#2A327D" height="4px" />
                    </mj-column>
                    <mj-column width="34%">
                      <mj-spacer container-background-color="white" height="4px" />
                    </mj-column>
                    <mj-column width="33%">
                      <mj-spacer container-background-color="#EE0C0C" height="4px" />
                    </mj-column>  
                </mj-group>
                </mj-section>
                ${echeancierMjmlSection}
                ${barometreMjmlSection}
                ${abonnementsMjmlSection}
                ${suppressionMjmlSection}
              </mj-body>
            </mjml>
          `,
          { validationLevel: "strict" },
        )
        assert.notStrictEqual(htmlOutput.errors, undefined)
        const text = htmlToText.convert(htmlOutput.html, { wordwrap: 72 })

        const transport = nodemailer.createTransport({
          auth: {
            user: smtp.username,
            pass: smtp.password,
          },
          host: smtp.host,
          port: smtp.port,
          secure: smtp.secure,
          tls: {
            rejectUnauthorized: smtp.sslRejectUnauthorized,
          },

          // debug: true,
          // logger: true,
        })

        const message = {
          subject: `🗞 Suivi de l'application de la ${titreComplet ?? "loi"}`,
          text,
          html: htmlOutput.html,
          from: `Assemblée nationale - Baromètre de l'application des lois <${smtp.username}>`,
          to: compte.email, // listed in rfc822 message header
        }
        if (!silent) {
          console.log(
            `Envoi d'un email à ${abonnement.email} à propos d'une mise à jour de la loi ${loiJorfId}…`,
          )
        }
        await transport.sendMail(message)

        if (echeancier["@derniere_maj"] !== undefined) {
          await db`
            UPDATE abonnements_lois
            SET derniere_maj = ${echeancier["@derniere_maj"]}
            WHERE
              email = ${compte.email}
              AND loi_jorf_id = ${loiJorfId}
          `
        }

        previousLignesHash = new Set(lignes.map(hashLigne))
      }
    }
  }

  return 0
}

sade("email_changes", true)
  .describe(
    "Envoi des emails aux abonnés quand les échéanciers des lois sont mis à jour",
  )
  .option("-s, --silent", "Hide log messages")
  .option("-v, --verbose", "Show every log messages")
  .action(async (options) => process.exit(await main(options)))
  .parse(process.argv)
