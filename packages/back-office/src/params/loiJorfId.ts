import type { ParamMatcher } from "@sveltejs/kit"

export const match: ParamMatcher = function (param) {
  return param.match(/^JORFTEXT\d{12}$/) !== null
}
