import type { ParamMatcher } from "@sveltejs/kit"

export const match: ParamMatcher = function (param) {
  return param.match(/^[\da-f]{16}$/) !== null
}
