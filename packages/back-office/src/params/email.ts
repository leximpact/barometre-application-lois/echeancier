import {
  auditChain,
  auditEmail,
  auditRequire,
  cleanAudit,
} from "@auditors/core"
import type { ParamMatcher } from "@sveltejs/kit"

export const match: ParamMatcher = function (param) {
  const [, emailError] = auditChain(auditEmail, auditRequire)(cleanAudit, param)
  return emailError === null
}
