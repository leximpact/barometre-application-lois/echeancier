import { auditChain, cleanAudit, auditRequire } from "@auditors/core"
import type { Classification } from "@leximpact/echeancier-common"
import { auditClassificationAndIndex } from "@leximpact/echeancier-common/server"
import { error, json } from "@sveltejs/kit"

import {
  createClassificationsMergeRequest,
  retrieveClassifications,
} from "$lib/server/classifications"

import type { RequestHandler } from "./$types"

export const PUT: RequestHandler = async ({ fetch, request }) => {
  const [body, bodyError] = auditChain(
    auditClassificationAndIndex,
    auditRequire,
  )(cleanAudit, await request.json()) as [
    {
      classification: Classification
      index: number
    },
    unknown,
  ]
  if (bodyError !== null) {
    error(400, JSON.stringify(bodyError, null, 2))
  }
  const { classification, index } = body

  const classifications = await retrieveClassifications(fetch)
  classifications[index] = classification

  const mergeRequest = await createClassificationsMergeRequest(classifications)
  return json(mergeRequest)
}
