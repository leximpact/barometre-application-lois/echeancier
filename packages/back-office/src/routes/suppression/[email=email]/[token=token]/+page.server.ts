import fs from "fs-extra"
import path from "path"

import type { MesuresData } from "@leximpact/echeancier-common"

import type { AbonnementLoi, Compte } from "$lib/abonnements"
import config from "$lib/server/config"
import { db } from "$lib/server/databases"

import type { Actions, PageServerLoad } from "./$types"

const { echeancierDataDir } = config

export const actions = {
  default: async ({ params }) => {
    const email = params.email.toLowerCase()

    await db`
      DELETE FROM abonnements_lois
      WHERE email = ${email}
    `
    const compte = (
      await db<Compte[]>`
        DELETE FROM comptes
        WHERE
          email = ${email}
        RETURNING *
      `
    )[0]
    // Delete subscriptions once again, in case the user subscribed
    // to a new law in between.
    await db`
      DELETE FROM abonnements_lois
      WHERE email = ${email}
    `

    return { compte }
  },
} satisfies Actions

export const load: PageServerLoad = async ({ params }) => {
  const email = params.email.toLowerCase()

  const compteExistant = (
    await db<Compte[]>`
      SELECT *
      FROM comptes
      WHERE
        email = ${email}
        AND token = ${params.token}
    `
  )[0]

  const loisJorfId = (
    await db<AbonnementLoi[]>`
      SELECT *
      FROM abonnements_lois
      WHERE email = ${email}
    `
  ).map((abonnement) => abonnement.loi_jorf_id)

  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  const loisSubscribed = lois.filter(({ loiJorfId }) =>
    loisJorfId.includes(loiJorfId),
  )

  return { compteExistant, loisSubscribed }
}
