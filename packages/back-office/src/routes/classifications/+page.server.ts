import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures, MesuresData } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

import {
  retrieveClassifications,
  type LoiInfos,
} from "$lib/server/classifications"

import type { PageServerLoad } from "./$types"

const { echeancierDataDir } = config

export const load: PageServerLoad = async () => {
  const [classifications, loisInfos] = await Promise.all([
    (async () => await retrieveClassifications(fetch))(),

    (async () => {
      const { lois } = (await fs.readJson(
        path.join(echeancierDataDir, "echeanciers", "index.json"),
      )) as MesuresData

      const loisInfos: LoiInfos[] = []
      for (const { loiJorfId, titreComplet } of lois) {
        const { echeanciers } = (await fs.readJson(
          path.join(echeancierDataDir, "echeanciers", `${loiJorfId}.json`),
        )) as LoiEtMesures
        loisInfos.push({
          echeanciers,
          titreComplet,
        })
      }

      return loisInfos
    })(),
  ])

  return { classifications, loisInfos }
}
