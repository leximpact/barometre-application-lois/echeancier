import {
  auditArray,
  auditChain,
  auditRequire,
  cleanAudit,
} from "@auditors/core"
import type { Classification } from "@leximpact/echeancier-common"
import { auditClassification } from "@leximpact/echeancier-common/server"
import { error, json } from "@sveltejs/kit"

import {
  createClassificationsMergeRequest,
  retrieveClassifications,
} from "$lib/server/classifications"

import type { RequestHandler } from "./$types"

export const PUT: RequestHandler = async ({ fetch, request }) => {
  const [body, bodyError] = auditChain(
    auditArray(auditClassification, auditRequire),
    auditRequire,
  )(cleanAudit, await request.json()) as [Classification[], unknown]
  if (bodyError !== null) {
    error(400, JSON.stringify(bodyError, null, 2))
  }
  const newClassifications = body

  const classifications = await retrieveClassifications(fetch)

  let changed = false
  for (const [index, newClassification] of newClassifications.entries()) {
    const classification = classifications[index]
    if (newClassification.human_etat !== classification.human_etat) {
      classification.human_etat = newClassification.human_etat
      changed = true
    }
    if (newClassification.note !== classification.note) {
      classification.note = newClassification.note
      changed = true
    }
  }
  if (!changed) {
    return json({
      warning:
        "La classification n'a pas été soumise, car elle ne contenait aucune modification.",
    })
  }

  const mergeRequest = await createClassificationsMergeRequest(classifications)
  return json(mergeRequest)
}
