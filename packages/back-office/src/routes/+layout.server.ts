import config from "$lib/server/config"

export const load = () => {
  return {
    appTitle: config.title,
    backOffice: config.backOffice,
    containerUrls: config.containerUrls,
    frontOffice: config.frontOffice,
  }
}
