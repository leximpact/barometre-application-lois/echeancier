import {
  auditChain,
  auditEmail,
  auditFunction,
  auditObject,
  auditRequire,
  auditTest,
  cleanAudit,
} from "@auditors/core"
import { error, json } from "@sveltejs/kit"
import assert from "assert"
import { randomBytes } from "crypto"
import dedent from "dedent-js"
import fs from "fs-extra"
import * as htmlToText from "html-to-text"
import mjml2html from "mjml"
import nodemailer from "nodemailer"
import path from "path"

import type { LoiEtMesures } from "@leximpact/echeancier-common"

import type { AbonnementLoi, Compte } from "$lib/abonnements"
import config from "$lib/server/config"
import { db } from "$lib/server/databases"
import { escapeXml, generateEmailFooterMjmlSections } from "$lib/server/emails"

import type { RequestHandler } from "./$types"

const { backOffice, containerUrls, echeancierDataDir, frontOffice, smtp } =
  config

const auditAbonnementLoiRequired = auditChain(
  auditObject({
    email: [
      auditEmail,
      auditFunction((email: string) => email.toLowerCase()),
      auditRequire,
    ],
    loi_jorf_id: [
      auditTest(
        (id) => id.match(/^JORFTEXT\d{12}$/) !== null,
        "Identifiant de loi incorrect",
      ),
      auditRequire,
    ],
  }),
  auditRequire,
)

export const OPTIONS: RequestHandler = async () => {
  return new Response(null, {
    headers: {
      "Access-Control-Allow-Methods": "POST, OPTIONS",
      "Access-Control-Allow-Origin": new URL(frontOffice.url).origin,
      "Access-Control-Allow-Headers": "*",
    },
  })
}

export const POST: RequestHandler = async ({ request }) => {
  const [body, bodyError] = auditAbonnementLoiRequired(
    cleanAudit,
    await request.json(),
  ) as [AbonnementLoi, unknown]
  if (bodyError !== null) {
    error(400, JSON.stringify(bodyError, null, 2))
  }
  const { email, loi_jorf_id } = body

  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${loi_jorf_id}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(404, `Loi non trouvée : ${loi_jorf_id}`)
  }
  const { dossierAssemblee, texteVersion } = (await fs.readJson(
    loiEtMesuresFilePath,
  )) as LoiEtMesures

  const dossierAssembleeUid = dossierAssemblee?.uid

  let compte = (
    await db<Compte[]>`
      SELECT *
      FROM comptes
      WHERE email = ${email}
    `
  )[0]
  if (compte === undefined) {
    compte = (
      await db<Compte[]>`
        INSERT INTO comptes (
          email,
          token
        ) VALUES (
          ${email},
          ${randomBytes(8).toString("hex")}
        )
        ON CONFLICT
        DO NOTHING
        RETURNING *
      `
    )[0]
  }

  const abonnement = (
    await db<AbonnementLoi[]>`
    SELECT *
    FROM abonnements_lois
    WHERE
      email = ${email}
      AND loi_jorf_id = ${loi_jorf_id}
  `
  )[0]

  const abonnementDesabonnementMjml =
    abonnement === undefined
      ? dedent`
          <mj-button href="${escapeXml(
            new URL(
              `abonnement/${email}/${compte.token}/${texteVersion.META.META_COMMUN.ID}`,
              backOffice.url,
            ).toString(),
          )}">
            Je confirme mon abonnement
          </mj-button>
        `
      : dedent`
          <mj-button href="${escapeXml(
            new URL(
              `desabonnement/${email}/${compte.token}/${texteVersion.META.META_COMMUN.ID}`,
              backOffice.url,
            ).toString(),
          )}">
          Je confirme mon désabonnement
          </mj-button>
        `

  const {
    abonnementsMjmlSection,
    barometreMjmlSection,
    suppressionMjmlSection,
  } = await generateEmailFooterMjmlSections(compte)

  const htmlOutput = mjml2html(
    dedent`
      <mjml>
        <mj-head>
          <mj-font
          name="Lato"
          href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,900;1,300;1,400;1,900&display=swap" />
          <mj-attributes>
            <mj-text font-size="15px" font-family="Lato, Helvetica Neue, Helvetica, sans-serif" padding="0px 25px 0px 25px"/>
            <mj-all font-family="Lato, Helvetica Neue, Helvetica, sans-serif"/>
            <mj-section background-color="#ffffff" padding="0px 0px 0px 0px" />
            <mj-divider
              border-color="#a6b0d2"
              border-style="solid"
              border-width="1px"
              padding="0px 0px 0px 0px"
              width="100%" />
            <mj-button
              background-color="#233f6b"
              border-radius="0px"
              color="#ffffff"
              font-size="16px"
              inner-padding="10px 25px 10px 25px"
              padding="10px 25px 10px 25px" />
            <mj-image fluid-on-mobile="false" />
            <mj-class name="section-block-gris" background-color="#ededf2" padding="10px 5px 60px 5px" />
            <mj-class name="section-drapeau" padding="0px 0px 0px 0px" background-color="#233f6b" border-style="solid" border-width="1px" border-color="#233f6b" box-shadow="0px 0px 5px grey" />
            <mj-class name="button-secondary" color="#233f6b" background-color="white" border="2px solid #233f6b" />
          </mj-attributes>
          <mj-style>
            @media (max-width: 768px) {
              h1 {
                font-size: 23px;
                line-height: 28px;
                margin-bottom: 20px;
                font-weight: 400;
                text-align: center;
              }
            }
            @media (min-width: 768px) {
              h1 {
                font-size: 25px;
                line-height: 35px;
                margin-top: 20px;
                margin-bottom: 20px;
                font-weight: 400;
                text-align: center;
              }
            }
            h2 {
              font-size: 21px;
              line-height: 26px;
              font-weight: 600;
            }
            h3 {
              font-size: 18px;
              line-height: 26px;
            }
            p {
              font-size: 16px;
              line-height: 23px;
            }
            li {
              line-height: 26px;
              margin-bottom: 10px;
            }
            b {
              font-weight: 900;
            }
            .box-shadow {
              box-shadow: 0px 5px 3px gray; 
            }
            .box-shadow-drapeau {
              box-shadow: 0px 0px 5px gray; 
            }
            .divider-drapeau {
              box-shadow: 0px 0px 5px gray !important; 
              width: 100px !important;
              margin-top: 25px;
              margin-bottom: 25px; 
              border-top: 0.5px solid #bfbfbf; 
              border-bottom: 0.5px solid #bfbfbf;
            }
          </mj-style>
          <mj-preview>${escapeXml(
            texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL?.replace(
              /\s*\(\d+\)$/,
              "",
            ) ?? "",
          )}</mj-preview>
          <mj-title>Confirmez votre ${
            abonnement === undefined ? "abonnement" : "désabonnement"
          } au baromètre de l'application des lois</mj-title>
        </mj-head>
        <mj-body background-color="#e6e6e6">
            <mj-section padding="20px 0px 20px 0px" css-class="box-shadow">
              <mj-column>
                <mj-text align="center" font-size="25px" color="#233f6b">
                  Assemblée nationale
                </mj-text>
              </mj-column>
            </mj-section>
            <mj-section mj-class="section-drapeau" background-color="#233f6b" css-class="box-shadow-drapeau" >
              <mj-group>
                <mj-column width="33%">
                  <mj-spacer container-background-color="#2A327D" height="4px" />
                </mj-column>
                <mj-column width="34%">
                  <mj-spacer container-background-color="white" height="4px" />
                </mj-column>
                <mj-column width="33%">
                  <mj-spacer container-background-color="#EE0C0C" height="4px" />
                  </mj-column>  
                <mj-column width="100%">
                  <mj-spacer container-background-color="#233f6b" height="30px" />
                </mj-column>
              </mj-group>
            </mj-section>
            <mj-section css-class="box-shadow">
              <mj-column>
                <mj-text>
                  <h1 style="line-height : 30px;">
                   Confirmez votre ${
                     abonnement === undefined
                       ? "abonnement pour suivre"
                       : "désabonnement du suivi de"
                   }<br><span style="font-size: 18px;"> l'application d'une loi</span>
                  </h1>
                  <p >
                    Vous avez demandé à ${
                      abonnement === undefined ? "suivre" : "ne plus suivre"
                    }
                    l'application de la <a style="color: #233f6b;" href="${escapeXml(
                      containerUrls.echeanciers !== undefined
                        ? new URL(
                            `${dossierAssembleeUid ?? loi_jorf_id}`,
                            containerUrls.echeanciers,
                          ).toString()
                        : new URL(
                            `${dossierAssembleeUid ?? loi_jorf_id}`,
                            frontOffice.url,
                          ).toString(),
                    )}">${escapeXml(
                      texteVersion.META.META_SPEC.META_TEXTE_VERSION.TITREFULL?.replace(
                        /\s*\(\d+\)$/,
                        "",
                      ) ?? "",
                    )}</a>.
                  </p>
                  <p>
                    Si vous n'êtes pas à l'origine de cette demande, vous pouvez ignorer ce courriel.
                  </p>
                  <p style="font-weight: bold;">
                    Sinon cliquez sur le bouton ci-dessous pour confirmer votre demande :
                  </p>
                </mj-text>
                ${abonnementDesabonnementMjml}
              </mj-column>
            </mj-section>
            <mj-section background-color="#FFFFFF" css-class="box-shadow" >
            <mj-group css-class="divider-drapeau">
              <mj-column width="33%">
                <mj-spacer container-background-color="#2A327D" height="4px" />
              </mj-column>
              <mj-column width="34%">
                <mj-spacer container-background-color="white" height="4px" />
              </mj-column>
              <mj-column width="33%">
                <mj-spacer container-background-color="#EE0C0C" height="4px" />
              </mj-column>  
            </mj-group>
          </mj-section>
          ${barometreMjmlSection}
          ${abonnementsMjmlSection}
          ${suppressionMjmlSection}
        </mj-body>
      </mjml>
    `,
    { validationLevel: "strict" },
  )
  assert.notStrictEqual(htmlOutput.errors, undefined)
  const text = htmlToText.convert(htmlOutput.html, { wordwrap: 72 })

  const transport = nodemailer.createTransport({
    auth: {
      user: smtp.username,
      pass: smtp.password,
    },
    host: smtp.host,
    port: smtp.port,
    secure: smtp.secure,
    tls: {
      rejectUnauthorized: smtp.sslRejectUnauthorized,
    },

    // debug: true,
    // logger: true,
  })

  const message = {
    subject: `📩 Confirmez votre ${
      abonnement === undefined ? "abonnement" : "désabonnement"
    } au baromètre de l'application des lois`,
    text,
    html: htmlOutput.html,
    from: `Assemblée nationale - Baromètre de l'application des lois <${smtp.username}>`,
    to: email, // listed in rfc822 message header
  }
  await transport.sendMail(message)

  return json(
    { compte },
    {
      headers: {
        "Access-Control-Allow-Origin": new URL(frontOffice.url).origin,
      },
    },
  )
}
