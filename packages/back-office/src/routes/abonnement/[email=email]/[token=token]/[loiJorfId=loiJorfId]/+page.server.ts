import { error } from "@sveltejs/kit"
import fs from "fs-extra"
import path from "path"

import type { LoiEtMesures } from "@leximpact/echeancier-common"

import type { AbonnementLoi, Compte } from "$lib/abonnements"
import config from "$lib/server/config"
import { db } from "$lib/server/databases"

import type { Actions, PageServerLoad } from "./$types"

const { echeancierDataDir } = config

export const actions = {
  default: async ({ params }) => {
    const email = params.email.toLowerCase()

    const compte = (
      await db<Compte[]>`
        SELECT *
        FROM comptes
        WHERE
          email = ${email}
          AND token = ${params.token}
      `
    )[0]
    if (compte === undefined) {
      error(404, "Compte utilisateur non trouvé")
    }

    const loiEtMesuresFilePath = path.join(
      echeancierDataDir,
      "echeanciers",
      `${params.loiJorfId}.json`,
    )
    if (!(await fs.pathExists(loiEtMesuresFilePath))) {
      error(404, `Loi non trouvée : ${params.loiJorfId}`)
    }
    const { echeanciers } = (await fs.readJson(
      loiEtMesuresFilePath,
    )) as LoiEtMesures

    const derniereMaj = echeanciers[0]?.["@derniere_maj"]

    const abonnement = (
      await db<AbonnementLoi[]>`
        INSERT INTO abonnements_lois (
          derniere_maj,
          email,
          loi_jorf_id
        )
        VALUES (
          ${derniereMaj ?? null},
          ${email},
          ${params.loiJorfId}
        )
        ON CONFLICT DO NOTHING
        RETURNING *
      `
    )[0]

    return { abonnement }
  },
} satisfies Actions

export const load: PageServerLoad = async ({ params }) => {
  const email = params.email.toLowerCase()

  const compte = (
    await db<Compte[]>`
      SELECT *
      FROM comptes
      WHERE
        email = ${email}
        AND token = ${params.token}
    `
  )[0]
  if (compte === undefined) {
    error(404, "Compte utilisateur non trouvé")
  }

  const loiEtMesuresFilePath = path.join(
    echeancierDataDir,
    "echeanciers",
    `${params.loiJorfId}.json`,
  )
  if (!(await fs.pathExists(loiEtMesuresFilePath))) {
    error(404, `Loi non trouvée : ${params.loiJorfId}`)
  }
  const loiEtMesures = (await fs.readJson(loiEtMesuresFilePath)) as LoiEtMesures

  const abonnementExistant = (
    await db<AbonnementLoi[]>`
      SELECT *
      FROM abonnements_lois
      WHERE
        email = ${email}
        AND loi_jorf_id = ${params.loiJorfId}
    `
  )[0]

  return {
    abonnementExistant,
    compte,
    dossierAssembleeUid: loiEtMesures.dossierAssemblee?.uid,
    ...loiEtMesures,
  }
}
