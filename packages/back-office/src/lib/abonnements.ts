export type AbonnementLoi = {
  derniere_maj: string | null
  email: string
  loi_jorf_id: string
}

export type Compte = {
  email: string
  token: string
  valid: boolean
}
