import dedent from "dedent-js"
import fs from "fs-extra"
import path from "path"

import type { MesuresData } from "@leximpact/echeancier-common"

import type { AbonnementLoi, Compte } from "$lib/abonnements"
import config from "$lib/server/config"
import { db } from "$lib/server/databases"

const { backOffice, containerUrls, echeancierDataDir, frontOffice } = config

export function escapeXml(s: string): string {
  return s
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
  // .replace(/'/g, "&apos;")
}

export async function generateEmailFooterMjmlSections(compte: Compte): Promise<{
  abonnementsMjmlSection: string
  barometreMjmlSection: string
  suppressionMjmlSection: string
}> {
  const loisJorfId = (
    await db<AbonnementLoi[]>`
    SELECT loi_jorf_id
    FROM abonnements_lois
    WHERE email = ${compte.email}
    ORDER BY loi_jorf_id DESC
  `
  ).map(({ loi_jorf_id }) => loi_jorf_id)

  const { lois } = (await fs.readJson(
    path.join(echeancierDataDir, "echeanciers", "index.json"),
  )) as MesuresData
  const loisSubscribed = lois.filter(({ loiJorfId }) =>
    loisJorfId.includes(loiJorfId),
  )

  const abonnementsActuelsListItems = loisSubscribed.map(
    ({ dossierAssembleeExtrait, loiJorfId, titreComplet }) => {
      const dossierAssembleeUid = dossierAssembleeExtrait?.uid
      return dedent`
        <li>
          <p style="margin-bottom: 0px;">${escapeXml(titreComplet ?? "loi")}
          </p>
          <p style="margin-top: 0px;">
            <a style="text-decoration: underline; color: #233f6b;" href="${escapeXml(
              containerUrls.echeanciers !== undefined
                ? new URL(
                    `${dossierAssembleeUid ?? loiJorfId}`,
                    containerUrls.echeanciers,
                  ).toString()
                : new URL(
                    `${dossierAssembleeUid ?? loiJorfId}`,
                    frontOffice.url,
                  ).toString(),
            )}">Consulter la loi sur le baromètre</a>

            &nbsp;|&nbsp;<a style="text-decoration: underline; color: #233f6b;" href="${escapeXml(
              new URL(
                `desabonnement/${compte.email}/${compte.token}/${loiJorfId}`,
                backOffice.url,
              ).toString(),
            )}">Me désabonner de cette loi</a>
          </p>
        </li>
      `
    },
  )
  return {
    abonnementsMjmlSection:
      abonnementsActuelsListItems.length === 0
        ? ""
        : dedent`
            <mj-section padding-bottom="25px" css-class="box-shadow">
            <mj-column>
                <mj-text>
                  <h2 style="text-align: center;">Tous vos abonnements</h2>
                  <ul>
                    ${abonnementsActuelsListItems.join("\n")}
                  </ul>
                </mj-text>
              </mj-column>
            </mj-section>
            <mj-section background-color="#FFFFFF" css-class="box-shadow" >
            <mj-group css-class="divider-drapeau">
              <mj-column width="33%">
                <mj-spacer container-background-color="#2A327D" height="4px" />
              </mj-column>
              <mj-column width="34%">
                <mj-spacer container-background-color="white" height="4px" />
              </mj-column>
              <mj-column width="33%">
                <mj-spacer container-background-color="#EE0C0C" height="4px" />
              </mj-column>  
            </mj-group>
          </mj-section>
        `,
    barometreMjmlSection: dedent`
      <mj-section padding-bottom="25px" css-class="box-shadow">
        <mj-column>
          <mj-text>
            <h2 style="text-align: center;">
              Taux d'application des dernières lois promulguées
            </h2>
            <p>Consulter l'ensemble des lois et leurs taux d'application depuis le baromètre :        
          </mj-text>
          <mj-button href="${escapeXml(containerUrls.index ?? frontOffice.url)}">
            Baromètre de l'Assemblée nationale
          </mj-button>
        </mj-column>
      </mj-section>
      <mj-section background-color="#FFFFFF" css-class="box-shadow" >
        <mj-group css-class="divider-drapeau">
          <mj-column width="33%">
            <mj-spacer container-background-color="#2A327D" height="4px" />
          </mj-column>
          <mj-column width="34%">
            <mj-spacer container-background-color="white" height="4px" />
          </mj-column>
          <mj-column width="33%">
            <mj-spacer container-background-color="#EE0C0C" height="4px" />
          </mj-column>  
        </mj-group>
      </mj-section>
    `,
    suppressionMjmlSection: dedent`
      <mj-section mj-class="section-block-gris" css-class="box-shadow">
        <mj-column>
          <mj-text>
            <h2 style="font-weight: normal;">Suppression du compte :</h2>
            <p>
              Pour ${
                abonnementsActuelsListItems.length === 0
                  ? ""
                  : "ne plus suivre aucune loi et "
              }supprimer
              complètement votre compte du baromètre de l'application des lois,
              cliquez sur le bouton ci-dessous :
            </p>
          </mj-text>
          <mj-button mj-class="button-secondary" href="${escapeXml(
            new URL(
              `suppression/${compte.email}/${compte.token}`,
              backOffice.url,
            ).toString(),
          )}">
            Supprimer mon compte 🗑
          </mj-button>
        </mj-column>
      </mj-section>
    `,
  }
}
