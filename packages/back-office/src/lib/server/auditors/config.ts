import {
  auditEmptyToNull,
  auditFunction,
  auditHttpUrl,
  auditInteger,
  auditRequire,
  auditSetNullish,
  auditStringToBoolean,
  auditStringToNumber,
  auditTest,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["assembleeDb", "legiDb"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditDbConfig)
  }
  for (const key of ["backOffice", "frontOffice"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditSiteConfig,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "containerUrls",
    true,
    errors,
    remainingKeys,
    auditContainerUrlsConfig,
    auditRequire,
  )
  for (const key of ["classificationsFilename", "echeancierDataDir", "title"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "db",
    true,
    errors,
    remainingKeys,
    auditDbConfig,
    auditRequire,
  )
  audit.attribute(
    data,
    "doleEcheanciersHtmlDir",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditEmptyToNull,
  )
  audit.attribute(
    data,
    "echeancierDataRepository",
    true,
    errors,
    remainingKeys,
    auditRepositoryConfig,
    auditRequire,
  )
  audit.attribute(
    data,
    "smtp",
    true,
    errors,
    remainingKeys,
    auditSmtpConfig,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

function auditContainerUrlsConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["dossiers", "echeanciers"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditHttpUrl,
      auditFunction((url) => url.replace(/\/+$/, "") + "/"),
    )
  }
  for (const key of ["fonctionnement", "index", "statistiques"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditHttpUrl)
  }

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export function auditDbConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["database", "host", "password", "user"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "port",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditStringToNumber,
    auditInteger,
    auditTest(
      (value) => 0 <= value && value <= 65536,
      "Must be an integer between 0 and 65536",
    ),
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditRepositoryConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of [
    "accessToken",
    "authorEmail",
    "authorName",
    "branch",
    "forge",
    "group",
    "project",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditSiteConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  audit.attribute(
    data,
    "url",
    true,
    errors,
    remainingKeys,
    auditHttpUrl,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function auditSmtpConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["host", "username", "password"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditEmptyToNull,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "port",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditStringToNumber,
    auditInteger,
    auditTest(
      (value: number) => value >= 0 && value < 65536,
      "Value must be between 0 and 65535",
    ),
    auditRequire,
  )
  audit.attribute(
    data,
    "secure",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditStringToBoolean,
    auditRequire,
  )
  audit.attribute(
    data,
    "sslRejectUnauthorized",
    true,
    errors,
    remainingKeys,
    auditTrimString,
    auditStringToBoolean,
    auditSetNullish(true),
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}
