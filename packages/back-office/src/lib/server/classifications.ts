import { error } from "@sveltejs/kit"
import type { Echeancier } from "@tricoteuses/legal-explorer"
import { randomBytes } from "crypto"
import fs from "fs-extra"
import path from "path"

import type { Classification } from "@leximpact/echeancier-common"

import config from "$lib/server/config"

export type LoiInfos = {
  echeanciers: Echeancier[]
  titreComplet?: string
}

const { classificationsFilename, echeancierDataDir, echeancierDataRepository } =
  config
const classificationsFilePath = path.join(
  echeancierDataDir,
  classificationsFilename,
)

export const classificationByDecret: { [decret: string]: Classification } =
  Object.fromEntries(
    (
      ((await fs.pathExists(classificationsFilePath))
        ? await fs.readJson(classificationsFilePath)
        : []) as Classification[]
    ).map((classification) => [classification.decret, classification]),
  )

export async function createClassificationsMergeRequest(
  classifications: Classification[],
): Promise<{ [key: string]: unknown }> {
  // Create new branch.
  const newBranchName = `back_office_${randomBytes(4).toString("hex")}`
  const newBranchUrl = `https://${
    echeancierDataRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${echeancierDataRepository.group}/${echeancierDataRepository.project}`,
  )}/repository/branches?branch=${newBranchName}&ref=${encodeURIComponent(
    echeancierDataRepository.branch,
  )}`
  const newBranchResponse = await fetch(newBranchUrl, {
    body: "",
    headers: {
      Accept: "application/json",
      "PRIVATE-TOKEN": echeancierDataRepository.accessToken,
    },
    method: "POST",
  })
  if (!newBranchResponse.ok) {
    error(newBranchResponse.status, await newBranchResponse.text())
  }

  // Update classifications file & commit it.
  const classificationsUpdateUrl = `https://${
    echeancierDataRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${echeancierDataRepository.group}/${echeancierDataRepository.project}`,
  )}/repository/files/${encodeURIComponent(classificationsFilename)}`
  const commitMessage = `Classification par le back-office ${
    new Date().toISOString().split(".")[0]
  }`
  const classificationsUpdateResponse = await fetch(classificationsUpdateUrl, {
    body: JSON.stringify(
      {
        author_email: echeancierDataRepository.authorEmail,
        author_name: echeancierDataRepository.authorName,
        branch: newBranchName,
        commit_message: commitMessage,
        content: JSON.stringify(classifications, null, 2),
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "PRIVATE-TOKEN": echeancierDataRepository.accessToken,
    },
    method: "PUT",
  })
  if (!classificationsUpdateResponse.ok) {
    error(
      classificationsUpdateResponse.status,
      await classificationsUpdateResponse.text(),
    )
  }
  /* const classificationsUpdate = */ await classificationsUpdateResponse.json()

  // Create merge request.
  const mergeRequestUrl = `https://${
    echeancierDataRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${echeancierDataRepository.group}/${echeancierDataRepository.project}`,
  )}/merge_requests`
  const mergeRequestResponse = await fetch(mergeRequestUrl, {
    body: JSON.stringify(
      {
        allow_collaboration: true,
        remove_source_branch: true,
        source_branch: newBranchName,
        target_branch: echeancierDataRepository.branch,
        title: commitMessage,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "PRIVATE-TOKEN": echeancierDataRepository.accessToken,
    },
    method: "POST",
  })
  if (!mergeRequestResponse.ok) {
    error(mergeRequestResponse.status, await mergeRequestResponse.text())
  }
  return await mergeRequestResponse.json()
}

export async function readClassificationByDecret(): Promise<{
  [decret: string]: Classification
}> {
  return Object.fromEntries(
    (
      ((await fs.pathExists(classificationsFilePath))
        ? await fs.readJson(classificationsFilePath)
        : []) as Classification[]
    ).map((classification) => [classification.decret, classification]),
  )
}

export async function retrieveClassifications(
  fetch: (
    input: RequestInfo | URL,
    init?: RequestInit | undefined,
  ) => Promise<Response>,
): Promise<Classification[]> {
  const url = `https://${
    echeancierDataRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${echeancierDataRepository.group}/${echeancierDataRepository.project}`,
  )}/repository/files/${encodeURIComponent(classificationsFilename)}/raw`
  const response = await fetch(url, {
    headers: {
      Accept: "application/json",
      "PRIVATE-TOKEN": echeancierDataRepository.accessToken,
    },
  })
  if (!response.ok) {
    error(response.status, await response.text())
  }
  return (await response.json()) as Classification[]
}
