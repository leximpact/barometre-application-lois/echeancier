import "dotenv/config"

import { validateConfig } from "$lib/server/auditors/config"
import type { RepositoryConfig } from "$lib/server/repositories"

export interface Config {
  assembleeDb?: DbConfig
  backOffice: {
    url: string
  }
  classificationsFilename: string
  containerUrls: App.ContainerUrls
  db: DbConfig
  doleEcheanciersHtmlDir?: string
  echeancierDataDir: string
  echeancierDataRepository: RepositoryConfig
  frontOffice: {
    url: string
  }
  legiDb?: DbConfig
  smtp: {
    host: string
    password: string
    port: number
    secure: boolean
    sslRejectUnauthorized: boolean
    username: string
  }
  title: string
}

export interface DbConfig {
  host: string
  port: number
  database: string
  user: string
  password: string
}

const [config, error] = validateConfig({
  assembleeDb: process.env.ASSEMBLEE_DB_NAME
    ? {
        host: process.env.ASSEMBLEE_DB_HOST,
        port: process.env.ASSEMBLEE_DB_PORT,
        database: process.env.ASSEMBLEE_DB_NAME,
        user: process.env.ASSEMBLEE_DB_USER,
        password: process.env.ASSEMBLEE_DB_PASSWORD,
      }
    : undefined,
  backOffice: {
    url: process.env["BACK_OFFICE_URL"],
  },
  classificationsFilename: process.env.CLASSIFICATIONS_FILENAME,
  containerUrls: {
    dossiers: process.env.CONTAINER_DOSSIERS_URL,
    echeanciers: process.env.CONTAINER_ECHEANCIERS_URL,
    fonctionnement: process.env.CONTAINER_FONCTIONNEMENT_URL,
    index: process.env.CONTAINER_INDEX_URL,
    statistiques: process.env.CONTAINER_STATISTIQUES_URL,
  },
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
  },
  doleEcheanciersHtmlDir: process.env.DOLE_ECHEANCIERS_HTML_DIR,
  echeancierDataDir: process.env.ECHEANCIER_DATA_DIR,
  echeancierDataRepository: {
    accessToken: process.env["ECHEANCIER_DATA_REPO_ACCESS_TOKEN"],
    authorEmail: process.env["ECHEANCIER_DATA_REPO_AUTHOR_EMAIL"],
    authorName: process.env["ECHEANCIER_DATA_REPO_AUTHOR_NAME"],
    branch: process.env["ECHEANCIER_DATA_REPO_BRANCH"],
    forge: process.env["ECHEANCIER_DATA_REPO_FORGE"],
    group: process.env["ECHEANCIER_DATA_REPO_GROUP"],
    project: process.env["ECHEANCIER_DATA_REPO_PROJECT"],
  },
  frontOffice: {
    url: process.env["FRONT_OFFICE_URL"],
  },
  legiDb: process.env.LEGI_DB_NAME
    ? {
        host: process.env.LEGI_DB_HOST,
        port: process.env.LEGI_DB_PORT,
        database: process.env.LEGI_DB_NAME,
        user: process.env.LEGI_DB_USER,
        password: process.env.LEGI_DB_PASSWORD,
      }
    : undefined,
  smtp: {
    host: process.env["SMTP_HOST"],
    password: process.env["SMTP_PASSWORD"],
    port: process.env["SMTP_PORT"],
    secure: process.env["SMTP_SECURE"],
    sslRejectUnauthorized: process.env["SMTP_SSL_REJECT_UNAUTHORIZED"],
    username: process.env["SMTP_USERNAME"],
  },
  title: process.env.TITLE,
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in server configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}
export default config
