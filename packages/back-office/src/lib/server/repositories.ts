export interface RepositoryConfig {
  accessToken: string
  authorName: string
  authorEmail: string
  branch: string
  forge: string
  group: string
  project: string
}
