import assert from "assert"

import { db, type Version, versionNumber } from "$lib/server/databases"

export async function configureDatabase() {
  // Table: version
  await db`
    CREATE TABLE IF NOT EXISTS version(
      number integer NOT NULL
    )
  `
  let version = (await db<Version[]>`SELECT * FROM version`)[0]
  if (version === undefined) {
    version = (
      await db<Version[]>`
        INSERT INTO version (number)
        VALUES (${versionNumber})
        RETURNING *
      `
    )[0]
  } else if (version.number === undefined) {
    version = { number: 0 }
  }
  assert(
    version.number <= versionNumber,
    `Database is too recent for current version of application: ${version.number} > ${versionNumber}.`,
  )
  if (version.number < versionNumber) {
    console.log(
      `Upgrading database from version ${version.number} to ${versionNumber}…`,
    )
  }

  // Apply patches that must be executed before every table is created.
  if (version.number < 2) {
    await db`
      ALTER TABLE IF EXISTS abonnements_lois
      ADD COLUMN IF NOT EXISTS derniere_maj text
    `
    await db`
      ALTER TABLE IF EXISTS comptes
      DROP COLUMN IF EXISTS valid
    `
  }

  // Types

  // Tables

  // Table: abonnements_lois
  await db`
    CREATE TABLE IF NOT EXISTS abonnements_lois (
      email text NOT NULL,
      loi_jorf_id text NOT NULL,
      derniere_maj text,
      PRIMARY KEY (loi_jorf_id, email)
    )
  `

  // Table: comptes
  await db`
    CREATE TABLE IF NOT EXISTS comptes (
      created_at timestamp with time zone NOT NULL DEFAULT NOW(),
      email text PRIMARY KEY,
      token text NOT NULL
    )
  `

  // Apply patches that must be executed after every table is created.

  // Add indexes once every table and column exists.

  // Add comments once every table and column exists.

  // Upgrade version number if needed.

  const previousVersionNumber = version.number

  version.number = versionNumber
  assert(
    version.number >= previousVersionNumber,
    `Error in database upgrade script: Wrong version number: ${version.number} < ${previousVersionNumber}.`,
  )
  if (version.number !== previousVersionNumber) {
    await db`UPDATE version SET number = ${version.number}`
    console.log(
      `Upgraded database from version ${previousVersionNumber} to ${version.number}.`,
    )
  }
}
