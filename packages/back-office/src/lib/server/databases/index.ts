import assert from "assert"
import postgres from "postgres"

import config from "$lib/server/config"

export const assembleeDb =
  config.assembleeDb === undefined ? undefined : postgres(config.assembleeDb)
export const db = postgres(config.db)
export const legiDb =
  config.legiDb === undefined ? undefined : postgres(config.legiDb)

export interface Version {
  number: number
}

export const assembleeVersionNumber = 9
export const legiVersionNumber = 8
export const versionNumber = 2

/// Check that assemblee database exists and is up to date.
export async function checkAssembleeDb(
  assembleeDb: postgres.Sql,
): Promise<void> {
  assert(
    (
      await assembleeDb`SELECT EXISTS (
        SELECT * FROM information_schema.tables WHERE table_name='version'
      )`
    )[0]?.exists,
    "Assemblee database is not initialized.",
  )
  const version = (await assembleeDb<Version[]>`SELECT * FROM version`)[0]
  assert.notStrictEqual(
    version,
    undefined,
    "Assemblee database has no version number.",
  )
  assert(
    version.number <= assembleeVersionNumber,
    "Assemblee database format is too recent.",
  )
  assert.strictEqual(
    version.number,
    assembleeVersionNumber,
    "Assemblee database must be upgraded.",
  )
}

/// Check that database exists and is up to date.
export async function checkDb(db: postgres.Sql): Promise<void> {
  assert(
    (
      await db`SELECT EXISTS (
        SELECT * FROM information_schema.tables WHERE table_name='version'
      )`
    )[0]?.exists,
    'Database is not initialized. Run "npm run configure" to do it.',
  )
  const version = (await db<Version[]>`SELECT * FROM version`)[0]
  assert.notStrictEqual(
    version,
    undefined,
    'Database has no version number. Run "npm run configure" to do it.',
  )
  assert(version.number <= versionNumber, "Database format is too recent.")
  assert.strictEqual(
    version.number,
    versionNumber,
    'Database must be upgraded. Run "npm run configure" to do it.',
  )
}

/// Check that LEGI database exists and is up to date.
export async function checkLegiDb(legiDb: postgres.Sql): Promise<void> {
  assert(
    (
      await legiDb`SELECT EXISTS (
        SELECT * FROM information_schema.tables WHERE table_name='version'
      )`
    )[0]?.exists,
    "Legi database is not initialized.",
  )
  const version = (await legiDb<Version[]>`SELECT * FROM version`)[0]
  assert.notStrictEqual(
    version,
    undefined,
    "Legi database has no version number.",
  )
  assert(
    version.number <= legiVersionNumber,
    "Legi database format is too recent.",
  )
  assert.strictEqual(
    version.number,
    legiVersionNumber,
    "Legi database must be upgraded.",
  )
}
