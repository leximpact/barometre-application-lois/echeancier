import type { Handle } from "@sveltejs/kit"

import config from "$lib/server/config"
import { csrfHandler } from "$lib/server/csrf_handler"

export const handle: Handle = csrfHandler(
  ["/abonnement/demande"],
  [new URL(config.frontOffice.url).origin],
)
