const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  plugins: [require("@tailwindcss/forms")],
  theme: {
    extend: {},
  },
}

module.exports = config
