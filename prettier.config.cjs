module.exports = {
  overrides: [{ files: "*.svelte", options: { parser: "svelte" } }],
  plugins: ["prettier-plugin-svelte", "prettier-plugin-tailwindcss"],
  semi: false,
  trailingComma: "all",
}
