# Baromètre de l'application des lois de l'Assemblée nationale (aka Échéancier LexImpact)

_French law enforcement tracking software_

## Front-Office Installation & Configuration

The front office is all you need to render the application that is embedded in https://www.assemblee-nationale.fr/dyn/taux-application-lois.

```bash
# Download Échéancier data (updated every day):
git clone https://git.leximpact.dev/leximpact/barometre-application-lois/echeancier-data.git

# Install monorepo of project:
git clone https://git.leximpact.dev/leximpact/barometre-application-lois/echeancier
cd echeancier/
npm install

# Build shared library (used by front-office & back-office):
cd packages/common/
npm run build

# Configure & launch front-office:
cd ../front-office/
cp example.env .env
# Optionally change the configuration by edititing `.env` file,
# Then, launch front-office in develoment mode:
npm run dev
```

### Generating Front-Office Static Site & Fragments

Generate & run static site:

```bash
npm run build
npx http-server build
```

Generate HTML, CSS & JS fragments needed to embed Échéancier front-office in Assemblée nationale web site:

```bash
npx tsx src/scripts/generate_fragments.ts https://barometre.assemblee-nationale.fr/
```

## Back-Office Installation & Configuration

The back-office is only used to generate data used by front-office, to send emails to subscribers, to improve classification of measures, etc.

### Back-Office Database Creation

### Using _Debian GNU/Linux_

As `root` user:

```bash
apt install postgresql
apt install libpq-dev
su - postgres
psql
```

### Using _MacOS_

```bash
brew install postgresql
psql postgres
```

### For everybody

```bash
CREATE USER echeancier WITH PASSWORD 'echeancier';
CREATE DATABASE echeancier WITH OWNER echeancier;
\q
logout # Back to `root`, only for Debian
```

As a normal user:

```bash
cd echeancier/
npm install
# Build shared library (used by front-office & back-office):
cd packages/common/
npm run build

cd ../back-office
cp example.env .env
# Optionally change the configuration by edititing `.env` file,
# Then, configure back-office database:
npm run configure
# Launch back-office in develoment mode:
npm run dev
```

## In Situ Installation & Configuration

`In_situ` is a website that simulates usage of front-office embedded in Assemblée nationale web site.

Note: `In_situ` doesn't use or need back-office and database.

```bash
cd echeancier/
npm install
# Build shared library:
cd packages/common/
npm run build

cd ../in_situ
cp example.env .env
# Optionally change the configuration by edititing `.env` file,
# Then,launch back-office in develoment mode:
npm run dev
```

### (Re)generation of Assemblée web pages used as templates for `in_situ`:

Note: This regeneration is only needed when Assemblée web site changes.

```bash
cd /tmp/
wget -r https://www.assemblee-nationale.fr/dyn/16/dossiers/calculer_retraite_base_nonsalaries_agricoles
# Wait a few minutes, then:
wget -r https://www.assemblee-nationale.fr/dyn/16/dossiers/alt/calculer_retraite_base_nonsalaries_agricoles
```

After a few minutes (in another terminal or after having stopped `wget`):

```bash
cd [...]/echeancier/packages/in_situ/
cp /tmp/www.assemblee-nationale.fr/dyn/16/dossiers/calculer_retraite_base_nonsalaries_agricoles templates/dossiers/calculer_retraite_base_nonsalaries_agricoles.html
cp /tmp/www.assemblee-nationale.fr/dyn/16/dossiers/alt/calculer_retraite_base_nonsalaries_agricoles templates/dossiers/alt/calculer_retraite_base_nonsalaries_agricoles.html
# wget -O templates/dossiers/index.html https://www.assemblee-nationale.fr/dyn/16/dossiers
rsync -av --delete --exclude videos/ /tmp/www.assemblee-nationale.fr/assets/ static/assets/
rsync -av --delete /tmp/www.assemblee-nationale.fr/dyn/assets/ static/dyn/assets/
find static/ -name "*\?*"
rm static/dyn/assets/fonts/Slick/slick.eot\?
rename -- 's/\?.*$//' static/dyn/assets/fonts/*/*
rsync -av --delete /tmp/www.assemblee-nationale.fr/dyn/css/ static/dyn/css/
rsync -av --delete /tmp/www.assemblee-nationale.fr/dyn/es6/ static/dyn/es6/
rsync -av --delete /tmp/www.assemblee-nationale.fr/dyn/js/ static/dyn/js/

wget -O templates/cec/index.html https://www.assemblee-nationale.fr/dyn/16/organes/delegations-comites-offices/cec/
```
